﻿using System;

namespace SimulasiUjian
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string ulang = "";
            do
            {
                int pilih;
                Console.Write("pilih soal 1-10 : ");
                pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 1:
                        soal1();
                        break;
                    case 2:
                        soal2();
                        break;
                    case 3:
                        soal3();
                        break;
                    case 4:
                        soal4();
                        break;
                    case 5:
                        soal5();
                        break;
                    case 6:
                        soal6();
                        break;
                    case 7:
                        soal7();
                        break;
                    case 8:
                        soal8();
                        break;
                    case 9:
                        soal9();
                        break;
                    case 10:
                        soal10();
                        break;
                }
                Console.ReadKey();
                Console.Write("\nPilih Soal Lagi? (y/n) : ");
                ulang = Console.ReadLine();
            }
            while (ulang.ToUpper() == "Y");
        }

        static void soal1()
        {
            Console.Write("Input = ");
            string input = Console.ReadLine();
            string alphabet = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
            int hasil = 0;

            for (int i = 0; i < input.Length; i++)
            {
                int indexHuruf = alphabet.IndexOf(input[i]);
                if (indexHuruf != -1)
                {
                    hasil += 1;
                }
            }
            Console.Write(hasil);
        }
        static void soal2()
        {
            Console.WriteLine("start : ");
            int start = int.Parse(Console.ReadLine());
            Console.WriteLine("End : ");
            int end = int.Parse(Console.ReadLine());
            DateTime date = new DateTime(2022, 08, 07);

            for (int i = start; i <= end; i++)
            {
                Console.Write($"XA-{date.Day}{date.Month}{date.Year}-");
                if (start < 100)
                {
                    Console.Write($"000{start}");
                }
                else if (start < 1000)
                {
                    Console.Write($"0{start}");
                }
                start++;
                Console.Write("\n");
            }
        }
        static void soal3()
        {
            Console.Write("Keranjang 1 = ");
            string keranjang1 = Console.ReadLine().ToLower();
            Console.Write("Keranjang 2 = ");
            string keranjang2 = Console.ReadLine();
            Console.Write("Keranjang 3 = ");
            string keranjang3 = Console.ReadLine();
            //karena mau di hitung masukkan ke string [] dan nantinya di parse ke int
            string[] arrKeranjang = new string[] { keranjang1, keranjang2, keranjang3 };

            int totalBuah = 0; //tempat save

            for (int i = 0; i < arrKeranjang.Length; i++)
            {
                if (arrKeranjang[i] == "kosong")
                {
                    arrKeranjang[i] = arrKeranjang[i].Replace("kosong", "0");
                }
            }

            int kerangjangA = int.Parse(arrKeranjang[0]);
            int kerangjangB = int.Parse(arrKeranjang[1]);
            int kerangjangC = int.Parse(arrKeranjang[2]);

            Console.Write("Keranjang yang dibawa ke pasar = ");
            int keranjangPasar = int.Parse(Console.ReadLine());

            if (keranjangPasar == 1)
            {
                totalBuah = kerangjangB + kerangjangC;
            }
            else if (keranjangPasar == 2)
            {
                totalBuah = kerangjangA + kerangjangC;
            }
            else if (keranjangPasar == 3)
            {
                totalBuah = kerangjangA + kerangjangB;
            }
            Console.Write($"Sisa Buah = {totalBuah} buah");
        }
        static void soal4()
        {
            Console.Write("Laki Dewasa = ");
            int lakiDewasa = int.Parse(Console.ReadLine());
            Console.Write("Wanita Dewasa = ");
            int wanitaDewasa = int.Parse(Console.ReadLine());
            Console.Write("Anak-anak = ");
            int anak = int.Parse(Console.ReadLine());
            Console.Write("Bayi = ");
            int bayi = int.Parse(Console.ReadLine());
            Console.Write("Input baju untuk = ");
            int pilih = int.Parse(Console.ReadLine());
            int total = 0;

            switch (pilih)
            {
                case 1:
                    Console.Write("Laki Dewasa = ");
                    int pilih1 = int.Parse(Console.ReadLine());
                    total = lakiDewasa + (wanitaDewasa * 2) + (anak * 3) + (bayi * 5) + pilih1;
                    break;
            }

            int totalBaru = 0;

            if (total > 10 && total % 2 > 0)
            {
                totalBaru = total;
            }

            Console.WriteLine($"Total Baju = {totalBaru} Buah");
        }
        static void soal5()
        {
            Console.WriteLine("Masukan Nilai = "); //73,67,38,33
            //string mau di jumlahkan ke bil.bulat gunakan array dan split
            string[] arrNilai = Console.ReadLine().Split(","); //73673833

            double[] nilai = new double[arrNilai.Length];//73673833
            for (int i = 0; i < arrNilai.Length; i++)
            {
                nilai[i] = double.Parse(arrNilai[i]);//0|73 > 1|67 > 2|38 > 3|33
            }
            double[] nilaiBaru = new double[nilai.Length];

            for (int i = 0; i < nilai.Length; i++) //0|73 > 1|67 > 2|38 > 3|33 length = 4
            {
                if (nilai[i] < 35) //33
                {
                    nilaiBaru[i] = nilai[i]; //33
                    Console.WriteLine(nilaiBaru[i]);//33
                }
                else
                {
                    if (nilai[i] % 5 < 3) // 67 sisa = 2
                    {
                        nilaiBaru[i] = nilai[i];
                        Console.WriteLine(nilaiBaru[i]); //67
                    }
                    else
                    {
                        nilaiBaru[i] = Math.Ceiling(nilai[i] / 5); //73 sisa = 3 ||75 | 38 sia = 3 || 40
                        nilaiBaru[i] *= 5; //untuk kelipatan 5
                        Console.WriteLine(nilaiBaru[i]);//75 dan 40
                    }
                }
            }
            Console.WriteLine("");
        }
        static void soal6()
        {
            //A quick brown fox jumps over the lazy dog
            //Check back tomorrow I Will see if the book has arrived
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Console.Write("Input = ");
            string input = Console.ReadLine().ToUpper();
            
            int hasil = 0;

            for (int i = 0; i < alphabet.Length; i++)// ini megikuti dari bacaan dari alphabet yang sudah di dbuat untuk di deteksi
            {
                int indexHuruf = input.IndexOf(alphabet[i]);// cek apakah input sudah ada di dalam index alphabet
                if (indexHuruf > -1)
                {
                    hasil += 1;
                }
            }
            Console.WriteLine(hasil);
            if (hasil >= 26)
            {
                Console.WriteLine("Kalimat ini adalah Pangram");
            }
            else
            {
                Console.WriteLine("Kalimat ini bukan Pangram");
            }
        }
        static void soal7() //belum
        {
            int x = 1, y = 1, z = 0, i, jumlah;
            Console.Write("Masukan angka : ");
            jumlah = int.Parse(Console.ReadLine());
            Console.Write($"{x} {y} "); // cetak angka pertama dan kedua menggunakan spasi
            for (i = 2; i < jumlah; ++i)
            {
                z = x + y;
                Console.Write(z + " ");
                x = y;
                y = z;
            }
            double sum = x + y + z - 1; //1,1,2,3,5,8,13,21,34,55 = 144-1 | 143

            Console.Write($"= {sum}, hasil Bagi = ");
            
            double hasilBagiFibo = sum / jumlah;// 143:10 =14,3
            Console.WriteLine(hasilBagiFibo);

            //ambil sum khusus genap
            double sumGenap = 0;//tempat penyimpanan Genap
            for (int j = 2; j <= jumlah * 2; j += 2) // j < 20 | mencari nilai batas maks array
            { 
                int a = j;// dimulai dari angka looping == 1;
                sumGenap += j; //kelipatan 2 sampai index 10x | 2,4,6,8,10,12,14,16,18,20
                Console.Write(a + " "); // 2 4 6 8 10 12 14 16 18 20
            }
            Console.Write($"= {sumGenap}, hasil bagi = ");
            double Genap = sumGenap / jumlah;//110:10 = 11
            Console.WriteLine(Genap);

            //ambil sum ganjil
            double sumGanjil = 0;//tempat penyimpanan Ganjil
            for (int k = 1; k < jumlah * 2; k += 2)// k < 19 | mencari nilai batas maks array
            {
                int b = k; // dimulai dari angka looping == 1;
                sumGanjil += k;//kelipatan 2 sampai index 10x | 2,4,6,8,10,12,14,16,18,20
                Console.Write(b + " "); // 1 3 5 7 9 11 13 15 17 19
            }
            Console.Write($" = {sumGanjil} hasil bagi = ");
            double bagiGanjil = sumGanjil / jumlah;//100:10 = 10
            Console.WriteLine(bagiGanjil);
        }
        static void soal8()
        {
            Console.Write("Input = "); //153,3 // output 9
            string[] input = Console.ReadLine().Split(",");

            int angkaPertama = int.Parse(input[0].ToString().Substring(0, 1));
            int angkaKedua = int.Parse(input[0].ToString().Substring(1, 1));
            int angkaKetiga = int.Parse(input[0].ToString().Substring(2, 1));
            int banyakAngka = int.Parse(input[1].ToString());

            int total = (angkaPertama + angkaKedua + angkaKetiga) * banyakAngka;
            string totalString = total.ToString();

            int hasilAkhir = int.Parse(totalString.Substring(0, 1)) + int.Parse(totalString.Substring(1, 1));
            Console.WriteLine(hasilAkhir);
        }

        
        static void soal9()
        {
            Console.Write("Beli Pulsa = ");
            int pulsa = int.Parse(Console.ReadLine());
            int point = 0;
            int rangePoint1 = 0;
            int rangePoint2 = 0;

            if (pulsa <= 10000)
            {
                point = 0;
            }
            if (pulsa <= 30000)
            {
                rangePoint1 = (pulsa - 10000) / 1000;
                point = rangePoint1;
            }
            else
            {
                rangePoint1 = (pulsa - 10000) / 1000;
                if (rangePoint1 >= 20)
                {
                    rangePoint1 = 20;
                }
                rangePoint2 = (pulsa - 30000) / 1000 * 2;
                point = rangePoint1 + rangePoint2;
            }
            Console.WriteLine($"Point Anda adalah = {point} point");
        }
        static void soal10() //belum
        {
            //double x = 1, y = 1, z = 0, i, jumlah;
            Console.Write("Masukan angka : ");
            string [] jumlah = Console.ReadLine().Split(","); //153|3 jadi gini
            //int[] hargaMENU1 = Array.ConvertAll(jumlah, int.Parse);
            char[] hargaMENU = jumlah.ToString().ToCharArray();
            //char [] hargaMENU = jumlah2.ToCharArray();
            //int[] hargaMENU = Array.ConvertAll(jumlah2, int.Parse);


            int sum = 0;
            for (int i = 0; i < hargaMENU.Length; i++) // LOOPING (0 - 3)
            {
                sum += hargaMENU[i];// DARI [I] ARRAY KE i IKUTI LOOPING (0-3| SESUAI LENGTH ARRAY NYA = 3) 12000 + 20000 + 9000 + 15000 |total = 56000
            }
            int jumlah1 = sum;
            Console.WriteLine($"test {jumlah1}");
            //string[] arrNilai = Console.ReadLine().Split(","); //73673833


            /*double[] nilai = new double[arrNilai.Length];//73673833
            for (int i = 0; i < arrNilai.Length; i++)
            {
                nilai[i] = double.Parse(arrNilai[i]);//0|73 > 1|67 > 2|38 > 3|33
            }
            double[] nilaiBaru = new double[nilai.Length];
            for (int i = 0; i < nilai.Length; i++) //0|73 > 1|67 > 2|38 > 3|33 length = 4
            {
                
                    nilaiBaru[i] += nilai[i]; 
                    Console.WriteLine(nilaiBaru[i]);
                
               
            }
            Console.WriteLine("");*/

            /*for (int i = 0; i < nilai.Length; i++) //0|73 > 1|67 > 2|38 > 3|33 length = 4
            {
                for (int j = 0; j <= jumlah.Length; j ++)
            {
                double index = double.Parse(jumlah[j]);
                index += jumlah[j];
                
                Console.WriteLine(index);
            }*/


        }
    }
}
