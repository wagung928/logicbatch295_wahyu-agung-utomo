create table tblPengarang (
	id int primary key identity (1,1),
	Kd_Pengarang varchar (7) not null,
	nama varchar(30) not null,
	alamat varchar(80) not null,
	kota varchar (15) not null,
	kelamin varchar (1) not null
)
select * from tblPengarang

insert into tblPengarang
(Kd_Pengarang, nama, alamat, kota, kelamin) values
('P0001','Ashadi','Jl.Beo 25','Yogya','P'),
('P0002','Rian','Jl.Solo 123','Yogya','P'),
('P0003','Suwadi','Jl.Semangka','Bandung','P'),
('P0004','Siti','Jl.Durian 15','Solo','W'),
('P0005','Amir','Jl.Gajah 33','Kudus','P'),
('P0006','Suparman','Jl.Harimau 25','Jakarta','P'),
('P0007','Jaja','Jl.Singa 7','Bandung','P'),
('P0008','Saman','Jl.Naga 12','Yogya','P'),
('P0009','Anwar','Jl.Tidar 6A','Magelang','P'),
('P0010','Fatmawati','Jl.Renjana 4','Bogor','W')

select * from tblPengarang
--kunci disini
create table tblGaji (
id int primary key identity (1,1),
id_tblPengarang int not null,
Kd_Pengarang varchar (7) not null,
nama varchar (30) not null,
gaji decimal(18,4) not null)
--
select * from tblGaji
--drop table tblGaji
insert into tblGaji 
(id_tblPengarang, Kd_Pengarang, nama, gaji) values
(1,'P0002', 'Rian',600000),
(2,'P0005', 'Amir',700000),
(3,'P0004', 'Siti',500000),
(4,'P0003', 'Suwadi',1000000),
(5,'P0010', 'Fatmawati',600000),
(6,'P0008', 'Saman',750000)

select * from tblGaji

--Soal-soal
--1. Hitung dan tampilkan jumlah pengarang dari table tblPengarang.
--jawaban :
select count(Kd_Pengarang) as jml_Pengarang
from tblPengarang

--2. Hitunglah berapa jumlah Pengarang Wanita dan Pria
--jawaban:
select count (kelamin) as jml_Pengarang,kelamin
from tblPengarang
group by kelamin

--3. Tampilkan record kota dan jumlah kotanya dari table tblPengarang.
--jawaban:
select count (kota) as jml_Kota,kota
from tblPengarang
group by kota

--4. Tampilkan record kota diatas 1 kota dari table tblPengarang.
--jawaban:
select count (kota) as jml_Kota,kota
from tblPengarang
group by kota
having count (kota) > 1

--5. Tampilkan Kd_Pengarang yang terbesar dan terkecil dari table tblPengarang.
--jawaban:
select * from tblPengarang where Kd_Pengarang = 'P0001' or Kd_Pengarang = 'P0010'
--JAWABAN 2
select min (Kd_Pengarang) as tabel_Min, max (Kd_Pengarang) as table_Max from tblPengarang

--6. Tampilkan gaji tertinggi dan terendah.
--jawaban:
select * from tblGaji where gaji = 1000000 or gaji = 500000
--JAWABAN 2
select max (gaji) as Gaji_Max, min (gaji) as Gaji_Min from tblGaji

--7. Tampilkan gaji diatas 600.000.
--jawaban:
select * from tblGaji where gaji !=500000

--8. Tampilkan jumlah gaji.
--jawaban:
select sum (gaji) as Total_Gaji from tblGaji

--9. Tampilkan jumlah gaji berdasarkan Kota
--jawaban:

select kota, sum (gaji) as total_Gaji_Kota from tblPengarang as p
join tblGaji as g
on p.Kd_Pengarang = g.Kd_Pengarang
group by kota

--JAWABAN KEDUA
select  peng.Kota, sum(gaj.gaji) as jml_gaji 
from tblPengarang as peng
inner join tblGaji as gaj
on peng.kd_pengarang = gaj.kd_pengarang
group by peng.Kota


--10. Tampilkan seluruh record pengarang antara P0001-P0006 dari tabel pengarang.
--jawaban: 

select * from tblPengarang where id <=6
--jawaban kedua
select * from tblPengarang where Kd_Pengarang between 'P0001' and 'P0006' --untuk interval

--select * from tblPengarang as p
--join tblGaji as g
--on p.id = g.id_tblPengarang

--11. Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang.
--jawaban:

select count (kota) as jml_Kota,kota
from tblPengarang 
where kota = 'Yogya' or kota = 'Solo' or kota = 'Magelang'
group by kota

--untuk seluruh kota
select count (kota) as jml_Kota,kota
from tblPengarang
group by kota

--12. Tampilkan seluruh data yang bukan yogya dari tabel pengarang.
--jawaban:
select count (kota) as jml_Kota,kota
from tblPengarang where kota != 'Yogya'
group by kota

--13. Tampilkan seluruh data pengarang yang nama (dapat digabungkan atau terpisah):
--a. dimulai dengan huruf [A]
--b. berakhiran [i]
--c. huruf ketiganya [a]
--d. tidak berakhiran [n]
--jawaban:
--a
select * from tblPengarang where nama like 'a%' order by id asc
--b
select * from tblPengarang where nama like '%i' order by id asc
--c
select * from tblPengarang where nama like '__a%' order by id asc
--d
select * from tblPengarang where nama not like '%n' order by id asc

--14. Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama
--jawaban:

--Answer 1
select * from tblPengarang as p
join tblGaji as g
on p.Kd_Pengarang = g.Kd_Pengarang
--Answer 2
select p.Kd_Pengarang,p.nama,p.alamat,p.kota,p.kelamin,g.gaji from tblPengarang as p
join tblGaji as g
on p.Kd_Pengarang = g.Kd_Pengarang
--Answer 3
select p.*, g.gaji from tblPengarang as p --diaturnya semua field colom adalah di setelah select dan sebelum from
join tblGaji as g
on p.Kd_Pengarang = g.Kd_Pengarang

--15. Tampilan kota yang memiliki gaji dibawah 1.000.000
--jawaban:

select p.kota, g.gaji from tblPengarang as p
join tblGaji as g
on p.Kd_Pengarang = g.Kd_Pengarang
where gaji != 1000000

--16. Ubah panjang dari tipe kelamin menjadi 10
--jawaban:
alter table tblPengarang alter column kelamin varchar (10)

--17. Tambahkan kolom [Gelar] dengan tipe Varchar (12) pada tabel tblPengarang
--jawaban:
alter table tblPengarang add Gelar varchar (12)

--18. Ubah alamat dan kota dari Rian di table tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekanbaru
--jawaban:
update tblPengarang set alamat = 'Jl. Cendrawasih 65', kota = 'Pekan Baru'  where nama = 'Rian' --ATAU NAMA where id = 2
-- set set alamat = 'Jl. Cendrawasih 65', setelah koma bisa lagi set kota = 'Pekan Baru'. (TANPA PEMBERIAN NAMA SET LAGI)

--19. Buatlah view untuk attribute Kd_Pengarang, Nama, Kota dengan nama vwPengarang
--jawaban:

--create view
create view vw_Pengarang as
select p.Kd_Pengarang, p.nama, p.kota
from tblPengarang as p
inner join tblGaji as g
on p.id = g.id_tblPengarang
--cek
select * from vw_Pengarang







--18. Ubah alamat dan kota dari Rian di table tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekanbaru

update tblPengarang set alamat = 'Jl. Cendrawasih 65', kota = 'PekanBaru' where nama = 'Rian'
select * from tblPengarang

--19. Buatlah view untuk attribute Kd_Pengarang, Nama, Kota, Gaji dengan nama vwPengarang

create view vwPengarang as -- sekali sudah di set di folder view pada object explorer maka syntax ini tidak bisa di pakai kembali untuk update pada select * from -- bisa dilakukan ketika syntax ini di copy dan di buat di tabel query baru dan bisa baru di update kembali
select p.Kd_Pengarang, p.nama, p.kota,g.gaji
from tblPengarang as p join tblGaji as g
on p.id = g.id_tblPengarang

select * from vwPengarang

select * from tblPengarang
update tblPengarang set Gelar = 'S1'