create table tblArtis (
id int primary key identity (1,1),
	kd_artis varchar (100) not null,
	nm_artis varchar(100) not null,
	jk varchar(100) not null,
	bayaran decimal (18,2) not null,
	award int not null,
	negara varchar (100) not null
)
select * from tblArtis
--drop table tblArtis
insert into tblArtis
(kd_artis, nm_artis, jk, bayaran, award, negara) values
('A001','ROBERT DOWNEY JR','PRIA','3000000000','2','AS'),
('A002','ANGELINA JOLIE','WANITA','70000000','2','AS'),
('A003','JACKIE CHAN','PRIA','20000000','2','HK'),
('A004','JOE TASLIM','PRIA','350000000','2','ID'),
('A005','CHELSEA ISLAN','WANITA','30000000','2','ID')

select * from tblArtis
--kunci disini
create table tblFilm (
id int primary key identity (1,1),
kd_film varchar (10) not null,
nm_film varchar (55) not null,
genre varchar (55) not null,
artis varchar (55) not null,
pruduser varchar (55) not null,
pendapatan decimal (18,2) not null,
nominasi int not null)

--drop table tblFilm
select * from tblFilm

insert into tblFilm
(kd_film,nm_film,genre,artis,pruduser,pendapatan,nominasi) values
('F001','IRON MAN','G001','A001','PD01','2000000000','3'),
('F002','IRON MAN 2','G001','A001','PD01','1800000000','2'),
('F003','IRON MAN 3','G001','A001','PD01','1200000000','0'),
('F004','AVENGER CIVIL WAR','G001','A001','PD01','2000000000','1'),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01','1300000000','0'),
('F006','THE RAID','G001','A004','PD03','800000000','5'),
('F007','FAST AND FORIOUS','G001','A004','PD05','830000000','2'),
('F008','HABIBIE DAN AINUN','G004','A005','PD03','670000000','4'),
('F009','POLICE STORY','G001','A003','PD02','700000000','3'),
('F010','POLICE STORY 2','G001','A003','PD02','710000000','1'),
('F011','POLICE STORY 3','G001','A003','PD02','615000000','0'),
('F012','RUSH HOUR','G003','A003','PD05','695000000','2'),
('F013','KUNGFU PANDA','G003','A003','PD05','923000000','5')

select * from tblFilm

create table tblProduser (
id int primary key identity (1,1),
kd_produser varchar (50) not null,
nm_produser varchar (50) not null,
international varchar (50) not null)

--drop table tblFilm
select * from tblProduser

insert into tblProduser
(kd_produser,nm_produser,international) values
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PARAMOUNT PICTURE','YA')

--drop table tblFilm
select * from tblProduser

create table tblNegara (
id int primary key identity (1,1),
kd_negara varchar (100) not null,
nm_negara varchar (100) not null)

--drop table tblFilm
select * from tblNegara

insert into tblNegara
(kd_negara,nm_negara) values
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')

--drop table tblFilm
select * from tblNegara


create table tblGenre (
id int primary key identity (1,1),
kd_genre varchar (50) not null,
nm_genre varchar (50) not null)

--drop table tblFilm
select * from tblGenre

insert into tblGenre
(kd_genre,nm_genre) values
('G001','ACTION'),
('G002','HOROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

--drop table tblFilm
select * from tblGenre


--SOAL
--drop table tblGenre
select * from tblArtis
select * from tblFilm
select * from tblProduser
select * from tblNegara
select * from tblGenre
--1. Menampilkan jumlah pendapatan produser marvel secara keseluruhan
--jawaban:
select nm_produser,sum (pendapatan) as jumlah_pendapatan_Marvel from tblArtis as tA
join tblFilm as tF on tA.kd_artis = tF.artis
join tblProduser as tP on tF.pruduser = tP.kd_produser
group by nm_produser having nm_produser = 'MARVEL'

--2. Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi
--jawaban:
select tF.nm_film, tF.nominasi from tblFilm as tF
where tF.nominasi = '0'

--3. Menampilkan nama film dan pendapatan yang paling tinggi
--jawaban: INI PENTING UNTUK SUB QUERY MATERI
-- ini query di dalam query atau disebut sub query
--mencari maksimal data film pendapatan
select * from tblFilm where
pendapatan = (select max (pendapatan) from tblFilm)
-- mencari maksimal data nominasi film
select * from tblFilm where
nominasi = (select max (nominasi) from tblFilm)

-- jawaban ke dua
select nm_film, max (pendapatan) as Pendapatan_Max from tblFilm
group by nm_film having nm_film = 'IRON MAN'

--4. Menampilkan nama film yang huruf depannya 'p'
--jawaban:
--huruf depannya
select * from tblFilm where nm_film like 'p%' order by id asc

--5. Menampilkan nama film yang huruf terakhir 'y'
--jawaban:
--huruf terakhir
select * from tblFilm where nm_film like '%y' order by id asc

--6. Menampilkan nama film yang mengandung huruf 'd'
--jawaban:
--mengandung huruf d
select * from tblFilm where nm_film like '%d%' order by id asc

--7. Menampilkan nama film dan artis
--jawaban: berdasarkan kode artisnya
select * from tblArtis
select * from tblFilm
select f.nm_film, a.nm_artis from tblFilm as f
join tblArtis as a on f.artis = a.kd_artis

--8. Menampilkan nama film yang artisnya berasal dari negara hongkong
--jawaban:
select * from tblFilm
select * from tblNegara
select * from tblArtis

select nm_film as Nama_Film, negara as Negara  from tblArtis as a
join tblFilm as f on a.kd_artis = f.artis
join tblNegara as neg on neg.kd_negara = a.negara
where kd_negara ='HK'

--9. Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf 'o'
--jawaban:
select * from tblFilm
select * from tblNegara
select * from tblArtis

select nm_film as Nama_Film, nm_negara as Negara  from tblArtis as a
join tblFilm as f on a.kd_artis = f.artis
join tblNegara as neg on neg.kd_negara = a.negara
where nm_negara not like '%o%'


--10. Menampilkan nama artis yang tidak pernah bermain film
--jawaban:
select * from tblArtis -- ini ada
select * from tblFilm -- ini ada


select nm_artis as Nama_Artis from tblArtis as a
left join tblFilm as f -- ini berdasarkan posisinya
on a.kd_artis = f.artis
where f.artis is null

--11. Menampilkan nama artis yang bermain film dengan genre drama
--jawaban:
select * from tblArtis -- ini ada
select * from tblFilm -- ini ada
select * from tblGenre -- ini ada


select nm_artis as Nama_Artis, nm_genre as Nama_Genre, nm_film as Nama_Film from tblArtis as a
join tblFilm as f on a.kd_artis = f.artis
join tblGenre as g on g.kd_genre = f.genre
where kd_genre = 'G004'

--12. Menampilkan nama artis yang bermain film dengan genre Action
--jawaban:

select * from tblArtis -- ini ada
select * from tblFilm -- ini ada
select * from tblGenre -- ini ada


select nm_artis as Nama_Artis, nm_genre as Nama_Genre
from tblArtis as a
join tblFilm as f
on a.kd_artis = f.artis
join tblGenre as g 
on g.kd_genre = f.genre
where nm_genre = 'ACTION'
group by a.nm_artis,g.nm_genre

--13. Menampilkan data negara dengan jumlah filmnya
--jawaban: BELUM
select * from tblFilm
select * from tblNegara
select * from tblArtis -- ini ada

select kd_negara, nm_negara, count (nm_film)as julmah  from tblArtis as a
join tblFilm as f on a.kd_artis = f.artis -- on ini berlaku HANYA UNTUK INISIALISASI KESAMAAN KODE YANG TERDAPAT PADA KE DUA TABEL YANG DIBUAT JOIN
right join tblNegara as neg on neg.kd_negara = a.negara -- left/right join berdasarkan dari inputan tbl pertama pasti masuk ke arah kiri dan tbl selanjutnya akan masuk ke arah kanan, jika di join (liat dulu posisi pertanyaannya apakah yang di cari dari join punya right/left)
group by kd_negara,nm_negara

--14. Menampilkan nama film yang skala internasional
--jawaban:

select * from tblFilm
select * from tblProduser

select nm_film from tblFilm as f
join tblProduser as p
on f.pruduser = p.kd_produser
where international = 'YA'

--15. Menampilkan jumlah film dari masing2 produser
--jawaban:

select * from tblFilm
select * from tblProduser

select nm_produser, count (nm_film) as jml_Film 
from tblFilm as f
right join tblProduser as p
on f.pruduser = p.kd_produser
group by nm_produser -- karena dia hanya di butuhkan jumlah film nya saja
-- maka tidak perlu di group by menggunakan nm_produsernya



-- Day 2
create view vw_pembayaran_artis as -- sum tidak bisa masuk ketika belum di masukkan ke temporary (temp)
select *,
case -- cari pajak
when bayaran >= 500000000 then bayaran * 0.2
when bayaran < 500000000 and bayaran > 200000000 then bayaran * 0.1
else 0
end as pajak,

case -- cari total pembayaran
when bayaran >= 500000000 then bayaran - (bayaran *0.2)
when bayaran < 500000000 and bayaran > 200000000 then bayaran - (bayaran *0.1)
else bayaran
end as Total_Bayaran
from tblArtis

select sum(Total_Bayaran) as Total_Bayaran from vw_pembayaran_artis --ini bisa di lakukan setelah ada create vw yang dimana ini adalah temp



