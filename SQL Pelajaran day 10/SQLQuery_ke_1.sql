--Create database batch_295

--create table karyawan (
--	id int primary key identity (1,1),
--	nama varchar(100) not null,
--	alamat varchar(255) not null,
--	email varchar (50),
--	gender varchar (1),
--	no_hp varchar (15)
--)

--alter table karyawan add status varchar (20)
--alter table karyawan alter column status varchar (50)
--alter table karyawan drop column status

--drop table karyawan 

--select * from karyawan
--select nama, alamat from karyawan

--insert into karyawan 
--(nama, alamat, email, gender, no_hp) values
--('Irfan','jakarta','wagung991@gmail.com','M','081212753891'),
--('Raisa','jakarta','wagung992@gmail.com','F','081212753892'),
--('Maman','jakarta','wagung993@gmail.com','M','081212753893'),
--('Raka','jakarta','wagung994@gmail.com','M','081212753894'),
--('Rahayu','jakarta','wagung995@gmail.com','F','081212753895'),
--('Inggrit','jakarta','wagung996@gmail.com','F','081212753896'),
--('Anis','jakarta','wagung997@gmail.com','F','081212753897'),
--('Tahta','jakarta','wagung998@gmail.com','M','081212753898'),
--('Gilang','jakarta','wagung910@gmail.com','M','081212753810'),
--('Munir','jakarta','wagung911@gmail.com','M','081212753811'),
--('Vikcy','jakarta','wagung912@gmail.com','M','081212753812')



--delete from karyawan where id = 12 and name = Irfan
--delete from karyawan where id >= 2 and id <=5
--delete from karyawan where id between 2 and 5

--update karyawan set alamat = 'jakarta Timur' where id = 1
--update karyawan set alamat = 'jakarta Pusat' where id >= 5 and id <= 7


select * from karyawan
select top 3 * from karyawan order by id desc
select top 3 * from karyawan order by id asc

select * from karyawan where nama like 'w%' order by id asc
select * from karyawan where nama like '%n' order by id asc
select * from karyawan where nama like '%it' order by id asc
select * from karyawan where nama like '%an%' order by id asc
select * from karyawan where nama not like '%an%' order by id asc

select count(gender) as jml_gender, gender
--from karyawan where gender = 'F'
from karyawan 
group by gender
having count (gender) = 4


select * from karyawan

create table cuti (
id int primary key identity (1,1),
id_karyawan int not null,
jumlah_cuti int not null,
ambil_cuti int not null,
alasan varchar (50))

insert into cuti (id_karyawan, jumlah_cuti, ambil_cuti, alasan) values
(1, 12,2, 'liburan'),
(2,12,1,'Sakit'),
(3,12,3,'Pulang Kampung'),
(1, 12,1,'Sakit')

select * from cuti

select * from karyawan
select * from cuti

select * from karyawan as kar
inner join cuti as c
on kar.id = c.id_karyawan

--inner join
--create table cuti (
--id int primary key identity (1,1),
--id_karyawan int not null,
--jumlah_cuti int not null,
--ambil_cuti int not null,
--alasan varchar (50))
select kar.nama,c.jumlah_cuti, c.ambil_cuti,c.jumlah_cuti-c.ambil_cuti as sisa_cuti,
	   c.alasan 
from karyawan as kar
inner join cuti as c
on kar.id = c.id_karyawan

--left join
select kar.nama,c.jumlah_cuti, c.ambil_cuti,c.jumlah_cuti-c.ambil_cuti as sisa_cuti,
	   c.alasan 
from karyawan as kar
left join cuti as c
on kar.id = c.id_karyawan

--right join
select kar.nama,c.jumlah_cuti, c.ambil_cuti,c.jumlah_cuti-c.ambil_cuti as sisa_cuti,
	   c.alasan 
from karyawan as kar
right join cuti as c
on kar.id = c.id_karyawan

--left join is null
select kar.nama,c.jumlah_cuti, c.ambil_cuti,c.jumlah_cuti-c.ambil_cuti as sisa_cuti,
	   c.alasan 
from karyawan as kar
left join cuti as c
on kar.id = c.id_karyawan
where c.id is null

--right join is null
select kar.nama,c.jumlah_cuti, c.ambil_cuti,c.jumlah_cuti-c.ambil_cuti as sisa_cuti,
	   c.alasan 
from karyawan as kar
right join cuti as c
on kar.id = c.id_karyawan
where kar.id is null

--full outer join
select kar.nama,c.jumlah_cuti, c.ambil_cuti,c.jumlah_cuti-c.ambil_cuti as sisa_cuti,
	   c.alasan 
from karyawan as kar
full outer join cuti as c
on kar.id = c.id_karyawan

--full outer join is null
select kar.nama,c.jumlah_cuti, c.ambil_cuti,c.jumlah_cuti-c.ambil_cuti as sisa_cuti,
	   c.alasan 
from karyawan as kar
full outer join cuti as c
on kar.id = c.id_karyawan
where kar.id is null or c.id is null

--create view
create view vw_pengajuan_cuti as
select kar.nama,c.jumlah_cuti, c.ambil_cuti,c.jumlah_cuti-c.ambil_cuti as sisa_cuti,
	   c.alasan 
from karyawan as kar
inner join cuti as c
on kar.id = c.id_karyawan

--untuk cek sisa cuti// dan update hanya bisa di ubah di tabel aslinya// nanti ini mengikuti update aslinya
select *from vw_pengajuan_cuti
where sisa_cuti > 10 --kondisi
order by nama --shorting nama berdasarkan alfabet