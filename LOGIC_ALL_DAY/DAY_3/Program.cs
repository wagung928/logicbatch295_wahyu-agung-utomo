﻿using System;

namespace LogicDay3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string ulang = "";
            do
            {
                int pilih;
                Console.Write("pilih soal 1-10 : ");
                pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 1:
                        soal1(); // 1	3	5	7	9	11	13
                        break;
                    case 2:
                        soal2(); // 2	4	6	8	10	12	14
                        break;
                    case 3:
                        soal3(); // 1	4	7	10	13	16	19
                        break;
                    case 4:
                        soal4(); // 1	5	9	13	17	21	25
                        break;
                    case 5:
                        soal5(); // 1	5	*	9	13	*	17
                        break;
                    case 6:
                        soal6(); // 1	5	*	13	17	*	25
                        break;
                    case 7:
                        soal7(); // 2	4	8	16	32	64	128
                        break;
                    case 8:
                        soal8(); // 3	9	27	81	243	729	2187
                        break;
                    case 9:
                        soal9(); // 4	16	*	64	256	*	1024
                        break;
                    case 10:
                        soal10(); // 3	9	27	XXX	243	729	2187
                        break;
                }
                Console.ReadKey();
                Console.Write("\nPilih Soal Lagi? (y/n) : ");
                ulang = Console.ReadLine();
            }
            while (ulang.ToUpper() == "Y");
        }
        static void soal1() // 1	3	5	7	9	11	13
        {
            int n = 7;
            int angka = 1;
            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{angka} ");
                angka += 2;
            }
        }
        static void soal2() // 2	4	6	8	10	12	14
        {
            int n = 7;
            int angka = 2;
            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{angka} ");
                angka += 2;
            }
        }
        static void soal3() // 1	4	7	10	13	16	19
        {
            int n = 7;
            int angka = 1;
            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{angka} ");
                angka += 3;
            }
        }

        static void soal4() // 1	5	9	13	17	21	25
        {
            int n = 7;
            int angka = 1;
            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{angka} ");
                angka += 4;
            }
        }
        static void soal5() // 1	5	*	9	13	*	17
        {
            int n = 7;
            int angka = 1;
            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write("* ");
                }
                else
                {
                    Console.Write($"{angka} ");

                }
                angka += 4;
            }
        }
        static void soal6() // 1	5	*	13	17	*	25
        {
            int n = 7;
            int angka = 1;
            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write("* ");
                    angka += 4;
                }
                else
                {
                    Console.Write($"{angka} ");
                    angka += 4;
                }
            }
        }
        static void soal7() // 2	4	8	16	32	64	128
        {
            int n = 7;
            int angka = 2;
            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{angka} ");
                angka *= 2;
            }
        }
        static void soal8() // 3	9	27	81	243	729	2187
        {
            int n = 7;
            int angka = 3;
            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{angka} ");
                angka *= 3;
            }
        }
        static void soal9() // 4	16	*	64	256	*	1024
        {
            int n = 7;
            int angka = 4;
            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write("* ");
                }
                else
                {
                    Console.Write($"{angka} ");
                    angka *= 4;
                }
            }
        }
        static void soal10() // 3	9	27	XXX	243	729	2187
        {
            int n = 7;
            int angka = 3;
            for (int i = 1; i <= n; i++)
            {
                if (i % 4 == 0)
                {
                    Console.Write("XXX ");

                }
                else
                {
                    Console.Write($"{angka} ");

                }
                angka *= 3;
            }
        }
    }
}
