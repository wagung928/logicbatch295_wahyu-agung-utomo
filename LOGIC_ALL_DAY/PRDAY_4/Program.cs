﻿using System;
using System.Linq;

namespace PRDay4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int pilih;
            Console.Write("pilih soal 1-4 : ");
            pilih = int.Parse(Console.ReadLine());
            switch (pilih)
            {
                case 1:/*done*/ //aku sayang kamu == A*u s****g K**u
                    soal1();
                    break;
                case 2: /*done*/ //Aku Mau Makan == *k* *a* *aka*
                    soal2();
                    break;
                case 3: /*done*/ //DETEKSI PALINDROME UNTUK JIKA DI BALIK HURUF YANG TAK SESUAI MAKA == FALSE
                    soal3();
                    break;
                case 4: /*done*/ //BAJU LEBARAN ANDI DENGAN KATEGORI DI JUMLAHKAN BAJU DAN CELANA SESUAI DENGAN UANGNYA ANDI
                    soal4();
                    break;
            }
            Console.ReadKey();
        }
        static void soal1()
        {
            //aku sayang kamu == ARRAY STRING [0|1|2]
            //A*u s****g K**u == ARRAY STRING [0|1|2]

            Console.Write("Input : ");
            string input = Console.ReadLine();
            string[] katakata = input.Split(' ');                                   // KARENA INI TIDAK MENGGUNAKAN PENJUMLAHAN MELAIN HANYA UNTUK MEMBERIKAN MARK/TANDA (*), MAKANYA HANYA MENGGUNAKAN STRING DENGAN ARRAY[]
                                                                                    // DENGAN DI SPLIT TANDA (' ') AGAR TANDA TERSEBUT TIDAK MEMILIKI NILAI INDEX NYA SENDIRI
            foreach (string kata in katakata)                                       // MENGGUNAKAN FOREACH INI ADALAH MENDETEKSI SELURUH ARRAY YANG ADA NAMUN, TIDAK BISA MEMISAHKAN ATAU MENGAMBIL SATU-SATU JIKA INGIN  DI JUMLAHKAN
            {                                                                       // KALAU HANYA UNTUK MENGUBAH ATAU MEREPLACE DI SETIAP PERULANGAN YANG ADA DIA BISA MELAKUKANNYA DENGAN FUNGSI FOREACH INI

                for (int i = 0; i < kata.Length; i++)
                {
                    if (i == 0 || i == kata.Length - 1)                             // JADI SETIAP LOOPING == AMBIL STRING ARRAY KE [0] DAN AMBIL PJG KATA ARRAY [0] TERSEBUT SEBELUM ARRAY KATA TERAKHIR
                    {                                                               // CETAKLAH SESUAI PERINTAH
                        Console.Write(kata[i]);
                    }
                    else
                    {
                        Console.Write("*");
                    }
                }
                Console.Write(" ");                                                 // SETELAH KATA PERTAMA DIA AKAN DI PISAHKAN MELALUI SPASI YANG DI BUAT KEMBALI
            }
        }
        static void soal2() //Aku Mau Makan == *k* *a* *aka* // INI ADALAH KEBALIKAN PERINTAH DARI PENJELASAN NO 1
        {
            Console.Write("Input : ");
            string input = Console.ReadLine();
            string[] katakata = input.Split(' ');
            foreach (string kata in katakata)
            {

                for (int i = 0; i < kata.Length; i++)
                {
                    if (i == 0 || i == kata.Length - 1)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(kata[i]);
                    }
                }
                Console.Write(" ");
            }
        }
        static void soal3() //DETEKSI PALINDROME UNTUK JIKA DI BALIK HURUF YANG TAK SESUAI MAKA == FALSE
        {
            Console.Write("Masukan string untuk di cek apakah polindrome : ");                      // CONTOH INPUT = "MAKAN"
            string input = Console.ReadLine().ToLower();
            string hasil = string.Empty;                                                            // INI ARTINYA SAMA DENGAN STRING = "";
                                                                                                    // SEBAGAI TEMPAT PENYIMPANAN STRING PADA VARIABEL HASIL YANG MASIH KOSONG
            for (int i = input.Length - 1; i >= 0; i--)
            {                                                                                       // INI UNTUK LOOPING PEMBALIKAN YANG DI BERIKAN AWAL INPUT BARU DI TULIS ULANG PADA PENYIMPANAN HASIL DI LUAR LOOPING (FOR) 
                hasil += input[i];                                                                  // MAKA OUTPUT LOOPING DARI INPUT DI ATAS AKAN MENJADI == "NAKAM"
            }

            // BARU SETELAH HASIL PERPUTARAN LOOPING TADI MASUK KE KONDISI UNTUK DETEKSI APAKAH SESUAI ATAU TIDANYA SUATU INPUTAN

            if (input == hasil)
            {
                Console.WriteLine($"{input}, yes, ini merupakan Palindrome");
            }
            else
            {
                Console.WriteLine($"{input}, no, ini bukan merupakan Palindrome");
            }
        }
        static void soal4() //BAJU LEBARAN ANDI DENGAN KATEGORI DI JUMLAHKAN BAJU DAN CELANA SESUAI DENGAN UANGNYA ANDI
        {
            // PENTING INI CARA UNTUK ANALISIS SOAL BENTUK STRING YANG BERSIFAT PENJUMLAHAN DAN WAJIB DI ARRAY DAN DI KONVERSI KE INT ARRAY

            // UANG ANDI : 78
            // HARGA BAJU : 35,40,50,20 // INI NANTI TAMBAH DENGAN BAWAHNYA SESUAI DENGAN ARRAY INDEX NYA YANG DI PERINTAHKAN OLEH LOOPING DAN KONDISI
            // HARGA CELANA : 40,30,45,10
            // OUTPUT : 75

            Console.Write("Input : ");
            int input = int.Parse(Console.ReadLine());

            /*input string tidak langsung (use:arrayLama)*/ // CARA MENCARI STRING CONVERT LOGIC AGAR INGIN DI HITUNG TP MENGGUNAKAN BANTUAN VARIABEL (use:arrayLama)

            Console.Write("Harga Celana : ");
            string hargaCelana = (Console.ReadLine());
            string[] arrayLama = hargaCelana.Split(",");                                // AGAR NILAI TANDA Split(",") TIDAK MEMILIKINYA
            int[] hrgCelana = Array.ConvertAll(arrayLama, int.Parse);                   // KARENA INGIN DI HITUNG SETIAP MASING2 INPUTANNYA YANG BERSIFAT STRING TADI MAKA WAJIB DI CONVERT SEPERTI INI

            /*input string langsung (not use : arrayLama) */ // CARA MENCARI STRING CONVERT LOGIC AGAR INGIN DI HITUNG TP TIDAK MENGGUNAKAN BANTUAN VARIABEL (not use : arrayLama)

            Console.Write("Harga Baju : ");
            string[] hargaBaju = Console.ReadLine().Split(",");
            int[] hrgBaju = Array.ConvertAll(hargaBaju, int.Parse);                     // konversi kesini agar string yang sebelumnya memiliki index sendiri dan bisa di jumlahkan

            int max = 0;                                                                // TEMPAT PENYIMPANAN INT YANG BERSIFAT PENJUMLAHAN DI HASILKAN DARI LOOPING NYA || DAN DIAMBIL NILAI PENJUMLAHAN AKHIR NYA
            for (int i = 0; i < hrgBaju.Length; i++)
            {                                                                           // TEMPAT SIMPANNYA BERADA DIDALAM SINI AGAR LANGSUNG DI EKSEKUSI
                int temp = hrgCelana[i] + hrgBaju[i];                                   // hrgCelana[i] + hrgBaju[i] SESUAI ARRAY LOOPINGANDAN DIAMBIL ANGKA DARI HASIL CONVERT INT MASING-MASING YANG SUDAH DI BUAT SEBELUM FOR
                if (temp <= input)                                                      // INPUT DISINI UNTUK NILAI INPUTAN YANG KITA MASUKKAN (NILAI BARU)
                {
                    if (max < temp)
                    {
                        max = temp;                                                     // MAKA NILAI MAX YANG DI ATAS AKAN DI REPLACE DENGAN NILAI EKSEKUSI INT TEMP DIDALAM FOR INI
                    }
                }
            }
            Console.Write($" Kamu dapat membeli baju dan celana dengan harga {max}");
        }
    }
}

