﻿using System;
using System.Linq;
namespace TugasDay5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string ulang = "";
            do
            {
                int pilih;
                Console.Write("pilih soal 1-5 : ");
                pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 1:/*done*/ //TIME CONVERSION 12 JAM = 07:05:45PM == 19:05:45 dan 12:00:10AM == 00:00:10
                        soal1();
                        break;
                    case 2: /*done*/ //SISA UANG ELSA BERDASARKAN INPUTAN YANG DIPERLUKAN
                        soal2();
                        break;
                    case 3: /*done*/ //PERULANGAN FOR DENGAN MENGGUNAKAN DIAGONAL DIFERENCE
                        soal3();
                        break;
                    case 4: /*done*/ //LILIN DI TIUP BERDASARKAN PERULANGAN YANG DI INPUTKAN
                        soal4();
                        break;
                    case 5: /*done*/ //perpindahan angka depan awal ke akhir belakang
                        soal5();
                        break;
                    case 6: /*done*/ //perpindahan angka akhir belakang kedepan
                        soal6();
                        break;
                }
                Console.ReadKey();
                Console.Write("\nPilih Soal Lagi? (y/n) : ");
                ulang = Console.ReadLine();
            }
            while (ulang.ToUpper() == "Y");
        }

        static void soal1() //TIME CONVERSION 12 JAM
        {
            /*07:05:45PM dan 12:00:10AM
                19:05:45 dan 00:00:10*/
            Console.Write("Input Waktu :  ");
            string inputWkt = Console.ReadLine().ToUpper();                 //toupper untuk strong huruf
            string[] detailWkt = inputWkt.Split(":");                       // index [0] = 07
            string pMaM = inputWkt.Substring(8, 2);                         // untuk kondisi (if) PM,AM = (index ke-8,ambil 2 Karakter)
            int jam = int.Parse(detailWkt[0]);                              //jam yg di liat dari aray [0] = {07}. INI ADALAH CARA MENGHITUNG WAKTU DAN CARA PENULISAN INPUT NYA

            //jangan di ubah menit dan detik termasuk pada (:)
            string titik2MenitDetik = inputWkt.Substring(2, 6);              // :09:08 = (index ke-2,ambil 6 karakter). DAN INI ANGKA DUA NYA BERDASARNYA DARI INDEX CHAR// BUKAN ARRAY
            if (pMaM == "PM")
            {
                jam += 12;
                if (jam >= 24)
                {
                    jam -= 24;
                }
                Console.Write((jam) + titik2MenitDetik);
            }
            else if (pMaM == "AM")
            {
                if (jam == 12)
                {
                    jam -= 12;
                }

                Console.Write((jam) + titik2MenitDetik);
            }
        }
        static void soal2() //SISA UANG ELSA BERDASARKAN INPUTAN YANG DIPERLUKAN
        {
            //total menu =4
            //index makanan alergi 1
            //harga menu = 12000,20000,9000,15000 |total = 56000 // HANYA INI YG STRING DAN DI CONVERT KE ARRAY INT SETELAHNYA
            //uang elsa = 20000

            Console.Write("Total Menu : ");
            int menu = int.Parse(Console.ReadLine());
            Console.Write("Total Makanan alergi: ");
            int makananAlergi = int.Parse(Console.ReadLine());                  // INI NANTI AKAN MENENTUKAN INT ARRAY PADA HARGA MENU
            Console.Write("Uang Elsa : ");
            int uang = int.Parse(Console.ReadLine());
            Console.Write("Harga Menu : ");
            string[] hargaMenu = (Console.ReadLine().Split(","));               //AWAL HARGA MENU BENTUK STRING DALAM ARRAY DAN DI SPLIT (",") AGAR TANDA (",") TIDAK MEMILIKI INDEX SENDIRI
            int[] hargaMENU = Array.ConvertAll(hargaMenu, int.Parse);           //DI CONVERSI KE INT DARI SETIAP HARGA MENU DI STRING ARRAY // KARENA MAU DIMASUKAN KE (PENJUMLAHAN)

            int sum = 0;
            for (int i = 0; i < hargaMENU.Length; i++)                          // LOOPING (0 - 3)
            {
                sum += hargaMENU[i];                                            // DARI [I] ARRAY KE i IKUTI LOOPING (0-3| SESUAI LENGTH ARRAY NYA = 3) 12000 + 20000 + 9000 + 15000 |total = 56000
            }
            int jumlah = sum - hargaMENU[makananAlergi];
            int total = jumlah / 2;
            int sisaUang = uang - total;
            Console.WriteLine($"elsa harus membayar {total}");

            if (sisaUang == 0)
            {
                Console.Write("Uang Elsa Pas");
            }
            else
            {
                Console.WriteLine($"sisa uang elsa = {sisaUang}");
                if (sisaUang < 0)
                {
                    int pinjamUang = total - uang;
                    Console.WriteLine($"wajib minjam = {pinjamUang}");
                }
            }
            Console.Write("\n");
        }
        static void soal3() //PERULANGAN FOR DENGAN MENGGUNAKAN DIAGONAL DIFERENCE
        {
            int[,] data = new int[,]
            {
                {11,2,4 },  // ROW 1
                {4,5,6 },   // ROW 2
                {10,8,-12 } // ROW 3
            };
            int temp = 0;                                                       // PENYIMPANAN NILAI PERTAMA NYA
            int temp1 = 0;                                                      // PENYIMPANAN NILAI KEDUANYA
            for (int i = 0; i < 3; i++)                                         // LOOPING (0-1-2)
            {
                for (int j = 0; j < 3; j++)                                     // NANTI DATA DI TABEL DI SEDIAKAN DI LOOPING INI DAN DI AMBIL BERDASAR KAN DATA ARRAY 2 DIMENSINYA SAJA
                {
                    if (i == j)
                    {
                        temp += data[i, j];                                     // LOOPING (0-1-2) YG MENGISI TEMP == 0 + 11 + 5 + (-12) = 4, JADI YG DIAMBIL HASIL PENJUMLAHANNYA SAJA == 4
                    }
                    if (j == 3 - i - 1)
                    {
                        temp1 += data[i, j];                                    // LOOPING (0-1-2) YG MENGISI TEMP1 == 0 + 4 + 5 + 10 = 19, JADI YG DIAMBIL HASIL PENJUMLAHANNYA SAJA == 19
                    }
                }
            }
            Console.WriteLine($"Perbedaan Diagonal = {temp - temp1}");          // 4 - 19 = -15
        }
        static void soal4() //LILIN DI TIUP BERDASARKAN PERULANGAN YANG DI INPUTKAN
        {
            string lilin;
            int total = 0;

            Console.Write("Tinggi Lilin : ");
            lilin = Console.ReadLine();
            string[] Lilin = lilin.Split(' ');                              //MAKA GUNAKAN CONVERT INT DAN STRING[] (WAJIB DI ARRAY) DAN DI SPLIT (" ") SESUAI JENIS DATA INPUTANNYA
            int[] konversiLilin = Array.ConvertAll(Lilin, int.Parse);       // KRN BENTUK STRING DAN INGIN DI JUMLAH, MAKA GUNAKAN CONVERT INT DAN STRING [] (WAJIB DI ARRAY)

            int tinggiLilin = konversiLilin.Max();                          //LILIN YANG DI TIUP DI AMBIL NILAI MAXNYA
            for (int i = 0; i < konversiLilin.Length; i++)
            {
                if (tinggiLilin == konversiLilin[i])
                {
                    total++;
                }
            }
            Console.WriteLine($"Lilin yang di tiup adalah {total}");

        }
        static void soal5() //perpindahan angka depan awal ke akhir belakang// INI TIDAK MENGGUNAKAN PENJULAHAN MAKA TIDAK HARU SDI CONVERT KE ARRAY INT DENGAN TANDA ([])
        {
            //5,6,7,0,1 arr // rot = 2
            //6,7,0,1,5 // 7,0,1,5,6 == memiliki array length == 5, DAN KARENA (i/j) di mulai dari == 0 maka gunakan (Array.Length - 1)
            //======perpindahan angka depan awal ke akhir belakang======

            Console.Write("Array input: ");                                 //5,6,7,0,1 arr // rot = 2
            string[] array = (Console.ReadLine().Split(","));               // STRING ARRAY INI AKAN KE UPDATE SESUAI KONDISI ROTASI DAN LOOPING YG DIBERIKAN == (6,7,0,1) + BARU MASUKSTRING TEMP = ""; == 5
            Console.Write("rotasi : ");                                     //JADI RORATE 2x =  6,7,0,1,5
            int rotate = int.Parse(Console.ReadLine());
            string temp = "";                                               // tempat save untuk nilai temp baru dari hasil looping for

            for (int j = 0; j < rotate; j++)                                // berdasarkan j = input rorate
            {
                for (int i = 0; i < array.Length - 1; i++)                  // berdasarkan string [] yang di buat array agar bisa di baca array, dan Length - 1, agar nilai index terbaca semua dan tidak melewatinya
                {                                                           // array.Length - 1 ==>(index|input) 0|5,1|6,2|7,3|0,4|1 ==> karena total array length nya = 5, serta di mulainya index deteksi dari (0/1/2/3/4)
                    temp = array[i];                                        // karena angka inputan awalnya = (5,6,7,0,1)
                    array[i] = array[i + 1];                                // yang di ambil dari (index i BUKAN index j)
                    // 0+0|5 = 0+1|6
                    // 1+0|6 = 1+1|7
                    // 2+0|7 = 2+1|0
                    // 3+0|0 = 3+1|1
                    // 4+0|1 = 4+1|? karena 5 indexnya tidak ada maka GUNAKAN DARI STRING TEMP = "";
                    // MENGELUARKAN DARI STRING TEMP = ""; . MAKA JADI ANGKA DIBELAKANG == 0+0|5 DI TARUH PADA (array.Length - 1) YG TERDAPAT PADA FOR
                    array[i + 1] = temp;
                    //0+1|6 = save ke string temp = ""; di atas
                    //1+1|7 = save ke string temp = ""; di atas
                    //2+1|0 = save ke string temp = ""; di atas
                    //3+1|1 = save ke string temp = ""; di atas
                    //0+0|5 = save ke string temp = ""; di atas DI TARUH PADA (array.Length - 1) YG TERDAPAT PADA FOR
                    // maka nilai awal string temp = ""; == 6,7,0,1,5
                }
                foreach (string s in array)
                {                                                           //cetak berdasarkan perintah di dalam for (array[i + 1] = temp)
                                                                            // dan yang sudah di simpan oleh perintah string temp = "";
                                                                            // yang berisi nilai index dan sudah di convert ke angka inputan
                                                                            // isi nya == 6,7,0,1,5
                    Console.Write($"{s} ");
                }
                Console.WriteLine();
            }
        }
        static void soal6() //perpindahan angka akhir belakang kedepan
        {

            //5,6,7,0,1 arr // rot = 2
            //1,5,6,7,0 // 0,1,5,6,7

            Console.Write("input : ");
            string input = Console.ReadLine();
            int[] arr = Array.ConvertAll(input.Split(","), int.Parse);                          //hasil dari string input di cari nilai indexnya dgn convertALL input nya punya string ke integer dan di bentuk array [] trs di split (",")
            // 5|6|7|0|1
            Console.Write("Rotasi : ");
            int rot = int.Parse(Console.ReadLine()); //rotasi 2x

            //mencari loopingnya

            while (rot != 0) //rot = 2
            {
                int temp = arr[arr.Length - 1];                                                 // sbg tempat penyimpanan index angka belakang dari 5|6|7|0|1 yaitu (1) di save
                for (int i = 0; i < arr.Length - 1; i++)
                //menghindari eror pada penjumlahan maka (i < arr.Length - 1)
                // maka length = 5 - 1 = (4)
                {
                    arr[arr.Length - 1 - i] = arr[arr.Length - 2 - i];
                    // 5|6|7|0|1 ini array indexnya  (5|0i)(6|1i)(7|2i)(0|3i)(1|4i)

                    // arr [arr.Length - 1] = [5-1-0] = arr[arr.Length - 2 - i] = [5-2-0] di timpa sesuai batas looping (i < arr.Length - 1) i| 0
                    // [4] = [3] >>> (1 = 0) di timpa
                    // arr [arr.Length - 1] = [5-1-1] = arr[arr.Length - 2 - i] = [5-2-1] di timpa sesuai batas looping (i < arr.Length - 1) i| 1
                    // [3] = [2] >>> (0 = 7) di timpa
                    // arr [arr.Length - 1] = [5-1-2] = arr[arr.Length - 2 - i] = [5-2-2] di timpa sesuai batas looping (i < arr.Length - 1) i| 2
                    // [2] = [1] >>> (7 = 6) di timpa
                    // arr [arr.Length - 1] = [5-1-3] = arr[arr.Length - 2 - i] = [5-2-3] di timpa sesuai batas looping (i < arr.Length - 1) i| 3
                    // [1] = [0] >>> (6 = 5) terakhir ini di timpa
                    // dan posisi array yang [0] berisi angka (5 tadi sudah di oper ke 6), maka
                    // nilai pada posisi arr [0] tadi jadi "kosong" lalu for berhenti
                    // kemudian masuk ke arr[0] = temp yang di luar dari for dan di timpa ke arr[0]
                }
                arr[0] = temp;                                                                  // hasil dari simpanan temp = (1|i dari hasil [arr.Length - 1])
                Console.WriteLine(string.Join(",", arr));                                       // disatukan berdasarkan hasil output dari kondisi yang di hasilkan
                rot--;                                                                          // kemudian rotasi akan berkurang dari setiap inputan rotasi kita yg => (2>1>0) dan for akan ke reset lagi dengan nilai inputan dari hasil yang terupdate
            }
        }
    }
}
