﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TugasDay6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string ulang = "";
            do
            {
                int pilih;
                Console.Write("pilih soal 1-5 : ");
                pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 1:/*done*/ //MENGHITUNG LEMBAH BERDASARKAN JUMLAH TOTAL STRING U/D DI SOAL
                        soal1();
                        break;
                    case 2: /*done*/ //ALFABET ASLI DI RATATE = 2x DAN DI CARI INDEX ALFABET BARUNYA BERDASARKAN INDEX SETIAP ALFABET OARIGINAL DAN ALFABET ROTATE
                        soal2();
                        break;
                    case 3: /*done*/ //input == dia DAN DI CARI BERDASARKAN INDEX // 1 3 1 4 6 2 1 1 3 5 2 3 1 1 1 1 5 2 3 1 3 5 4 3 2 5
                        soal3();
                        break;
                    case 4: /*done*/ //DETEKSI HURUF VOKAL DAN KONSONAN PADA INPUT TEKS DAN SIFATNYA DI UBAH KE CHAR DAN BUAT KE ARRAY CHAR
                        soal4();
                        break;
                    case 5: /*done*/ //DETEKSI PASSWORD KUAT/LEMAH/KURANG DARI SEGI HURUF/ANGKA/SIMBOL
                        soal5();
                        break;
                }
                Console.ReadKey();
                Console.Write("\nPilih Soal Lagi? (y/n) : ");
                ulang = Console.ReadLine();
            }
            while (ulang.ToUpper() == "Y");
        }
        static void soal1() //MENGHITUNG LEMBAH BERDASARKAN JUMLAH TOTAL STRING U/D DI SOAL
        {
            int batas = 0;
            int lembah = 0;
            Console.Write("masukan input = ");
            string input = Console.ReadLine();

            for (int i = 0; i < input.Length; i++)
            {
                string a = input[i].ToString();                 //[i] sifat dia char (character) dan di konversi ke string maka menggunakan ToString();
                if (a == "D")                                   // disini pun maka inisialisasi (a) => harus bersifat string juga dengan tanda ("");
                {
                    batas--;
                }
                else if (a == "U")
                {
                    batas++;
                }
                if (a == "D" && batas == -1)
                {
                    lembah++;
                }
            }
            Console.Write(lembah);
        }


        static void soal2() //ALFABET ASLI DI RATATE = 2x DAN DI CARI INDEX ALFABET BARUNYA BERDASARKAN INDEX SETIAP ALFABET OARIGINAL DAN ALFABET ROTATE
        {
            Console.Write("Masukan Kalimat = ");
            string kalimat = Console.ReadLine();

            Console.Write("rotasi : ");
            int rotate = int.Parse(Console.ReadLine());

            string alfabetAsli = "abcdefghijklmnopqrstuvwxyz";
            string kalimatBaru = "";
            string alfabetBaruAsli = "";
            string simpan_alfabet = alfabetAsli;

            for (int i = 0; i < rotate; i++)
            {
                alfabetBaruAsli = simpan_alfabet.Substring(1, simpan_alfabet.Length - 1) + simpan_alfabet[0];   //+ simpan_alfabet[0] untuk menimpa nilai char yang pertama kan itu = (a) maka bcd-z kedepan dan + a
                simpan_alfabet = alfabetBaruAsli;                                                               // simpan huruf char pertama dari hasil looping ke deklarasi sinpan_alfabet
            }
            foreach (char huruf in kalimat)
            {
                int indeks = alfabetAsli.IndexOf(huruf);                                                        // huruf-indexOF-alfabetasli == huruf yang terdeteksi berdasarkan input dan dan diliat dari nilai index char nya (0)
                if (indeks != -1)                                                                               // jika indeks dari input alfabetAsli = -1 sama dengan kondisi tersebut
                {
                    kalimatBaru += alfabetBaruAsli[indeks];
                }
                else
                {
                    kalimatBaru += huruf;
                }
            }
            Console.WriteLine($"Alphabet = {alfabetAsli}");
            Console.WriteLine($"Alphabet Baru = {alfabetBaruAsli}");
            Console.WriteLine($"Kalimat Baru = {kalimatBaru}");
        }


        static void soal3() //input == dia DAN DI CARI BERDASARKAN INDEX // 1 3 1 4 6 2 1 1 3 5 2 3 1 1 1 1 5 2 3 1 3 5 4 3 2 5
        {
            Console.Write("Input Tinggi: ");
            int[] tinggi = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);          // deteksi soal itu adalah angka, maka convert array, kemudian di split agar mendapatkan indexnya, dan di parse karena di awal sifatnya console input itu string
            Console.Write("input Text Kalimat : ");
            string textKalimat = Console.ReadLine().ToLower();

            //buat deteksi untuk huruf yang nantinya akan di input dan di detec dengan data yang tersedia
            string alfabet = "abcdefghijklmnopqrstuvwxyz";
            // buat tempat penyimpanan dengan jenis List untuk inputan huruf baru
            List<int> simpan = new List<int>();

            //BUAT PERULANGAN UNTUK CEK TEXT KALIMAT AGAR INDEXNYA KEDETEKSI PADA TABEL STRING ALFABET YANG TERSEDIA
            for (int i = 0; i < textKalimat.Length; i++)
            {
                char huruf = textKalimat[i];                                                    //UNTUK detek dari inputan textKalimat pada index ke [i] di buat dalam variabel HURUF DENGAN JENIS CHAR
                int indeks = alfabet.IndexOf(huruf);                                            // untuk membuat deteksi BARU dari hasil ALFABET YG TERSEDIA DAN DIDETEKSI INDEXNYA MILIK SI HURUF yang bersifat char tadi
                int nilai = tinggi[indeks];                                                     // ini untuk deteksi di setiap INDEKX PADA TINGGI 
                simpan.Add(nilai);

                //cetak
                Console.WriteLine($"Index Alfabet {huruf} = {indeks}, maka index ke {indeks} di elemen tinggi bernilai = {nilai}");
            }

            //buat elemen baru lagi untuk deteksi PANJANG/Length dan nilai MAX
            int lenght = textKalimat.Length;
            int max = simpan.Max();
            int hasil = max * lenght;
            Console.WriteLine($"{max} x {lenght} = {hasil}");
            //string tidak memerlukan perhitungan, jika perlu perhitungan WAJIB DIUBAH KE INT
        }
        static void soal4() //DETEKSI HURUF VOKAL DAN KONSONAN PADA INPUT TEKS DAN SIFATNYA DI UBAH KE CHAR DAN BUAT KE ARRAY CHAR
        {

            //CARA MENGGUNAKAN LIST PADA HURUF
            char[] konsonan = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
            char[] vokal = "aiueo".ToCharArray();

            Console.Write("input text: ");
            string text = Console.ReadLine().ToLower();                                     // setelah di input dibikin all-in semua kecil
            List<Char> vocc = new List<char>();                                             //  bkin list dengan sifat char kosong
            List<Char> konson = new List<char>();                                           //  bkin list dengan sifat char kosong

            for (int i = 0; i < text.Length; i++)                                           // kita gunakan text apa yg kita input
            {
                int index = Array.IndexOf(vokal, text[i]);                                  // karena untuk mengisi index = yang akan menjadi array tp diambil dari array pada (vokal bentuk Array (char) yang sudah disediakan, dan string text pada index [i] sesuai dengan looping
                if (index != -1)                                                            // ini berlaku (index = -1) pada bentuk index HURUF sesuai apa yang ia detek
                {
                    vocc.Add(text[i]);                                                      // ini VOKAL (vocc) pada List item yang masih kosong akan bertambah inputan baru untuk huruf namun berdasarkan index [i]
                }
                else
                {
                    konson.Add(text[i]);                                                    // yang ini pada KONSONAN (konson) Lis bentuk char posisi awal yang kosong diatas
                }
            }
            vocc.Sort();                                                                    // hasil dari kondisi if pada vocc di rapihkan dan di urutkan berdasarkan alfabet
            konson.Sort();                                                                  // yang ini jg sama merapihkan/urutkan seperti vocc
            Console.WriteLine($"Huruf Vokal = " + String.Join("", vocc));                   // setelah dirapihkan dari sort LALU DIGUNAKAN STRING.JOIN DALAM KONDISI KOSONG, agar huruf di setiap Konsonan/Vocc. Sort ke input di dalamnya dan di tempelkan semua(JANGAN LUPAKAN FORMAT INPUTNYA SESUAI dengan functionnya)
            Console.WriteLine("Huruf Konsonan = " + String.Join("", konson));
        }
        static void soal5() //DETEKSI PASSWORD KUAT/LEMAH/KURANG DARI SEGI HURUF/ANGKA/SIMBOL
        {

            //CARA 1 MENDETEKSI PASSWORD
            Console.WriteLine();
            Console.Write("Masukan Password: ");                                            //karena bersifat kombinan dari misal : 1@3jA+# ini, gunakan string agar bisa kebaca semua
            string password = Console.ReadLine();
            int pjgPass = password.Length;
            string simbol = "~!@#$%^&*()_+-={}[]|<>?/:;";
            string angka = "1234567890";
            string upHuruf = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string lowHuruf = "abcdefghijklmnopqrstuvwxyz";

            // wajib membuat inisialisasi untuk membuati boolean ketika passwor masuk
            int simbol_simbol = 0;
            int angka_angka = 0;
            int hurufBesar = 0;
            int hurufKecil = 0;
            bool length = true;                                                             // digunakan ini agar kondisinya lebih spesifik lagi dalam medeteksinya

            //TAHAP 1 DETEKSI
            if (pjgPass <= 6)                                                               // karena batasannya akan kebaca jika kondisi ==6
            {
                length = false;                                                             // akan masuk ke nilai bool diatas
            }
            // TAHAP 2 DETEKSI
            for (int i = 0; i < pjgPass; i++)                                               // akan masuk ke perulangan jika passwordnya di input mau itu 6 atau lebih
            {
                if (lowHuruf.IndexOf(password[i]) > -1)                                     // dia membaca dari kondisi string lowHuruf yang tersedia dan deteksi dari NILAI INDEX string awal input password
                {
                    hurufKecil++;
                }
                if (upHuruf.IndexOf(password[i]) > -1)                                      // INI AKAN SELALU MEMBACA BERDASARKAN HURUF DENGAN INDEX AWAL 0, JIKA BUKAN MAKA -1
                {
                    hurufBesar++;
                }
                if (angka.IndexOf(password[i]) > -1)                                        // jika muncul IF MASING2 IF lagi maka dia akan cek semua satu2 dan BEDA DENGAN ELSE IF
                {
                    angka_angka++;
                }
                if (simbol.IndexOf(password[i]) > -1)
                {
                    simbol_simbol++;
                }
            }                                                                               // setelah SEMUA sudah di kondisikan berdasarkan jenis type variabel nya maka buat kondisi CETAK TAMPILANNYA

            //TAHAP 3 DETEKSI untuk membaca di kedua KONDISI ATASS YANG SUDAH SPESIFIK
            if (length == true && hurufKecil != 0 && hurufBesar != 0 && simbol_simbol != 0 && angka_angka != 0)
            {
                Console.WriteLine("Password Kuat");
            }
            if (length == false)
            {
                Console.WriteLine("Password Lemah & kurang dari 6 digit");
            }
            if (simbol_simbol == 0)
            {
                Console.WriteLine("Password Lemah & kurang Simbol");
            }
            if (angka_angka == 0)
            {
                Console.WriteLine("Password Lemah & kurang Angka");
            }
            if (hurufBesar == 0)
            {
                Console.WriteLine("Password Lemah & kurang Huruf Besar");
            }
            if (hurufKecil == 0)
            {
                Console.WriteLine("Password Lemah & kurang Huruf Kecil");
            }
        }

    }
}
