﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimulasiUjian
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string ulang = "";
            do
            {
                int pilih;
                Console.Write("pilih soal 1-10 : ");
                pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 1:
                        soal1();
                        break;
                    case 2:
                        soal2();
                        break;
                    case 3:
                        soal3();
                        break;
                    case 4:
                        soal4();
                        break;
                    case 5:
                        soal5();
                        break;
                    case 6:
                        soal6();
                        break;
                    case 7:
                        soal7();
                        break;
                    case 8:
                        soal8();
                        break;
                    case 9:
                        soal9();
                        break;
                    case 10:
                        soal10();
                        break;
                }
                Console.ReadKey();
                Console.Write("\nPilih Soal Lagi? (y/n) : ");
                ulang = Console.ReadLine();
            }
            while (ulang.ToUpper() == "Y");
        }

        static void soal1()
        { //AkuSayangKamuTapiKamu == output 5 (bisa char bisa string) ada 2 cara

            Console.WriteLine("input : ");
            char[] kalimat = Console.ReadLine().ToCharArray();
            int x = 0;
            foreach (char kata in kalimat)                                  // karena sifatnya char langsung saja gunakan foreach
            {
                if (kata.ToString() == kata.ToString().ToUpper())           // untuk mencek data agar dirinya sendiri di imbangi
                {
                    x += 1;
                }
            }
            Console.Write($"Output = {x}");
        }
        static void soal2()
        {
            //Start = 10	Start : 100
            //End = 25    End = 105
            //output = XA-07082022-00010	XA-07082022-00100

            string xsis = "XA";
            int input, end;

            DateTime tgl = new DateTime(2022, 08, 07, 00, 00, 00);                  //ini karena bentuk (int) dan bentuknya itu inputan langsung terbaca
            Console.WriteLine(tgl);

            Console.WriteLine("Masukan input : ");
            input = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan batas :");
            end = int.Parse(Console.ReadLine());

            int date = tgl.Day;                                                     // karena ini bentuk int untuk tgl
            int month = tgl.Month;
            int year = tgl.Year;

            for (int i = input; i <= end; i++)                                      // i nya akan mengikuti berdasarkan dengan inputan yang di masukan
            {
                string baru = "00000" + i;                                          // jika input = 20 maka baru (00000) + 20
                baru = baru.Substring(i.ToString().Length, 5);

                //baru => di ambil berdasarkan rumus (substring) yg dimana (i = jenis inputannya (int) wajib di convert ke string == (ToString))
                //lalu masuk ke length, dimana length ini berdasarkan (char).
                //jadi jika inputan i = 20, maka 20 ini sudah string = "20" (dimana lenght = 2 (total charnya)
                //sehingga isi dari Substring(i.ToString().Length,5); menjadi
                //Substring(20(int).ToString("20").Length(2 = dari "20"), dan 5 (banyak char yang diambil));
                //Substring(2,5) ini hasilnya

                Console.WriteLine($"{xsis}-0{date}{month}{year}-{baru}");
            }
        }

        static void soal3()
        {
            //Keranjang 1 = Kosong /0
            //Keranjang 2 = 3
            //Keranjang 3 = 10
            //Keranjang 1 dibawa ke pasar

            //output
            //Sisa Buah = 13
            int output = 0;
            Console.Write("Keranjang 1 = ");
            string keranjang1 = Console.ReadLine();
            Console.Write("Keranjang 2 = ");
            string keranjang2 = Console.ReadLine();
            Console.Write("Keranjang 3 = ");
            string keranjang3 = Console.ReadLine();

            // awal bentuk string ini akan di deteksi berdasarkan index string, dan jika ingin di jumlah kan akan di konvert ke integer

            Console.Write("Keranjang yg di bawa = ");
            int keranjangygdibawa = int.Parse(Console.ReadLine());
            if (keranjang1.ToLower() == "kosong")                                   // jika dia bentuk inputannya kosong maka hasilnya di buat string 0
            {
                keranjang1 = "0";
            }
            if (keranjang2.ToLower() == "kosong")
            {
                keranjang2 = "0";
            }
            if (keranjang3.ToLower() == "kosong")
            {
                keranjang3 = "0";
            }

            if (keranjangygdibawa == 1)
            {
                output = int.Parse(keranjang2) + int.Parse(keranjang3);             // setelah hasilnya string 0 sampai nilai inputan dengan bentuk string "0"
                                                                                    //baru di konvert lagi ke arah integer kembali dari yang bawaannya itu adalah string "0" dan angka dst...
            }
            if (keranjangygdibawa == 2)
            {
                output = int.Parse(keranjang1) + int.Parse(keranjang3);
            }
            if (keranjangygdibawa == 3)
            {
                output = int.Parse(keranjang3) + int.Parse(keranjang1);
            }
            Console.WriteLine($"sisa buah = {output}");
        }
        static void soal4()
        {
            Console.Write("Laki Dewasa = ");
            int lakiDewasa = int.Parse(Console.ReadLine());
            Console.Write("Wanita Dewasa = ");
            int wanitaDewasa = int.Parse(Console.ReadLine());
            Console.Write("Anak-anak = ");
            int anak = int.Parse(Console.ReadLine());
            Console.Write("Bayi = ");
            int bayi = int.Parse(Console.ReadLine());
            Console.Write("Input baju untuk = ");
            int pilih = int.Parse(Console.ReadLine());
            int total = 0;

            switch (pilih)
            {
                case 1:
                    Console.Write("Laki Dewasa = ");
                    int pilih1 = int.Parse(Console.ReadLine());
                    total = lakiDewasa + (wanitaDewasa * 2) + (anak * 3) + (bayi * 5) + pilih1;
                    break;
                case 2:

                    Console.Write("Wanita Dewasa = ");
                    int pilih2 = int.Parse(Console.ReadLine());
                    total = lakiDewasa + (wanitaDewasa * 2) + (anak * 3) + (bayi * 5) + (pilih2 * 2);
                    break;

                case 3:
                    Console.Write("Anak-anak = ");
                    int pilih3 = int.Parse(Console.ReadLine());
                    total = lakiDewasa + (wanitaDewasa * 2) + (anak * 3) + (bayi * 5) + (pilih3 * 3);
                    break;

                case 4:

                    Console.Write("Bayi = ");
                    int pilih4 = int.Parse(Console.ReadLine());
                    total = lakiDewasa + (wanitaDewasa * 2) + (anak * 3) + (bayi * 5) + (pilih4 * 5);
                    break;
            }

            int totalBaru = 0;

            if (total > 10 && total % 2 > 0)
            {
                totalBaru = total;
            }

            Console.WriteLine($"Total Baju = {totalBaru} Buah");
        }
        static void soal5()
        {
            //73,67,38,33
            Console.Write("Masukan nilai : ");
            string[] input = Console.ReadLine().Split(',');
            int[] nilai = Array.ConvertAll(input, int.Parse);
            int simpan = 0;
            for (int i = 0; i < nilai.Length; i++)
            {
                simpan = nilai[i];                              //INPUTAN AWALNYA dan di save
                                                                // hasil ini tidak akan sama dengan perulangan dari while, dan selalu akan menyimpan input baru berdasarkan integer yang sudah di convert ke array
                if (nilai[i] > 35)                              // 33 mental keluar langsung cetak
                {
                    while (nilai[i] % 5 != 0)                   //73|++|75      67|++|70     38|++|40
                    {
                        nilai[i]++;
                    }
                    if (nilai[i] - simpan < 3)                  //nilai ke update dari while ==> (75 - 73 = 2 < 3) ||| (40 - 38 = 2 < 3) DARI (KEDUA WHILE INI) AKAN KE UPDATE BERDASARKAN KONDISI
                    {
                        simpan = nilai[i];                      // nilai ini AKAN KE UPDATE BERDASARKAN WHILE-NYA
                    }
                    else if (nilai[i] - simpan >= 3)            // DARI WHILE ==> (70 - 67 = 3) |||  DARI (WHILE INI) AKAN KE UPDATE BERDASARKAN KONDISI
                    {
                        nilai[i] = simpan;                      // nilai ini tetap sama dengan inputannya AWALNYA
                    }
                }
                Console.WriteLine(nilai[i]);                    //nilai ini akan dicetak berdasarkan kondisi yang di atas
            }
        }
        static void soal6()
        {
            //A quick brown fox jumps over the lazy dog
            //Check back tomorrow I Will see if the book has arrived

            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Console.Write("Input = ");
            string input = Console.ReadLine().ToUpper();

            int hasil = 0;

            for (int i = 0; i < alphabet.Length; i++)               // ini megikuti dari bacaan dari alphabet yang sudah di dbuat untuk di deteksi/ dan berdasarkan lenght ketentuan dari alfabet = 26
            {
                int indexHuruf = input.IndexOf(alphabet[i]);        // cek apakah input sudah ada di dalam index alphabet// jika ada (nilai indexnya = 0)
                                                                    // setelah nambah looping nya maka di cari nilai alfabet tadi yaitu huruf B/C/D dst...
                if (indexHuruf > -1)                                // harus 0 indexnya
                {
                    hasil += 1;
                }
            }
            Console.WriteLine(hasil);
            if (hasil == 26)                                        // wajib harus komplit semua huruf yang tersedia di alfabet tersedia semua baru masuk kondisi ini
            {
                Console.WriteLine("Kalimat ini adalah Pangram");
            }
            else
            {
                Console.WriteLine("Kalimat ini bukan Pangram");
            }
        }
        static void soal7()
        {
            //-  input      : Masukkan maksimal himpunan : 10
            //-  fibonaci   : 1,1,2,3,5,8,13,21,34,55 sum = 143 avg = 14,3
            //-  output     : Genap : 2 ,4,6,8,10,12,14,16,18,20 sum = 110 avg = 11
            //-  output     : Ganjil: 1,3,5,7,9,11,13,15,17,19 sum = 100 avg = 10

            int x = 1, y = 1, z = 0, i, jumlah;
            Console.Write("Masukan angka : ");
            jumlah = int.Parse(Console.ReadLine());
            Console.Write($"{x} {y} ");                     // cetak angka pertama dan kedua menggunakan spasi
            for (i = 2; i < jumlah; ++i)
            {
                z = x + y;
                Console.Write(z + " ");
                x = y;
                y = z;
            }
            double sum = x + y + z - 1;                     //1,1,2,3,5,8,13,21,34,55 = 144-1 | 143

            Console.Write($"= {sum}, hasil Bagi = ");

            double hasilBagiFibo = sum / jumlah;            // 143:10 =14,3
            Console.WriteLine(hasilBagiFibo);

            //ambil sum khusus genap
            double sumGenap = 0;                            //tempat penyimpanan Genap
            for (int j = 2; j <= jumlah * 2; j += 2)        // j < 20 | mencari nilai batas maks array
            {
                int a = j;                                  // dimulai dari angka looping == 1;
                sumGenap += j;                              //kelipatan 2 sampai index 10x | 2,4,6,8,10,12,14,16,18,20
                Console.Write(a + " ");                     // 2 4 6 8 10 12 14 16 18 20
            }
            Console.Write($"= {sumGenap}, hasil bagi = ");
            double Genap = sumGenap / jumlah;               //110:10 = 11
            Console.WriteLine(Genap);

            //ambil sum ganjil
            double sumGanjil = 0;                           //tempat penyimpanan Ganjil
            for (int k = 1; k < jumlah * 2; k += 2)         // k < 19 | mencari nilai batas maks array
            {
                int b = k;                                  // dimulai dari angka looping == 1;
                sumGanjil += k;                             //kelipatan 2 sampai index 10x | 2,4,6,8,10,12,14,16,18,20
                Console.Write(b + " ");                     // 1 3 5 7 9 11 13 15 17 19
            }
            Console.Write($" = {sumGanjil} hasil bagi = ");
            double bagiGanjil = sumGanjil / jumlah;         //100:10 = 10
            Console.WriteLine(bagiGanjil);
        }
        static void soal8()
        {
            int[] input = Array.ConvertAll("153,3".Split(","), int.Parse);          // pecah dulu ilangin tanda koma dengan split "," //153|3 == 0|1 array

            int x = 0;
            int y = 0;

            //tujuannya di pecah dulu ke arah char agar dapat semua angka yang di perlukan

            foreach (char a in input[0].ToString())                                 // convert string dulu karena inoput itu bentuknya integer
            {
                x += int.Parse(a.ToString());                                       // convert kembali ke string dulu karena a itu char, kemudian di ubah ke int parse
            }

            int z = x * input[1];                                                   // ,3 yang di belakang 153,3 di ambil
            if (z > 9)                                                              // (z>9) itu keterangan ouput 1 digit yang di soal
            {
                foreach (char b in z.ToString())                                    // karena b bentuknya char
                {
                    y += int.Parse(b.ToString());                                   // karena bentuk nya char maka dia harus di ubah ke string dan int parse dulu makanya bisa di jumlahkan
                }
                Console.WriteLine(y);
            }
            else
                Console.WriteLine(x);
        }
        static void soal9()
        {
            Console.Write("input :");
            int input = int.Parse(Console.ReadLine());
            int point = 0;
            int point2 = 0;
            if (input <= 10000)
            {
                Console.WriteLine("Point = 0");
            }
            if (input > 10000 && input <= 30000)
            {
                input -= 10000;
                point = input / 1000;
                Console.WriteLine($"Point = 0 + {point}");
            }
            if (input > 30000)
            {
                input -= 30000;
                point = 20000 / 1000;
                point2 = (input / 1000) * 2;
                Console.WriteLine($"Point = 0 + {point} + {point2} = {point + point2}");
            }
        }
        static void soal10()
        {
            Console.Write("i = ");
            int inputI = int.Parse(Console.ReadLine());
            Console.Write("j = ");
            int inputJ = int.Parse(Console.ReadLine());
            Console.Write("k = ");
            int inputK = int.Parse(Console.ReadLine());

            string temp = "";                                                       //20/22
            string temp2 = "";                                                      //21/23
            for (int i = inputI; i <= inputJ; i++)                                  //20-23 sampai
            {
                temp = i.ToString();                                                //20|21|22, karena temp bentuk nmya simpanan string maka i.ToString
                temp2 = temp[1].ToString() + temp[0].ToString();                    //02|12// ini diputar balik

                double hasil = (double.Parse(temp) - double.Parse(temp2)) % 6;      //(20-02)%6

                if (hasil == 0)
                {
                    Console.WriteLine($"{i} hari baik");                            //20|22
                }
                else
                {
                    Console.WriteLine($"{i} bukan hari baik");                      //21|23
                }
            }
        }
    }
}
