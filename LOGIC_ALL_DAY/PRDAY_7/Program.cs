﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PRDAY_7
{
    internal class Program
    {
        static void Main(string[] args)
        {
            soal1();
            Console.ReadKey();
        }
        static void soal1() //PRDay_7 ==> BATANG KAYU YANG TERSISA HARUS HABIS//
        {
            Console.Write("Input : ");                                                                  // cara mencari int ke dalam LIST dan convert untuk mengubahnya dan tujuannya utk di hitung
            int[] inputArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);              // cara mencari dan mengkonvert dari int array ke list input
            List<int> input = new List<int>(inputArray);                                                // 6 index
            input.Sort();                                                                               // di rapihkan dari angka terkecil-terbesar
            List<int> hasilSimpan = new List<int>();
            List<int> hasil = new List<int>();                                                          //simpanan kosong
            hasil.Add(input.Count);
            while (input.Count != 0)
            {
                int min = input.Min();                                                                  //5,4,4,2,2,8 == agar akan selalu di cari yang palin == min yaitu dua, makanya di taruh di luar
                for (int i = 0; i < input.Count; i++)                                                   // i<6
                {
                    int temp = input[i] - min;                                                          //5-2 =3|2|2|6 == selebih nya yang pengurangan sama dengan 0 == di hapus (masuk ke Remove)
                    input[i] = temp;
                    if (temp != 0)
                    {
                        hasilSimpan.Add(temp);                                                          // hapus berdasarkan count yang muncul
                    }
                    else
                    {
                        input.RemoveAt(temp);
                        i--;                                                                            // balik lg i++ input.count nya == 6
                    }
                }
                if (input.Count != 0)                                                                   //untuk jika kondisi ada angka 0 di akhir inputan dan dia akan di hapus
                {
                    hasil.Add(input.Count);
                }
                hasilSimpan = new List<int>();                                                          // hasil data simpan yang akan di reset dari atas
            }
            Console.WriteLine(String.Join(" ", hasil));                                                 // cara cetak khusus untuk inputan List
        }
    }
}
