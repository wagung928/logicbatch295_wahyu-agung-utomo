﻿using System;

namespace TugasDay4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int pilih;
            Console.Write("pilih soal 1-5 : ");
            pilih = int.Parse(Console.ReadLine());
            switch (pilih)
            {
                case 1:/*done*/ //SHORTING 2,5,4,1,3 == 1,2,3,4,5
                    soal1();
                    break;
                case 2: /*done*/ //BIL. PRIMA MAX 100 == 2,3,5,7,11,13... YANG DIBAGI DENGAN DIRINYA SENDIRI
                    soal2();
                    break;
                case 3: /*done*/ //HALLOWEN SALE VIDEO GAME DENGAN PEMBELIAN GAME AWAL DST DIBERIKAN KONDISI
                    soal3();
                    break;
                case 4: /*done*/ //SEGITIGA TRAPESIUM DENGAN TANDA (#)
                    soal4();
                    break;
                case 5: /*done*/ //SOSSPSSQSSOR == MENDETEKSI SINYAL YANG TIDAK SESUAI = SOS
                    soal5();
                    break;
            }
            Console.ReadKey();
        }

        static void soal1() /*2,5,4,1,3 == 1,2,3,4,5*/
        {
            
            Console.WriteLine("==Shorting==");
            int input, i, j;
            Console.Write("Masukan batas input : ");
            input = int.Parse(Console.ReadLine());
            int[] array = new int[input];                                           //MEMBUAT PENYIMPANAN ARRAY ANGKA BARU DARI INDEX
            for (i = 0; i < input; i++)                                             // maks i = 0 s/d i = 4. JIKA INPUT NYA (i == 5)
            {                                                                       // i0 = 2 / i1 = 5 / i2 = 4 / i3 = 1 / i4 = 3 (array length = 5)
                Console.Write("Masukan Angka = ");                                  // CETAK MASING2 SATU INDEX DALAM ARRAY
                array[i] = int.Parse(Console.ReadLine());                           // bentuk ini tidak bisa digunakan dengan tanda (,) == (2,5,4,1,3)
            }                                                                       // melain kan isi masing2 angka dengan looping for sampai batas input yang diberikan user
            Array.Sort(array);                                                      // angka yang di simpan di (int [] array = new int[input]). DI SHORT AUTO URUT
            for (j = 0; j < array.Length; j++)                                      // ini untuk cetaknya dari (for i atas)
            {                                                                       // j == (array length = 5) dan array yang sudah di (Array.Sort(array))
                                                                                    // Array.Sort(array) == 1/2/3/4/5
                Console.Write(array[j] + " ");                                      // jadi ini ambil dari array sinpanan di(int [] array = new int[input]) 
                                                                                    // j0 = 1 / j1 = 2 / j2 = 3 / j3 = 4 / j4 = 5
            }
        }
        static void soal2() //BIL. PRIMA MAX 100 == 2,3,5,7,11,13... YANG DIBAGI DENGAN DIRINYA SENDIRI
        {
            int bilPrima, input;
            Console.Write("Masukan bil : ");                                        // misal input = 15
            input = int.Parse(Console.ReadLine());

            for (int i = 1; i <= input; i++)                                        // i == 15
            {
                bilPrima = 0;
                for (int j = 2; j <= i / 2; j++)                                    // i/2 bergantung pada looping for pada i sampai bisa masuk kategori j
                {
                    if (i % j == 0)
                    {
                        bilPrima++;
                        break;
                    }
                }
                if (bilPrima == 0 && i != 1)                                        // i yg bukan == 1
                {
                    Console.Write($"{i}, ");                                        // setiap masuk kondisi ini maka looping i akan tercetak
                }
            }
        }
        static void soal3() //HALLOWEN SALE VIDEO GAME DENGAN PEMBELIAN GAME AWAL DST DIBERIKAN KONDISI
        {
            int p, d, m, s;
            Console.Write("Input dolar P : ");
            p = int.Parse(Console.ReadLine());
            Console.Write("Input dolar D : ");
            d = int.Parse(Console.ReadLine());
            Console.Write("Input dolar M : ");
            m = int.Parse(Console.ReadLine());
            Console.Write("Input dolar S : ");
            s = int.Parse(Console.ReadLine());
            int games = 0;
            while (p >= m && s >= p)
            {
                s -= p;
                p -= d;
                if (p < m)
                {
                    p = m;
                }
                games++;
            }
            Console.Write($"{games} Video Game");
        }
        static void soal4() //SEGITIGA TRAPESIUM DENGAN TANDA (#)
        {
            int input;
            Console.Write("Masukan input : ");
            input = int.Parse(Console.ReadLine());
            for (int i = 1; i <= input; i++)                                    // dimulai dari i = 1 dan i <= apa yang kita inputkan
            {
                for (int j = i; j <= input; j++)                                // dimulai dari (j = i) yang dimana berarti (i) akan selalu berubah pada posisi looping (j) dan (i) <= apa yang kita inputkan
                {
                        Console.Write(" ");
                }
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("#");
                }
                Console.Write("\n");
            }
        }
        static void soal5() //SOSSPSSQSSOR == MENDETEKSI SINYAL YANG TIDAK SESUAI = SOS
        {
            int x;
            string hasil = "";
            int sinyalSalah = 0;
            Console.Write("Input : ");
            string input = Console.ReadLine();                                  //karena inputannya string dan tidak di jumlahkan hanya di deteksi saja, maka bentuknya string biasa saja
            for (x = 0; x < input.Length; x += 3)
            {
                hasil = input.Substring(x, 3);

                if (hasil == "SOS")
                {
                    Console.WriteLine($"sinyal benar = {hasil} ");

                }
                else
                {
                    Console.WriteLine($"sinyal salah = {hasil} ");
                    sinyalSalah++;
                }
            }
            Console.Write($"Total Sinyal Salah = {sinyalSalah}");
        }
    }
}
