﻿using System;

namespace Function_DateTime
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DateTime date = new DateTime();
            Console.WriteLine(date);

            DateTime date1 = new DateTime(2022, 08, 12, 10, 30, 00); //Tahun, bulan, hari, jam, menit, detik
            Console.WriteLine(date1);

            DateTime dateNow = DateTime.Now;
            Console.WriteLine(dateNow);


            Console.Write("Input Tanggal (mm/dd/yyyy) : ");
            string input = Console.ReadLine(); //FORMAT = BULAN, TANGGAL, TAHUN
            DateTime dateInput = DateTime.Parse(input);
            Console.WriteLine(dateInput);
            Console.WriteLine("SET FORMAT : " + dateInput.ToString("dd MMMM yyyy")); //SET FORMAT
            //

            int year = dateNow.Year;
            int menit = dateNow.Month;
            Console.WriteLine("Tahun " + year);
            Console.WriteLine("Menit " + menit);

            //var hari = dateNow.DayOfWeek;
            var hari = (int)dateNow.DayOfWeek;// (ini) disini adalah datetime di int.Parse

            Console.WriteLine("Hari dalam integer " + hari);


            //TIMESPAN ====> INTERVAL ATAU SELISIH

            TimeSpan interval = dateInput - dateNow;
            Console.WriteLine("Interval dari hari : " + interval.Days);

            dateNow = dateNow.AddDays(1);// untuk menambah 1 hari, jadi hari ini nambah 1 hari
            Console.WriteLine(dateNow);
            dateNow = dateNow.AddDays(-5);// untuk mengurangi 5 hari, jadi hari ini dikurang 5 hari
            Console.WriteLine(dateNow);
        }
    }
}
