﻿using System;

namespace LogicDay1
{
    class Program
    {
        static void Main(string[] args)
        {
            lingkaran();
            persegi();
            mod();
            puntung_rokok();

            Console.ReadKey();
        }
        static void Biodata()
        {
            int tahun, hasil = 0;
            int tahunSekarang = 2022;
            int bulan = 0;
            int bulanSekarang = 6;

            Console.WriteLine("===Biodata===");

            Console.Write("Tuliskan nama : ");
            string nama = Console.ReadLine();

            Console.Write("masukan tahun lahir :");
            tahun = int.Parse(Console.ReadLine());

            hasil = tahunSekarang - tahun;

            Console.Write("Masukan bulan lahir :");
            bulan = int.Parse(Console.ReadLine());



            if (bulan >= bulanSekarang)
            {
                tahunSekarang = hasil - 1;

            }
            else
            {
                tahunSekarang = hasil;
            }


            Console.Write("Tuliskan alamat :");
            string alamat = Console.ReadLine();

            Console.Write("Tuliskan asal kampus :");
            string kampus = Console.ReadLine();

            Console.Write("Jurusan :");
            string jurusan = Console.ReadLine();



            Console.WriteLine("===============");

            Console.WriteLine($"Hallo,{nama} selamat datang di bootcamp batch 295");

            Console.WriteLine($"umur  anda sekarang : {tahunSekarang}");

            Console.WriteLine($"alamt : {alamat}");

            Console.WriteLine($"kampus : {kampus}");

            Console.WriteLine($"jurusan : {jurusan}");
        }


        static void lingkaran()
        {
            const double phi = 3.14;

            Console.Write("Masukan nilai r = ");
            int r = int.Parse(Console.ReadLine());

            double luas = phi * r * r;
            double kel = 2 * phi * r;

            Console.WriteLine($"Luas lingkaran ={luas}");
            Console.WriteLine($"Keliling linkaran = {kel}");
        }

        static void persegi()
        {


            Console.Write("Masukan sisi = ");
            int s = int.Parse(Console.ReadLine());

            int luasPersegi = s * s;
            int kelPersegi = 4 * s;

            Console.WriteLine($"Luas Persegi ={luasPersegi}");
            Console.WriteLine($"keliling Persegi={kelPersegi}");
        }
        static void mod()
        {
            Console.Write("Masukan angka integer = ");
            int a = int.Parse(Console.ReadLine());

            Console.Write("masukan angka pembagi = ");
            int b = int.Parse(Console.ReadLine());



            if (a % b == 0)
            {
                Console.WriteLine($"angka {a}   modulus pembagi {b} adlah 0 ");

            }
            else
            {
                int hasil = a % b;
                Console.WriteLine($"angka{a} modulus pembagi {b} adlah hasil mod {hasil} ");
            }
        }

        static void puntung_rokok()
        {
            int rokok;
            int batang;
            int sisaBatang;
            int penghasilan;

            Console.Write("berapa banyak putung yang dirangkai : ");
            rokok = int.Parse(Console.ReadLine());
            batang = (rokok / 8);
            sisaBatang = (rokok % 8);

            Console.Write($"Batang yang dapat di buat adalah = {batang}");
            Console.Write($"Sisa batangnya adalah = {sisaBatang}\n");

            //proses penghitungan penghasilan
            penghasilan = batang * 500;
            Console.WriteLine($"Penghasilan yang di dapat oleh si pemulung adalah = Rp. {penghasilan} \n");


            Console.ReadKey(true);
        }

    }
}
