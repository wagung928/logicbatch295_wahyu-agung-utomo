﻿using System;

namespace LogicDay1
{
    class Program
    {
        static void Main(string[] args)
        {
            bilangan();
            nilai();

            Console.ReadKey();
        }
        static void bilangan()

        {
            Console.WriteLine("Masukan bilangan : ");
            int bil = int.Parse(Console.ReadLine());

            if (bil % 2 == 0)
            {
                Console.WriteLine($"angka {bil} adalah genap");
            }
            else
            {
                Console.WriteLine($"angka {bil} adalah  ganjl");
            }

        }
        static void nilai()
        {
            Console.Write("Masukan Nilai : ");
            int nilai = int.Parse(Console.ReadLine());

            if (nilai >= 80)
            {
                Console.Write("Grade =A");
            }
            else if (nilai >= 60 && nilai < 80)
            {
                Console.Write("Grade =B");
            }
            else
            {
                Console.Write(" Grade =C");
            }
        }
    }
}
