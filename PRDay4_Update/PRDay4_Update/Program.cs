﻿using System;
using System.Linq;

namespace PRDay4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int pilih;
            Console.Write("pilih soal 1-10 : ");
            pilih = int.Parse(Console.ReadLine());
            switch (pilih)
            {
                case 1:/*done*/
                    soal1();
                    break;
                case 2: /*done*/
                    soal2();
                    break;
                case 3: /*done*/
                    soal3();
                    break;
                case 4: /*0done*/
                    soal4();
                    break;
            }
            Console.ReadKey();
        }
        static void soal1()
        {
            //aku sayang kamu
            //A*u s****g K**u

            Console.Write("Input : ");
            string input = Console.ReadLine();
            string[] katakata = input.Split(' ');
            foreach (string kata in katakata)
            {

                for (int i = 0; i < kata.Length; i++)
                {
                    if (i == 0 || i == kata.Length - 1)
                    {
                        Console.Write(kata[i]);
                    }
                    else
                    {
                        Console.Write("*");
                    }
                }
                Console.Write(" ");
            }
        }
        static void soal2()
        {
            Console.Write("Input : ");
            string input = Console.ReadLine();
            string[] katakata = input.Split(' ');
            foreach (string kata in katakata)
            {

                for (int i = 0; i < kata.Length; i++)
                {
                    if (i == 0 || i == kata.Length - 1)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(kata[i]);
                    }
                }
                Console.Write(" ");
            }
        }
        static void soal3()
        {
            Console.Write("Masukan string untuk di cek apakah polindrome : ");
            string input = Console.ReadLine().ToLower();
            string hasil = string.Empty;

            for (int i = input.Length - 1; i >= 0; i--)
            {
                hasil += input[i];
            }

            if (input == hasil)
            {
                Console.WriteLine($"{input}, yes ini merupakan Palindrome");
            }
            else
            {
                Console.WriteLine($"{input}, no ini bukan merupakan Palindrome");
            }
        }
        static void soal4()
        {
            int input;
            string hargaCelana;
            Console.Write("Input : ");
            input = int.Parse(Console.ReadLine());
            /*input string tidak langsung (use:arrayLama)*/
            Console.Write("Harga Celana : ");
            hargaCelana = (Console.ReadLine());
            string[] arrayLama = hargaCelana.Split(",");
            int[] hrgCelana = Array.ConvertAll(arrayLama, int.Parse);

            /*input string langsung (not use : arrayLama) */
            Console.Write("Harga Baju : ");
            string[] hargaBaju = Console.ReadLine().Split(",");
            int[] hrgBaju = Array.ConvertAll(hargaBaju, int.Parse);

            int max = 0;
            //untuk cari menggunakan max awal
            /*int[] jumlah = new int[hrgBaju.Length];*/
            for (int i = 0; i < hrgBaju.Length; i++)
            {
                int temp = hrgCelana[i] + hrgBaju[i];
                if (temp <= input)
                {
                    //untuk cari menggunakan max awal
                    /*jumlah[i] = temp;*/
                    if (max < temp)
                    {
                        max = temp;
                    }
                }
            }
            /*Console.Write($" Kamu dapat membeli baju dan celana dengan harga {jumlah.Max()}");*/
            Console.Write($" Kamu dapat membeli baju dan celana dengan harga {max}");
        }
    }
}

