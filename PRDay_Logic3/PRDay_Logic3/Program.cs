﻿using System;

namespace PRDay_Logic3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int pilih;
            Console.Write("pilih soal 1-10 : ");
            pilih = int.Parse(Console.ReadLine());
            switch (pilih)
            {
                case 1:/*done*/ //POHON FAKTOR DENGAN DETAIL AKAR PEMBAGIANNYA
                    soal1();
                    break;
                case 2: /*done*/ //MEMBUAT PERSEGI DENGAN BOLONG TENGAH SISI ANGKA DAN BINTANG
                    soal2();
                    break;
                case 3: /*done*/ // 3 * 27 * 243 * 2187
                    soal3();
                    break;
                case 4: /*done*/ // -5/ 10/ -15/ 20/ -25/ 30/ -35
                    soal4();
                    break;
                case 5: /*done*/ //FIBONACI = 1,1,2,3,4,8,13
                    soal5();
                    break;
                case 6: /*done*/ //FIBONACI = 1,1,1,3,5,9,17
                    soal6();
                    break;
            }
            Console.ReadKey();
        }
        static void soal1() //POHON FAKTOR DENGAN DETAIL AKAR PEMBAGIANNYA
        {
            int faktor, hasilBagi;
            Console.Write("Pilih input Faktor : ");
            faktor = int.Parse(Console.ReadLine());

            for (int i = 2; i <= faktor; i++)
            {
                while (faktor % i == 0)
                {
                    hasilBagi = faktor / i;
                    Console.WriteLine($"{faktor}/{i}={hasilBagi}");
                    faktor = hasilBagi;
                }
            }
        }
        static void soal2() //MEMBUAT PERSEGI DENGAN BOLONG TENGAH SISI ANGKA DAN BINTANG
        {
            int startAngkaAkhir = 5;
            int startAngkaAwal = 1;
            for (int i = 1; i <= 5; i++)
            {
                //loop kolom dan baris
                for (int j = 1; j <= 5; j++)
                {
                    if (i == 1)
                    {
                        Console.Write($"{startAngkaAwal}" + " ");
                        startAngkaAwal++;
                    }
                    else if (i == 5)
                    {
                        Console.Write($"{startAngkaAkhir}" + " ");
                        startAngkaAkhir -= 1;
                    }
                    else if (i == 2 && i == 3 && i == 4 || j == 1 || j == 5)
                    {
                        Console.Write("* ");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.Write("\n");
            }
        }
        /*int n = 5;
        for (int i = 0; i < n; i++)
        {
            //loop spasi
            for (int j = 0; j < n; j++)
            {

                if (i == 0 || i == n - 1 || j == 0 || j == n - 1)
                {
                    Console.Write("({0},{1}) ", i, j);
                }
                else
                {
                    Console.Write("  ");
                }
            }
            Console.Write("\n");
        }

        for (int i = 1; i <= n; i++)
        {
            Console.Write(i);
        }
        Console.Write("\n");
        for (int i = 0; i < n - 2; i++)
        {
            Console.Write("*");
            //spasi
            for (int j = 0; j < n - 2; j++)
            {
                Console.Write(" ");
            }
            Console.Write("*");
            Console.Write("\n");
        }
        for (int i = 1; i <= n; i++)
        {
            Console.Write(i);
        }
        Console.Write("\n");
    }*/
        static void soal3() // 3 * 27 * 243 * 2187
        {
            int n = 7;
            int angka = 3;
            for (int i = 1; i <= n; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write("* ");

                }
                else
                {
                    Console.Write($"{angka} ");

                }
                angka *= 3;
            }
        }
        static void soal4() // -5/ 10/ -15/ 20/ -25/ 30/ -35
        {
            int n = 7;
            int angka = 5;
            for (int i = 1; i <= n; i++)
            {
                if (i % 2 == 1)
                {
                    Console.Write($"-{angka} ");
                }
                else
                {
                    Console.Write($"{angka} ");
                }
                angka += 5;
            }
        }
        static void soal5() //FIBONACI = 1,1,2,3,4,8,13
        {
            int x = 1, y = 1, z, jumlah;

            Console.Write("Masukan angka fibonaci : ");
            jumlah = int.Parse(Console.ReadLine());
            Console.Write(x + " " + y + " ");
            for (int i = 2; i <= jumlah; i++)
            {

                z = x + y;
                Console.Write(z + " ");
                x = y;
                y = z;
            }
        }
        static void soal6() //FIBONACI = 1,1,1,3,5,9,17
        {

            {
                int x = 1, y = 1, z = 1, a = 1, jumlah;

                Console.Write("Masukan angka fibonaci : ");
                jumlah = int.Parse(Console.ReadLine());
                Console.Write(x + " " + y + " ");
                for (int i = 3; i <= jumlah; i++)
                {
                    Console.Write(a + " ");
                    a = x + y + z;
                    x = y;
                    y = z;
                    z = a;
                }
            }
        }
    }
}


