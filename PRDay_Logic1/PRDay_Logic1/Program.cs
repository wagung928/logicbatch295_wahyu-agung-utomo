﻿using System;

namespace PRDay_Logic1
{
    internal class Program
    {
        /*SOAL NOMER 1 PEMBERIAN GRADE NILAI A/B/C*/
        /*static void Main(string[] args)
        {
            int nilai, grade;
            Console.Write("Nilai : ");
            nilai = int.Parse(Console.ReadLine());
            grade = gradeNilai(nilai); //memanggil method parameter grade nilai

            Console.Write("Nilai : ");
            nilai = int.Parse(Console.ReadLine());
            grade = gradeNilai(nilai); //memanggil method parameter grade nilai

            Console.Write("Nilai : ");
            nilai = int.Parse(Console.ReadLine());
            grade = gradeNilai(nilai); //memanggil method parameter grade nilai
            Console.ReadKey();
        }

        //Pemberian nilai grade
        static int gradeNilai(int nilai)
        {
            if (nilai >= 80)
            {
                Console.WriteLine("Grade A");
            }
            else if (nilai >= 60 && nilai <= 80)
            {
                Console.WriteLine("Grade B");
            }
            else
            {
                Console.WriteLine("Grade C");
            }
            return nilai;
        }*/




        /*SOAL NOMER 2 PEMBERIAN NILAI GANJIL GENAP*/
        static void Main(string[] args)
        {
            int inputAngka, input;
            Console.Write("Nilai : ");
            inputAngka = int.Parse(Console.ReadLine());

            //memanggil method parameter NILAI GANJIL/GENAP
            input = Nilai(inputAngka);


            Console.Write("Nilai : ");
            inputAngka = int.Parse(Console.ReadLine());

            //memanggil method parameter NILAI GANJIL/GENAP
            input = Nilai(inputAngka);

            Console.ReadKey();
        }

        //Pemberian nilai GANJIL GENAP
        static int Nilai(int inputAngka)
        {
            if (inputAngka % 2 == 1)
            {
                Console.WriteLine($"Angka {inputAngka} adalah bilangan Ganjil");
            }
            else
            {
                Console.WriteLine($"Angka {inputAngka} adalah bilangan Genap");
            }

            return inputAngka;

        }
    }
}
