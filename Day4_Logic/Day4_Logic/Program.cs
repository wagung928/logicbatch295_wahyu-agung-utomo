﻿using System;

namespace TugasDay4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int pilih;
            Console.Write("pilih soal 1-5 : ");
            pilih = int.Parse(Console.ReadLine());
            switch (pilih)
            {
                case 1:/*done*/ //SHORTING 2,5,4,1,3 == 1,2,3,4,5
                    soal1();
                    break;
                case 2: /*done*/ //BIL. PRIMA MAX 100 == 2,3,5,7,11,13... YANG DIBAGI DENGAN DIRINYA SENDIRI
        
                    soal2();
                    break;
                case 3: /*done*/ //HALLOWEN SALE VIDEO GAME DENGAN PEMBELIAN GAME AWAL DST DIBERIKAN KONDISI
                    soal3();
                    break;
                case 4: /*done*/ //SEGITIGA TRAPESIUM DENGAN TANDA (#)
        
                    soal4();
                    break;
                case 5: /*done*/ //SOSSPSSQSSOR == MENDETEKSI SINYAL YANG TIDAK SESUAI = SOS
                    soal5();
                    break;
            }
            Console.ReadKey();
        }
        static void soal1()
        {
            /*2,5,4,1,3 == 1,2,3,4,5*/
            Console.WriteLine("==Shorting==");
            int input, i, j;
            Console.Write("Masukan batas input : ");
            input = int.Parse(Console.ReadLine());
            int[] array = new int[input]; //MEMBUAT PENYIMPANAN ARRAY ANGKA BARU DARI INDEX
            for (i = 0; i < input; i++)
            {
                Console.Write("Masukan Angka = ");
                array[i] = int.Parse(Console.ReadLine());
            }
            Array.Sort(array);
            for (j = 0; j < array.Length; j++)
            {
                Console.Write(array[j] + " ");
            }
        }
        static void soal2() //BIL. PRIMA MAX 100 == 2,3,5,7,11,13... YANG DIBAGI DENGAN DIRINYA SENDIRI
        {
            int bilPrima, input;
            Console.Write("Masukan bil : ");
            input = int.Parse(Console.ReadLine());

            for (int i = 1; i <= input; i++)
            {
                bilPrima = 0;
                for (int j = 2; j <= i / 2; j++)
                {
                    if (i % j == 0)
                    {
                        bilPrima++;
                        break;
                    }
                }
                if (bilPrima == 0 && i != 1)
                {
                    Console.Write($"{i}, ");
                }
            }
        }
        static void soal3() //HALLOWEN SALE VIDEO GAME DENGAN PEMBELIAN GAME AWAL DST DIBERIKAN KONDISI
        {
            int p, d, m, s;
            Console.Write("Input dolar P : ");
            p = int.Parse(Console.ReadLine());
            Console.Write("Input dolar D : ");
            d = int.Parse(Console.ReadLine());
            Console.Write("Input dolar M : ");
            m = int.Parse(Console.ReadLine());
            Console.Write("Input dolar S : ");
            s = int.Parse(Console.ReadLine());
            int games = 0;
            while (p >= m && s >= p)
            {
                s -= p;
                p -= d;
                if (p < m)
                {
                    p = m;
                }
                games++;
            }
            Console.Write($"{games} Video Game");
        }
        static void soal4() //SEGITIGA TRAPESIUM DENGAN TANDA (#)
        {
            int input;
            Console.Write("Masukan input : ");
            input = int.Parse(Console.ReadLine());
            for (int i = 1; i <= input; i++)
            {
                for (int j = i; j <= input; j++)
                {
                    Console.Write(" ");
                }
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("#");
                }
                Console.Write("\n");
            }
        }
        static void soal5() //SOSSPSSQSSOR == MENDETEKSI SINYAL YANG TIDAK SESUAI = SOS
        {
            int x;
            string hasil = "";
            int sinyalSalah = 0;
            Console.Write("Input : ");
            string input = Console.ReadLine();
            for (x = 0; x < input.Length - 1; x += 3)
            {
                hasil = input.Substring(x, 3);

                if (hasil == "SOS")
                {
                    Console.WriteLine($"sinyal benar = {hasil} ");

                }
                else
                {
                    Console.WriteLine($"sinyal salah = {hasil} ");
                    sinyalSalah++;
                }
            }
            Console.Write($"Total Sinyal Salah = {sinyalSalah}");
        }
    }
}
