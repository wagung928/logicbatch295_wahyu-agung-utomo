﻿using System;
using System.Linq;
namespace jawabanTugasDay5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string ulang = "";
            do
            {
                int pilih;
                Console.Write("pilih soal 1-5 : ");
                pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 1:/*done*/ //TIME CONVERSION 12 JAM = 07:05:45PM == 19:05:45 dan 12:00:10AM == 00:00:10
                        soal1();
                        break;
                    case 2: /*done*/ //SISA UANG ELSA BERDASARKAN INPUTAN YANG DIPERLUKAN
                        soal2();
                        break;
                    case 3: /*done*/ //PERULANGAN FOR DENGAN MENGGUNAKAN DIAGONAL DIFERENCE
                        soal3();
                        break;
                    case 4: /*done*/ //LILIN DI TIUP BERDASARKAN PERULANGAN YANG DI INPUTKAN
                        soal4();
                        break;
                    case 5: /*done*/ //perpindahan angka depan awal ke akhir belakang
                        soal5();
                        break;
                    case 6: /*done*/ //perpindahan angka akhir belakang kedepan
                        soal6();
                        break;
                }
                Console.ReadKey();
                Console.Write("\nPilih Soal Lagi? (y/n) : ");
                ulang = Console.ReadLine();
            }
            while (ulang.ToUpper() == "Y");
        }

        static void soal1() //TIME CONVERSION 12 JAM
        {
            /*07:05:45PM dan 12:00:10AM
                19:05:45 dan 00:00:10*/
            Console.Write("Input Waktu :  ");
            string inputWkt = Console.ReadLine().ToUpper();                  //toupper untuk strong huruf
            string[] detailWkt = inputWkt.Split(":");                       // index [0] = 07
            int jam = int.Parse(detailWkt[0]);                              //jam yg di liat dari aray [0] = {07}
            string pMaM = inputWkt.Substring(8, 2);                          // untuk kondisi (if) PM,AM = (index ke-8,ambil 2 Karakter)
            //jangan di ubah menit dan detik termasuk pada (:)
            string titik2MenitDetik = inputWkt.Substring(2, 6);              // :09:08 = (index ke-2,ambil 6 karakter)
            if (pMaM == "PM")
            {
                jam += 12;
                if (jam >= 24)
                {
                    jam -= 24;
                }
                Console.Write((jam) + titik2MenitDetik);
            }
            else if (pMaM == "AM")
            {
                if (jam == 12)
                {
                    jam -= 12;
                }

                Console.Write((jam) + titik2MenitDetik);
            }
        }
        static void soal2() //SISA UANG ELSA BERDASARKAN INPUTAN YANG DIPERLUKAN
        {
            //total menu =4
            //index makanan alergi 1
            //harga menu = 12000,20000,9000,15000 |total = 56000
            //uang elsa = 20000
            Console.Write("Total Menu : ");
            int menu = int.Parse(Console.ReadLine());
            Console.Write("Total Makanan alergi: ");
            int makananAlergi = int.Parse(Console.ReadLine());
            Console.Write("Uang Elsa : ");
            int uang = int.Parse(Console.ReadLine());
            Console.Write("Harga Menu : ");
            string[] hargaMenu = (Console.ReadLine().Split(",")); //AWAL HARGA MENU BENTUK STRING DALAM ARRAY
            int[] hargaMENU = Array.ConvertAll(hargaMenu, int.Parse); //DI CONVERSI KE INT DARI SETIAP HARGA MENU DI STRING ARRAY

            int sum = 0;
            for (int i = 0; i < hargaMENU.Length; i++)
            {
                sum += hargaMENU[i];
            }
            int jumlah = sum - hargaMENU[makananAlergi];
            int total = jumlah / 2;
            int sisaUang = uang - total;
            Console.WriteLine($"elsa harus membayar {total}");

            if (sisaUang == 0)
            {
                Console.Write("Uang Elsa Pas");
            }
            else
            {
                Console.WriteLine($"sisa uang elsa = {sisaUang}");
                if (sisaUang < 0)
                {
                    int pinjamUang = total - uang;
                    Console.WriteLine($"wajib minjam = {pinjamUang}");
                }
            }
            Console.Write("\n");
        }
        static void soal3() //PERULANGAN FOR DENGAN MENGGUNAKAN DIAGONAL DIFERENCE
        {
            int[,] data = new int[,]
            {
                {11,2,4 },
                {4,5,6 },
                {10,8,-12 }
            };
            int temp = 0;// PENYIMPANAN NILAI PERTAMA NYA
            int temp1 = 0;// PENYIMPANAN NILAI KEDUANYA
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (i == j)
                    {
                        temp += data[i, j];
                    }
                    if (j == 3 - i - 1)
                    {
                        temp1 += data[i, j];
                    }
                }
            }
            Console.WriteLine($"Perbedaan Diagonal = {temp - temp1}");
        }
        static void soal4() //LILIN DI TIUP BERDASARKAN PERULANGAN YANG DI INPUTKAN
        {
            string lilin;
            int total = 0;

            Console.Write("Tinggi Lilin : ");
            lilin = Console.ReadLine();
            string[] Lilin = lilin.Split(' ');
            int[] konversiLilin = Array.ConvertAll(Lilin, int.Parse);

            int tinggiLilin = konversiLilin.Max(); //LILIN YANG DI TIUP DI AMBIL NILAI MAXNYA
            for (int i = 0; i < konversiLilin.Length; i++)
            {
                if (tinggiLilin == konversiLilin[i])
                {
                    total++;
                }
            }
            Console.WriteLine($"Lilin yang di tiup adalah {total}");

        }
        static void soal5() //perpindahan angka depan awal ke akhir belakang
        {
            //5,6,7,0,1 arr // rot = 2
            //6,7,0,1,5 // 7,0,1,5,6
            //perpindahan angka depan awal ke akhir belakang
            Console.Write("Array input: ");
            string[] array = (Console.ReadLine().Split(","));
            Console.Write("rotasi : ");
            int rotate = int.Parse(Console.ReadLine());
            string temp = "";

            for (int j = 0; j < rotate; j++)
            {
                for (int i = 0; i < array.Length - 1; i++)
                {
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
                foreach (string s in array)
                {
                    Console.Write($"{s} ");
                }
                Console.WriteLine();
            }
        }
        static void soal6() //perpindahan angka akhir belakang kedepan
        {
            //5,6,7,0,1 arr // rot = 2
            //6,7,0,1,5 // 7,0,1,5,6
            //perpindahan angka akhir belakang kedepan
            Console.Write("Array input: ");
            string[] array = (Console.ReadLine().Split(","));
            Console.Write("rotasi : ");
            int rotate = int.Parse(Console.ReadLine());
            string temp = "";

            for (int j = 0; j < rotate; j++)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    temp = array[i];
                    array[i] = array[array.Length - 1];
                    array[array.Length - 1] = temp;
                }
                foreach (string s in array)
                {
                    Console.Write($"{s} ");
                }
                Console.WriteLine();
            }
        }
    }
}
