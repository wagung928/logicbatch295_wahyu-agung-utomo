﻿using System;

namespace TugasDay7_Dan_PRDay7
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string ulang = "";
            do
            {
                int pilih;
                Console.Write("pilih soal 1-8 : ");
                pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 1:/*done*/ // PENCARIAN TRACK TIKET KAPAN DIA MASUK DAN KELUAR UNTUK MEMBAYAR KARCISNYA
                        soal1();
                        break;
                    case 2: /*done*/ // PINJAM BUKU BERDASARKAN WAKTU YANG DI LEWATI DARI PERJANJIAN DAN KENA DENDA SETELAH MELEWATI PERJANJIAN
                        soal2();
                        break;
                    case 3: /*0done*/ //MENCARI KAPAN WAKTU TES UJIAN 1 PADA SAAT PERHITUNGAN DI MULAI DARI AWAL MASUK BOOTCAMP
                        soal3();
                        break;
                    case 4: /*done*/ //SEWA GAME WARNET PERJAM DAN NAMBAH BILING
                        soal4();
                        break;
                    case 5: /*done*/ //UNTUK OJOL MENGANTARKAN PAKET BERDASARKAN SIFAT LINIER JALAN / JALAN WAJIB MELEWATI YANG LAINNYA
                        soal5();
                        break;
                    case 6: /*done*/ // FAKTORIAL LOOPING == KUNCI PENTING UNTUK MENGETAHUI PROSES UPDATE DI SETIAP PERLOOPINGAN DENGAN FOR
                        soal6();
                        break;
                    case 7: /*done*/ // PERTARUHAN (BEETING) DENGAN ANGKA RANDOM == WIN/LOSE/SERI
                        soal7();
                        break;
                    case 8: /*0done*/ //PRDay_7 ==> BATANG KAYU YANG TERSISA HARUS HABIS
                        soal8();
                        break;
                }
                Console.ReadKey();
                Console.Write("\nPilih Soal Lagi? (y/n) : ");
                ulang = Console.ReadLine();
            }
            while (ulang.ToUpper() == "Y");
        }
        static void soal1()
        {
            // PENCARIAN TRACK TIKET KAPAN DIA MASUK DAN KELUAR UNTUK MEMBAYAR KARCISNYA

            DateTime masukGedung = new DateTime(2019, 08, 20, 07, 50, 00);//Tahun, bulan, hari, jam, menit, detik
            Console.WriteLine(masukGedung);
            DateTime keluarGedung = new DateTime(2019, 08, 20, 17, 30, 00);//Tahun, bulan, hari, jam, menit, detik
            Console.WriteLine(keluarGedung);
            TimeSpan interval = keluarGedung - masukGedung; // 09.40 = 29000
            Console.WriteLine("Interval menit : " + interval.TotalMinutes); //580 menit = 9.6 jam
            double dateTime = ((double)interval.TotalMinutes);
            double bayarPerJam = 3000;
            double bayar = (bayarPerJam * dateTime) / 60;
            Console.WriteLine(Math.Round(bayar));// Math.Round
        }
        static void soal2()
        {
            // PINJAM BUKU BERDASARKAN WAKTU YANG DI LEWATI DARI PERJANJIAN DAN KENA DENDA SETELAH MELEWATI PERJANJIAN

            DateTime pinjamBuku = new DateTime(2019, 06, 09);//Tahun, bulan, hari, jam, menit, detik
            Console.WriteLine(pinjamBuku);
            DateTime mengembalikkanBuku = new DateTime(2019, 07, 10);//Tahun, bulan, hari, jam, menit, detik
            Console.WriteLine(mengembalikkanBuku);
            int dendaPerHari = 500;
            int bayarDenda = 0;
            if (mengembalikkanBuku > pinjamBuku)
            {
                TimeSpan interval = mengembalikkanBuku - pinjamBuku.AddDays(3); //diskon terakhir tanpa denda
                int Interval = (int)interval.TotalDays; //diskon terakhir tanpa denda
                bayarDenda = Interval * dendaPerHari;//start denda
                Console.WriteLine($"Denda = Rp{bayarDenda}");
            }

            //Cara 2 INPUT MANUAL

            /*int total = 0;

            Console.Write("Masukkan Tanggal Meminjam Buku = ");
            string tanggal_meminjam = Console.ReadLine();
            DateTime pinjam = DateTime.Parse(tanggal_meminjam);

            Console.Write("Masukkan Lama Peminjaman = ");
            int lama_peminjaman = int.Parse(Console.ReadLine());

            Console.WriteLine($"Tanggal Seharusnya Mengembalikan = {pinjam = pinjam.AddDays(lama_peminjaman)}");

            Console.Write("Masukkan Tanggal Mengembalikan Buku = ");
            string tanggal_mengembalikan = Console.ReadLine();
            DateTime kembalikan = DateTime.Parse(tanggal_mengembalikan);

            if (kembalikan > pinjam.AddDays(lama_peminjaman))
            {
                TimeSpan interval = kembalikan - pinjam.AddDays(lama_peminjaman);
                int Interval = (int)interval.TotalDays;
                total = Interval * 500;
                Console.WriteLine($"Denda = Rp{total}");
            }
            else
            {
                Console.WriteLine("Denda = Rp.0");
            }*/
        }
        static void soal3() //belum
        {
            //MENCARI KAPAN WAKTU TES UJIAN 1 PADA SAAT PERHITUNGAN DI MULAI DARI AWAL MASUK BOOTCAMP

            //Tanggal Mulai(mm/ dd / yyyy) = 07 / 30 / 2022
            //Hari Libur = 7,8	
            //Output : Kelas akan ujian pada = 08/18/2022			
            Console.Write("Input Tanggal BootCamp (mm/dd/yyyy) : ");
            string input = Console.ReadLine(); //FORMAT = BULAN, TANGGAL, TAHUN
            DateTime dateInput = DateTime.Parse(input);
            Console.WriteLine(dateInput);
            Console.WriteLine("SET FORMAT : " + dateInput.ToString("dd MMMM yyyy")); //SET FORMAT


            Console.Write("Masukan Input Tanggal Masuk Bootcamp = ");
            string tanggalIn = Console.ReadLine();
            //DateTime dateInput = DateTime.Parse(tanggalIn);
            Console.WriteLine(dateInput.ToString("dd MMMM yyyy"));
            for (int i = 0; i < 10; i++)
            {
                int tanggal = dateInput.Day;
                string hari = dateInput.DayOfWeek.ToString();
                if (hari == "Sunday" && hari == "Saturday")
                {
                    tanggal++;
                    Console.Write($"libur terdapat di {hari}");
                }
                else
                {
                    tanggal++; 
                }
                dateInput = dateInput.AddDays(1);
            }
            Console.WriteLine(dateInput);

        }
        static void soal4()
        {
            //SEWA GAME WARNET PERJAM DAN NAMBAH BILING

            //CARA KE 1 LANGSUNG INPUT
            DateTime gameSet1 = new DateTime(0001, 01, 01, 08, 15, 00); //mulai dia mau main
            gameSet1 = gameSet1.AddHours(3);// start pasang game set 1 selama 3 jam
            TimeSpan intervalGame1 = gameSet1 - gameSet1.AddHours(3);
            //Console.WriteLine((gameSet1));
            int perjamGame = 3500;
            int dateTimeEnd = (int)intervalGame1.TotalHours;
            int hargaBayarSewa1 = -(dateTimeEnd * perjamGame);
            Console.WriteLine($"Game Set 1 end = {gameSet1} dan Bayar = {hargaBayarSewa1}");
            gameSet1 = gameSet1.AddHours(2);// nambah waktu 2 jam game set 2
            TimeSpan intervalGame2 = gameSet1 - gameSet1.AddHours(2);
            int dateTimeEnd2 = (int)intervalGame2.TotalHours;
            int hargaBayarSewa2 = -(dateTimeEnd2 * perjamGame);
            Console.WriteLine($"Game Set 2 end = {gameSet1} dan Bayar = {hargaBayarSewa1 + hargaBayarSewa2}");


            //CARA 2 INPUT MANUAL
            int durasi_main;
            int harga;
            int tambahan_billing;

            Console.Write("Masukkan Durasi Sewa Warnet = ");
            durasi_main = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Jam Mulai Main = ");
            string jam_masuk = Console.ReadLine();
            DateTime masuk = DateTime.Parse(jam_masuk);

            int jam = masuk.Hour + durasi_main;

            harga = durasi_main * 3500;

            Console.Write("Masukkan Tambahan Billing = ");
            tambahan_billing = int.Parse(Console.ReadLine());

            int total_jam = jam + tambahan_billing;
            int total_biaya = harga + (tambahan_billing * 3500);

            Console.WriteLine($"Durasi Sewa Akan Habis Pada Pukul {total_jam}:" + masuk.Minute);
            Console.WriteLine($"Total Harga nya Adalah = Rp.{total_biaya}");
        }
        static void soal5()
        {
            //UNTUK OJOL MENGANTARKAN PAKET BERDASARKAN SIFAT LINIER JALAN / JALAN WAJIB MELEWATI YANG LAINNYA

            Console.Write("Input Antar Customer: ");
            double inputAntarCustomer = double.Parse(Console.ReadLine());
            double tokoCS1 = 2000, cs1cs2 = 500, cs2cs3 = 1500, cs3cs4 = 300;
            double satuLiterBensin = 2500;
            double jarakTempuh = 0;
            double pemakaianBensin = 0;
            if (inputAntarCustomer == 1)
            {
                jarakTempuh = tokoCS1;
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }
            if (inputAntarCustomer == 2)
            {
                jarakTempuh = (tokoCS1 + cs1cs2);
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }
            if (inputAntarCustomer == 3)
            {
                jarakTempuh = (tokoCS1 + cs1cs2 + cs2cs3);
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }
            if (inputAntarCustomer == 4)
            {
                jarakTempuh = (tokoCS1 + cs1cs2 + cs2cs3 + cs3cs4);
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }
            Console.WriteLine($"Jarak Tempuh = {jarakTempuh / 1000} KM");
            Console.WriteLine($"Bensin = {Math.Round(pemakaianBensin)}");
        }
        static void soal6()
        {
            // FAKTORIAL LOOPING
            //KUNCI PENTING UNTUK MENGETAHUI PROSES UPDATE DI SETIAP PERLOOPINGAN DENGAN FOR

            int faktorial;
            int total = 1;//1->2->6 ==> ini adalah perputaran hasil setiap update loopingnya

            Console.Write("Masukkan Faktorial = ");
            faktorial = int.Parse(Console.ReadLine());

            for (int i = 1; i <= faktorial; i++) //maks input = 3
            {
                total *= i;
            }
            Console.Write($"Ada {total} Cara");
        }
        static void soal7()
        {
            // PERTARUHAN (BEETING) DENGAN ANGKA RANDOM == WIN/LOSE/SERI

            bool ulangiPermainanTaruhan = true; // kondisi UNTUK PERULANGAN DO WHILE ==> untuk mencek mau taruhan lagi atau tidaknya WALAUPUN MASIH MEMILIKI POINT
            Console.Write("Masukan Point = "); //poin taruhannya untuk menebak
            int point = int.Parse(Console.ReadLine()); //INI NANTI AKAN KE UPDATE DI SETIAP PERULANGAN BEETING
            do
            {
                while (ulangiPermainanTaruhan)
                {
                    Console.Write("Masang Taruhan = "); //beeting untuk masang harga tebakan awal
                    int masangTaruhan = int.Parse(Console.ReadLine());
                    Console.Write("Masukan Jawaban Tebakan Anda U/D = "); // jawaban taruhan ketika yakin
                    string answerSure = Console.ReadLine().ToUpper();
                    Random randomAngka = new Random(); // Fungsi Random angka di komputer
                    int numbers = randomAngka.Next(0, 9); // angka random mulai dari 0-9 yang telah disediakan/ sudah di deteksi
                    string password = ""; /// buat memberikan jawaban yang akan di hasilnya
                    if (point > 0)
                    {
                        if (masangTaruhan <= point) //point ke update
                        {
                            if (numbers > 0)
                            {
                                password = "U";
                            }
                            if (numbers < 0)
                            {
                                password = "D";
                            }
                            // MASUK KE KONDISI WIN/LOSE/SERI
                            else if (numbers == 5)// ini untuk kondisi if (numbers == 5 SERTA (answersure == password && answersure != password) TIDAK TERKENA IMPAC/DAMPAK)
                            {
                                point += 0;
                                Console.WriteLine("SERI");
                            }
                            if (answerSure == password && numbers != 5) //GUNAKAN NUMBERS !=5 AGAR TIDAK TERKENA IMPAC.DAMPAK DARI KONDISI SERI
                            {
                                point += masangTaruhan;
                                Console.WriteLine("You Win");
                            }
                            else if (answerSure != password && numbers !=5)
                            {
                                point -= masangTaruhan;
                                Console.WriteLine("You Lose");
                            }
                            Console.WriteLine($"Angka Taruhan Yang Keluar Adalah {numbers}"); // UNTUK ANGKA TARUHAN YANG KELUAR
                        }
                        else // agar dia tidak bisa melakukan beeting
                        {
                            Console.WriteLine($"Anda tidak bisa masang taruhan lebih dari = {point}");
                        }
                    }
                    if (point <= 0) // JIKA SUAD LOSE DAN POIN == 0 MAKA BARU KONDISI GAME OVER INI
                    {
                        point = 0;
                        Console.WriteLine("Game Over");
                    }
                    Console.WriteLine($"Point Anda Saat ini = {point}"); // SEDANGKAN YANG INI UNTUK DETAIL POIT SETELAH BET
                    
                    //MASUK KE KONDISI MAU TARUHAN ATAU TIDAK BERDASARKAN (POIN SISA YANG WAJIB) DIMILIKI
                    Console.WriteLine("Mau Bertaruh Lagi ? Y/N");
                    string yeah = Console.ReadLine().ToUpper();
                    if (yeah == "Y" && point > 0) // masuk ke kondisi mau taruhan lagi atau tidaknya berdasarkan poin yang ada mau di taruhkan kembali atau tidak
                    {
                        ulangiPermainanTaruhan = true;
                    }
                    else
                    {
                        ulangiPermainanTaruhan = false;
                        Console.WriteLine("Terimakasih telah ikut partisipasi taruhannya. Semoga dapat bergabung kembali.");// KONDISI JIKA MAU LANJUT PERTARUHAN BAIK ADA POINT ATAU TIDAK ADA POINT
                    }
                    Console.WriteLine($"Point Anda Saat ini = {point}"); // untuk memberikan detail point pertama saat permainan pertaruhkan (MAU DI MULAI ULANG)
                }
            } while (point > 0); //point > 0 // ini untuk perulangan WHILE
        }
        static void soal8() //PRDay_7 ==> BATANG KAYU YANG TERSISA HARUS HABIS
        {
        }
    } 
}
