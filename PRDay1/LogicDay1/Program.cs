﻿using System;

namespace LogicDay2
{
    internal class Program
    {
        /*Logic Day 2*/
        /*SOAL 1*/
        /*static void Main(string[] args)
        {
            Nilai();
            Console.Read();
        }
        static void Nilai()
        {
            int inputAngka;
            Console.Write("Nilai: ");
            inputAngka = int.Parse(Console.ReadLine());


            if (inputAngka >= 85)
            {
                Console.WriteLine("nilai A");
            }
            else if (inputAngka >=70)
            {
                Console.WriteLine("nilai B");
            } else if (inputAngka >=55)
            {
                Console.WriteLine("nilai c");
            }
        }*/


        /*SOAL 2*/
        /*static void Main(string[] args)
        {
            Pulsa();
            Console.Read();
        }
        static void Pulsa()
        {
            int membeliPulsa;
            Console.Write("Membeli Pulsa: ");
            membeliPulsa = int.Parse(Console.ReadLine());


            if (membeliPulsa >= 10000 && membeliPulsa < 25000)
            {
                Console.WriteLine($"Poin = 80");
            }
            else if (membeliPulsa >= 25000 && membeliPulsa < 50000)
            {
                Console.WriteLine($"Point = 200");
            }
            else if (membeliPulsa >= 50000 && membeliPulsa < 100000)
            {
                Console.WriteLine($"Point = 400");
            }
            else if (membeliPulsa >= 100000)
            {
                Console.WriteLine($"Point = 800");
            }
        }*/


        /*SOAL 3*/
        /*static void Main(string[] args)
        {
            diskon();
            
            Console.Read();
        }

        
            static void diskon()
        {
            int belanja,jarak;
            double totalBelanja, ifdiskon;
            string Promo;
            Console.Write("Belanja : ");
            belanja = int.Parse(Console.ReadLine());

            Console.Write("Jarak : ");
            jarak = int.Parse(Console.ReadLine());
                      
            Console.Write("Masukkan Promo : ");
            Promo = Console.ReadLine();

            if (Promo == "JKTOVO" && belanja < 30000)
            {
                totalBelanja = belanja + (jarak * 1000);
                Console.Write($"Belanja = {belanja}");
                Console.Write($"Diskon Tidak dapat");
                Console.Write($"Ongkir = {jarak*1000}");
                Console.Write($"Total Belanja = {totalBelanja}");
            }
            else if (Promo == "JKTOVO" && belanja >= 30000)
            {
                totalBelanja = belanja + (0.4*belanja) + (jarak * 1000);
                Console.WriteLine($"Belanja = {belanja}");
                Console.WriteLine($"Diskon = {0.4*belanja}");
                Console.WriteLine($"Ongkir = {jarak * 1000}");
                Console.WriteLine($"Total Belanja = {totalBelanja}");
            } 
            else if (Promo == "JKTOVO" && belanja >= 30000)
            {

                //untuk memaksimalkan diskon ketentuan maks.30000
                ifdiskon = 0.4 * belanja;
                    if (ifdiskon >= 30000)
                {
                    totalBelanja = belanja + (jarak * 1000) - ifdiskon;
                    Console.WriteLine($"Belanja = {belanja}");
                    Console.WriteLine($"Diskon = {0.4 * belanja}");
                    Console.WriteLine($"Ongkir = {jarak * 1000}");
                    Console.WriteLine($"Total Belanja = {totalBelanja}");
                } 
                
            }
            else
            {
                totalBelanja = belanja + (jarak * 1000);
                Console.WriteLine($"Belanja = {belanja}");
                Console.WriteLine($"Diskon = Tidak mendapat diskon");
                Console.WriteLine($"Ongkir = {jarak * 1000}");
                Console.WriteLine($"Total Belanja = {totalBelanja}");
            }
        }*/

        /*SOAL 4*/
        /*static void Main(string[] args)
        {
            diskon();

            Console.Read();
        }


        static void diskon()
        {
            int belanja, ongkosKirim, pilihVoucher, diskonOngkir, diskonBelanja, totalBelanja;
            Console.WriteLine("1. Min Order 30rb free ongkir 5rb dan potongan harga belanja 5rb\n2. Min Order 50rb free ongkir 10rb dan potongan harga belanja 10rb\n3. Min Order 100rb free ongkir 20rb dan potongan harga belanja 10rb");

            Console.Write("Belanja : ");
            belanja = int.Parse(Console.ReadLine());

            Console.Write("Ongkir : ");
            ongkosKirim = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Promo : ");
            pilihVoucher = int.Parse(Console.ReadLine());


            switch (pilihVoucher)
            {
                case 1:

                    if (belanja >= 30000)
                    {
                        diskonBelanja = 5000;
                        diskonOngkir = 5000;
                        totalBelanja = belanja + ongkosKirim - diskonOngkir - diskonBelanja;
                        Console.WriteLine($"Belanja                 = {belanja}");
                        Console.WriteLine($"Ongkos Kirim            = {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir           = {diskonOngkir}");
                        Console.WriteLine($"Diskon Belanja          = {diskonBelanja}");
                        Console.WriteLine($"Total Belanja           = {totalBelanja}");
                    }
                    break;
                case 2:
                    if (belanja >= 50000)
                    {
                        diskonBelanja = 10000;
                        diskonOngkir = 10000;
                        totalBelanja = belanja + ongkosKirim - diskonOngkir - diskonBelanja;
                        Console.WriteLine($"Belanja                 = {belanja}");
                        Console.WriteLine($"Ongkos Kirim            = {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir           = {diskonOngkir}");
                        Console.WriteLine($"Diskon Belanja          = {diskonBelanja}");
                        Console.WriteLine($"Total Belanja           = {totalBelanja}");
                    }
                    break;
                case 3:
                    if (belanja >= 100000)
                    {
                        diskonBelanja = 10000;
                        diskonOngkir = 20000;
                        totalBelanja = belanja + ongkosKirim - diskonOngkir - diskonBelanja;
                        Console.WriteLine($"Belanja                 = {belanja}");
                        Console.WriteLine($"Ongkos Kirim            = {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir           = {diskonOngkir}");
                        Console.WriteLine($"Diskon Belanja          = {diskonBelanja}");
                        Console.WriteLine($"Total Belanja           = {totalBelanja}");
                    }
                    break;

            }

        }*/


        /*SOAL 5*/
        /*static void Main(string[] args)
        {
            generasiLahir();

            Console.Read();
        }


        static void generasiLahir()
        {

            int babyBoomer, kelahiran, generasiX, generasiY, generasiZ;
            string nama;
            Console.Write("Masukkan nama Anda: ");
            nama = (Console.ReadLine());

            Console.Write("Tahun berapa anda Lahir? : ");
            kelahiran = int.Parse(Console.ReadLine());
            if (kelahiran >= 1944 && kelahiran <=1964)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Baby Boomer");
            }
            else if (kelahiran >= 1965 && kelahiran <= 1979)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi X");
            }
            else if (kelahiran >= 1980 && kelahiran <= 1994)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Y");
            }
            else if (kelahiran >= 1995 && kelahiran <= 2015)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Z");
            }
        }*/

        /*SOAL 6*/
        /*static void Main(string[] args)
        {
            gajiPokok();

            Console.Read();
        }


        static void gajiPokok()
        {
            double banyakBulan, totalGaji, gajiPerBulan, pajak, bpjs, gapok, tunjangan;

            string nama;

            Console.Write("Nama: ");
            nama = Console.ReadLine();

            Console.Write("Tunjangan : ");
            tunjangan = double.Parse(Console.ReadLine());

            Console.Write("Gaji Pokok : ");
            gapok = double.Parse(Console.ReadLine());

            Console.Write("Banyak Bulan : ");
            banyakBulan = int.Parse(Console.ReadLine());

            if (gapok + tunjangan <= 5000000)
            {
                pajak = (0.05) * (gapok + tunjangan);
                bpjs = (0.03) * (gapok + tunjangan);
                gajiPerBulan = (gapok + tunjangan) - (pajak + bpjs);
                totalGaji = ((gapok + tunjangan) - (pajak + bpjs)) * banyakBulan;

                Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut:");
                Console.WriteLine($"Pajak                               = {pajak}");
                Console.WriteLine($"BPJS                                = {bpjs}");
                Console.WriteLine($"Gaji/bln                            = {gajiPerBulan}");
                Console.WriteLine($"Total gaji/banyak bulan             = {totalGaji}");
            }
            else if (gapok + tunjangan > 5000000 && gapok + tunjangan <= 10000000)
            {
                pajak = (0.10) * (gapok + tunjangan);
                bpjs = (0.03) * (gapok + tunjangan);
                gajiPerBulan = (gapok + tunjangan) - (pajak + bpjs);
                totalGaji = ((gapok + tunjangan) - (pajak + bpjs)) * banyakBulan;

                Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut:");
                Console.WriteLine($"Pajak                               = {pajak}");
                Console.WriteLine($"BPJS                                = {bpjs}");
                Console.WriteLine($"Gaji/bln                            = {gajiPerBulan}");
                Console.WriteLine($"Total gaji/banyak bulan             = {totalGaji}");
            }
            else if (gapok + tunjangan > 10000000)
            {
                pajak = (0.15) * (gapok + tunjangan);
                bpjs = (0.03) * (gapok + tunjangan);
                gajiPerBulan = (gapok + tunjangan) - (pajak + bpjs);
                totalGaji = ((gapok + tunjangan) - (pajak + bpjs)) * banyakBulan;

                Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut:");
                Console.WriteLine($"Pajak                               = {pajak}");
                Console.WriteLine($"BPJS                                = {bpjs}");
                Console.WriteLine($"Gaji/bln                            = {gajiPerBulan}");
                Console.WriteLine($"Total gaji/banyak bulan             = {totalGaji}");
            }



        }*/

        /*SOAL 7*/
        /*static void Main(string[] args)
        {
            bodyMassIndex();

            Console.Read();
        }


        static void bodyMassIndex()
        {

            int beratBadan, tinggiBadan;
            double convertTinggi,BMI;
            Console.Write("Berat Badan: ");
            beratBadan = int.Parse(Console.ReadLine());

            Console.Write("Tinggi Badan : ");
            tinggiBadan = int.Parse(Console.ReadLine());

            convertTinggi = 0.01 * tinggiBadan;
            BMI = beratBadan / (convertTinggi*convertTinggi);

            if (BMI < 18.5)
            {
                Console.WriteLine($"Nilai BMI anda adalah {BMI}");
                Console.WriteLine("Anda Termasuk Berbadan terlalu Kurus");
            }
            else if (BMI>=18.5&&BMI<=25){
                Console.WriteLine($"Nilai BMI anda adalah {BMI}");
                Console.WriteLine("Anda Termasuk Berbadan Langsing/Sehat ");
            }
            else if (BMI>25){
                Console.WriteLine($"Nilai BMI anda adalah {BMI}");
                Console.WriteLine("Anda Termasuk Berbadan Gemuk");
            }
        }*/

        /*SOAL 8*/
        /*static void Main(string[] args)
        {
            nilaiMapel();

            Console.Read();
        }


        static void nilaiMapel()
        {

            int mtk, fisika, kimia, rataRata;
            Console.Write("Masukkan nilai MTK: ");
            mtk = int.Parse(Console.ReadLine());

            Console.Write("Masukkan nilai Fisika : ");
            fisika = int.Parse(Console.ReadLine());

            Console.Write("Masukkan nilai Kimia : ");
            kimia = int.Parse(Console.ReadLine());
            rataRata = (mtk + fisika + kimia) / 3;
            if (rataRata>=75)
            {
                Console.WriteLine($"Nilai Rata-rata = {rataRata}");
                Console.WriteLine("Selamat\nKamu Berhasil\nKamu Hebat");
            }
            else if (rataRata < 75)
            {
                Console.WriteLine($"Nilai Rata-rata = {rataRata}");
                Console.WriteLine("Maaf\nKamu Gagal");
            }
        }*/
    }
}



