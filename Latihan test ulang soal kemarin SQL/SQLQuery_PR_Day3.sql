-- PR Day 3 SQL

create table tblKaryawan (
id bigint primary key identity (1,1),
	nip varchar (50) not null,
	nama_depan varchar(50) not null,
	nama_belakang varchar(50) not null,
	jenis_kelamin varchar (50) not null,
	agama varchar (50) not null,
	tempat_lahir varchar (50) not null,
	tgl_lahir date null,
	alamat varchar (50) not null,
	pendidikan_terakhir varchar (50) not null,
	tgl_masuk date null
)
select * from tblKaryawan
--drop table tblKaryawan
insert into tblKaryawan
(nip, nama_depan,nama_belakang,jenis_kelamin,agama,tempat_lahir,tgl_lahir,alamat,pendidikan_terakhir,tgl_masuk) values
('001','Hamidi','Samsudin','PRIA','ISLAM','SUKABUMI','1977-04-21','Jl.Sudirman No.12','S1 Teknik Mesin','2015-12-07'),
('003','Paul','Cristian','PRIA','KRISTEN','AMBON','1980-05-27','Jl.Veteran No.4','S1 Pendidikan Geografi','2014-01-12'),
('002','Ghandi','Wamida','WANITA','ISLAM','PALU','1992-01-12','Jl.Rambutan No.22','SMA Negeri 02 Palu','2014-12-01')
select * from tblKaryawan
--drop table tblKaryawan

create table tblDivisi (
id bigint primary key identity (1,1),
	kd_divisi varchar (50) not null,
	nama_divisi varchar(50) not null
)
select * from tblDivisi
--drop table tblDivisi
insert into tblDivisi
(kd_divisi,nama_divisi) values
('GD','Gudang'),
('HRD','HRD'),
('KU','Keuangan'),
('UM','Umum')
select * from tblDivisi
--drop table tblDivisi

create table tblJabatan (
id bigint primary key identity (1,1),
	kd_jabatan varchar (50) not null,
	nama_jabatan varchar(50) not null,
	gaji_pokok numeric null,
	tunjangan_jabatan numeric null
)
select * from tblJabatan
--drop table tblJabatan
insert into tblJabatan
(kd_jabatan,nama_jabatan,gaji_pokok,tunjangan_jabatan) values
('MGR','Manager','5500000','1500000'),
('OB','Office Boy','1900000','200000'),
('ST','Staff','3000000','750000'),
('WMGR','Wakil Manager','4000000','1200000')
select * from tblJabatan
--drop table tblJabatan

create table tblPekerjaan (
id bigint primary key identity (1,1),
	nip varchar (50) not null,
	kode_jabatan varchar(50) not null,
	kode_divisi varchar(50) not null,
	tunjangan_kinerja numeric null,
	kota_penempatan varchar (50) null
)
select * from tblPekerjaan
--drop table tblPekerjaan
insert into tblPekerjaan
(nip,kode_jabatan,kode_divisi,tunjangan_kinerja,kota_penempatan) values
('001','ST','KU','750000','Cianjur'),
('002','OB','UM','350000','Sukabumi'),
('003','MGR','HRD','1500000','Sukabumi')
select * from tblPekerjaan
--drop table tblPekerjaan



--SOAL
select * from tblKaryawan
select * from tblDivisi
select * from tblJabatan
select * from tblPekerjaan
--drop table tblPekerjaan

--1. Tampilkan nama lengkap, nama jabatan, total tunjangan, yang gaji + tunjangannya dibawah 5juta
--jawaban:
select * from tblKaryawan
select * from tblJabatan
select * from tblPekerjaan

select
concat (nama_depan,' ',nama_belakang) as nama_lengkap, -- concat itu menghubungkan antar colom pada setiap tabel yang di inginkan, kuncinya pada (join) dan (as) pada tabel yang kita inginkan
nama_jabatan,
case
when gaji_pokok + tunjangan_jabatan < 5000000 then gaji_pokok+tunjangan_jabatan
end as gaji_tunjangan
from tblJabatan as j -- ini bisa di masukkan kedalam create view yang kita inginkan
join tblPekerjaan as p on j.kd_jabatan = p.kode_jabatan
join tblKaryawan as k on k.nip = p.nip
where gaji_pokok+tunjangan_jabatan < 5000000

--2. Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi
--jawaban:
select * from tblKaryawan
select * from tblJabatan
select * from tblPekerjaan
select * from tblDivisi

select
concat (nama_depan,' ',nama_belakang) as nama_lengkap,
nama_jabatan, nama_divisi,
gaji_pokok+tunjangan_jabatan+tunjangan_kinerja as total_gaji,
0.05 * (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja) as pajak,
case
when (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja) - (0.05 * (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)) >= 4000000 and
(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja) - (0.05 * (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)) < 8000000 
then (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja) - (0.05 * (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja))
else 0
end as gaji_bersih
from tblJabatan as j
join tblPekerjaan as p on j.kd_jabatan = p.kode_jabatan
join tblKaryawan as k on k.nip = p.nip
join tblDivisi as d on d.kd_divisi = p.kode_divisi
where (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja) - (0.05 * (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)) >= 4000000 and
(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja) - (0.05 * (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)) < 8000000

--3. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja * 7)
--jawaban:
select * from tblKaryawan
select * from tblJabatan
select * from tblPekerjaan
select * from tblDivisi

select
k.nip,concat (nama_depan,' ',nama_belakang) as nama_lengkap,
nama_jabatan, nama_divisi,
0.25 * (gaji_pokok + tunjangan_jabatan + tunjangan_kinerja)*7 as bonus
from tblJabatan as j
join tblPekerjaan as p on j.kd_jabatan = p.kode_jabatan
join tblKaryawan as k on k.nip = p.nip
join tblDivisi as d on d.kd_divisi = p.kode_divisi

--4. Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR
--jawaban:
select * from tblKaryawan
select * from tblJabatan
select * from tblPekerjaan
select * from tblDivisi

select
k.nip, concat (nama_depan,' ',nama_belakang) as nama_lengkap,
gaji_pokok+tunjangan_jabatan+tunjangan_kinerja as total_gaji,
0.05*(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja) as infak
from tblJabatan as j
join tblPekerjaan as p on j.kd_jabatan = p.kode_jabatan
join tblKaryawan as k on k.nip = p.nip
join tblDivisi as d on d.kd_divisi = p.kode_divisi
where kode_jabatan = 'MGR'

--5. Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1
--jawaban: belum

select * from tblKaryawan
select * from tblJabatan
select * from tblPekerjaan
select * from tblDivisi

select
k.nip,concat (nama_depan,' ',nama_belakang) as nama_lengkap,
nama_jabatan, pendidikan_terakhir, 2000000 as tunjangan_pendidikan, -- ini ditambahkan
(gaji_pokok + tunjangan_jabatan + 2000000) as total_gaji
from tblJabatan as j
join tblPekerjaan as p on j.kd_jabatan = p.kode_jabatan
join tblKaryawan as k on k.nip = p.nip
join tblDivisi as d on d.kd_divisi = p.kode_divisi
where gaji_pokok >= 3000000

--6. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus
--jawaban:
select * from tblKaryawan
select * from tblJabatan
select * from tblPekerjaan
select * from tblDivisi

select
k.nip,concat (nama_depan,' ',nama_belakang) as nama_lengkap,
nama_jabatan, nama_divisi,
case
when nama_jabatan = 'Manager' then 0.25 * (gaji_pokok + tunjangan_jabatan + tunjangan_kinerja)*7
when nama_jabatan = 'Staff' then 0.25 * (gaji_pokok + tunjangan_jabatan + tunjangan_kinerja)*5
else
0.25 * (gaji_pokok + tunjangan_jabatan + tunjangan_kinerja)*2
end as bonus
from tblJabatan as j
join tblPekerjaan as p on j.kd_jabatan = p.kode_jabatan
join tblKaryawan as k on k.nip = p.nip
join tblDivisi as d on d.kd_divisi = p.kode_divisi

--7. Buatlah kolom nip pada table karyawan sebagai kolom unique
--jawaban:

alter table tblKaryawan
add constraint UC_nip unique (nip);

--8. buatlah kolom nip pada table karyawan sebagai index (KHUSUS INDEX TIDAK ADA ALTER)
--jawaban:
CREATE INDEX index_nip ON tblKaryawan (nip); -- ini tidak terpaku dengan tanda (;)

--9. Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf W
--jawaban:

--select * from tblFilm where
--pendapatan = (select max (pendapatan) from tblFilm)

select * from tblKaryawan

select
concat (nama_depan,' ',upper (nama_belakang)) as nama_lengkap--upper disini untuk nama agar yang di select sesuai ukuran yang di syntaxkan (upper)
from tblKaryawan where nama_belakang like 'W%' order by id asc
--update tblKaryawan set nama_belakang = 'SAMSUDIN' where nama_belakang = 'Samsudin'
--update tblKaryawan set nama_belakang = 'CRISTIAN' where nama_belakang = 'Cristian'
--update tblKaryawan set nama_belakang = 'WAMIDA' where nama_belakang = 'Wamida'

--yang belum = 8/9

--10.  Perusahaan akan memberikan bonus sebanyak 10% dari total gaji bagi karyawan yg sudah join di peruashaan diatas 8/ sudah 8 thn tahun
-- Tampilkan nip, nama lengkap, jabatan, nama divisi, total gaji , bonus, lama bekerja
-- (total gaji = Gaji_pokok+Tunjangan_jabatan+Tunjangan_kinerja )
--jawaban:

select * from tblKaryawan
select * from tblJabatan
select * from tblDivisi
select * from tblPekerjaan

select k.nip, concat (nama_depan,' ', nama_belakang) as nama_lengkap, nama_jabatan, nama_divisi,
(gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) as total_gaji,
0.1 * (gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) as bonus,
--untuk cari tgl masuk dan tgl saat ini == lama dia bekerja
tgl_masuk, GETDATE () as tahun_sekarang, -- tgl masuk detail
datediff (year, tgl_masuk, GETDATE()) as Lama_bekerja

from tblKaryawan as k
join tblPekerjaan as p on k.nip = p.nip
join tblJabatan as j on p.kode_jabatan = j.kd_jabatan
join tblDivisi as d on p.kode_divisi = d.kd_divisi
where datediff (year, tgl_masuk, GETDATE()) > = 8 -- wehere itu keseluruhan data