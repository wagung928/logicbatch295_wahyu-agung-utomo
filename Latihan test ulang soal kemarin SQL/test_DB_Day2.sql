--TEST DB DAY 2 ULANG
select * from tblArtis
select * from tblFilm
select * from tblProduser
select * from tblNegara
select * from tblGenre

--soal

--1. Menampilkan jumlah pendapatan produser marvel secara keseluruhan
--jawaban: --berdasarkan kd_produser
select * from tblFilm
select * from tblProduser

select produser.nm_produser,
sum (film.pendapatan) as total_pendapatan 
from tblFilm as film
join tblProduser as produser
on film.produser = produser.kd_produser
group by produser.nm_produser
having produser.nm_produser = 'marvel'

--2. Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi
--jawaban:
select * from tblFilm

select nm_film,nominasi from tblFilm
where nominasi = 0

--3. Menampilkan nama film dan pendapatan yang paling tinggi
--jawaban: INI PENTING UNTUK SUB QUERY MATERI
-- ini query di dalam query atau disebut sub query
--mencari maksimal data film pendapatan
select * from tblFilm

select * from tblFilm
where pendapatan = (select max (pendapatan) from tblFilm) --ini query di dalam query atau disebut sub query

--4. Menampilkan nama film yang huruf depannya 'p'
--jawaban:
--huruf depannya
select * from tblFilm

select * from tblFilm
where nm_film like 'p%' order by id asc 
-- order by id asc/desc adalah untuk urutan nomer dari paling  atas/bawah untuk memulainya

--5. Menampilkan nama film yang huruf terakhir 'y'
--jawaban:
--huruf terakhir
select * from tblFilm

select * from tblFilm
where nm_film like '%y' order by id asc

--6. Menampilkan nama film yang mengandung huruf 'd'
--jawaban:
--mengandung huruf d
select * from tblFilm

select * from tblFilm
where nm_film like '%d%' order by id asc

--7. Menampilkan nama film dan artis
--jawaban: berdasarkan kode artisnya
select * from tblArtis
select * from tblFilm

select film.nm_film, artis.nm_artis from tblArtis as artis
join tblFilm as film on artis.kd_artis = film.artis

--8. Menampilkan nama film yang artisnya berasal dari negara hongkong
--jawaban: --kd_artis -- kd_negara
select * from tblFilm
select * from tblArtis
select * from tblNegara

select film.nm_film, negara.kd_negara, negara.nm_negara from tblFilm as film
join tblArtis as artis on film.artis = artis.kd_artis
right join tblNegara as negara on artis.negara = negara.kd_negara
where negara.kd_negara = 'hk'

--9. Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf 'o'
--jawaban: --kd_artis --kd_negara
select * from tblFilm
select * from tblArtis
select * from tblNegara

select film.nm_film, negara.nm_negara from tblFilm as film
join tblArtis as artis on film.artis = artis.kd_artis
join tblNegara as negara on artis.negara = negara.kd_negara
where negara.nm_negara not like '%o%'

--10. Menampilkan nama artis yang tidak pernah bermain film
--jawaban: --kd_artis
select * from tblArtis
select * from tblFilm

--select * from tblArtis as artis --AWALNYA BEGINI
select artis.nm_artis from tblArtis as artis 
left join tblFilm as film on artis.kd_artis = film.artis --DAN DI AMBIL LEFT KARENA WAJIB MENAMPILKAN DATA NULL YANG DIBACA PADA TABEL ARTIS
-- jadi harus tampil nama artis yang sifatnya null dan di ambil berdasarkan (kd_artis dari data yang terdapat pada angelina jolie yang sifatnya null|)
where film.nm_film is null -- maka diambil datanya yaitu berdasarkan (nm_film) yang ga dimainin si angelina jolie


--11. Menampilkan nama artis yang bermain film dengan genre drama
--jawaban: --kd_artis --kd_genre
select * from tblArtis
select * from tblFilm
select * from tblGenre

select artis.nm_artis, genre.nm_genre from tblArtis as artis
join tblFilm as film on artis.kd_artis = film.artis
join tblGenre as genre on film.genre = genre.kd_genre
where genre.nm_genre = 'drama'

--12. Menampilkan nama artis yang bermain film dengan genre Action
--jawaban: --kd_artis --kd_genre
select * from tblArtis
select * from tblFilm
select * from tblGenre

select artis.nm_artis, genre.nm_genre from tblArtis as artis
join tblFilm as film on artis.kd_artis = film.artis
join tblGenre as genre on film.genre = genre.kd_genre
--where genre.nm_genre = 'action' --BISA MENGGUNAKAN INI NAMUN WAJIB SAMA ISI NAMA DI KOLOM SETIAP TABEL AGAR GROUP TIDAK AMBIGOUS
group by artis.nm_artis,genre.nm_genre
having genre.nm_genre = 'action'

--13. Menampilkan data negara dengan jumlah filmnya
--jawaban: --kd_negara --kd_artis
select * from tblNegara
select * from tblArtis
select * from tblFilm
--TAHAP 1
select * from tblNegara as negara
join tblArtis as artis on negara.kd_negara = artis.negara
join tblFilm as film on artis.kd_artis = film.artis

--TAHAP 2
--WAJIB HARUS TERBACA FILE YANG NULL TAPI TIDAK NAMA DAN NEGARANYA JG IKUT NULL WAJIB MUNCUL DATANYA
select * from tblNegara as negara
LEFT join tblArtis as artis on negara.kd_negara = artis.negara
LEFT join tblFilm as film on artis.kd_artis = film.artis
--SESUDAH MUNCUL SEMUA DAPAT, YANG BERSIFAT NULL TP TIDAK NULL SEMUA YA ISI DATANYA

--TAHAP 3
--MAKA Menampilkan data negara dengan jumlah filmnya (KD_NEGARA/NM_NEGARA/NM_FILM(VARCHAR YG BISA DI (COUNT BUKAN DI SUM)
select NEGARA.nm_negara, COUNT (FILM.nm_film) AS JUMLAH_FILM_PERNEGARA 
from tblNegara as negara
LEFT join tblArtis as artis on negara.kd_negara = artis.negara
LEFT join tblFilm as film on artis.kd_artis = film.artis
GROUP BY NEGARA.nm_negara 
--INI WAJIB NAMA/ID/SUM/COUNT YANG SIFAT DAN NILAINYA DI DALAM 1 KOLOM TERSEBUT WAJIB ADA YANG SAMA TIDAK BOLEH DI PECAH 1-1
--(COUNT) ITU BISA DI HITUNG WALAUPUN TYPE DATA NYA VARCHAR/ SEDANGKAN SUM ITU WAJIB TYPE DATA INTEGER

--14. Menampilkan nama film yang skala internasional
--jawaban: --kd_produser
select * from tblFilm
select * from tblProduser

select film.nm_film, produser.international
from tblFilm as film
join tblProduser as produser on film.produser = produser.kd_produser
where international = 'ya'

--15. Menampilkan jumlah film dari masing2 produser
--jawaban: --kd_produser
select * from tblFilm
select * from tblProduser

select count(film.nm_film) as NM_FILM_JGN_DI_PECAH_KE_GROUP_BY, produser.nm_produser from tblFilm as film
RIGHT join tblProduser as produser on film.produser = produser.kd_produser
--KENAPA RIGHT JOIN, KARENA UNTUK KLARIFIKASI DATA NYA BERDASARKAN KODE_PRODUSER YANG TERDAPAT -
--DI (from tblProduser) DIMANA ADA DATA (nm_produser PARKIT) YANG SIFATNYA NULL
group by produser.nm_produser