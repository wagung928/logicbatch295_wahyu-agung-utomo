--SIMULASI UJIAN SQL

--TABLE BIODATA
create table tblBiodata (
id bigint primary key identity (1,1),
	first_name varchar (20) not null,
	last_name varchar (30) not null,
	dob datetime,
	pob varchar (50) not null,
	address varchar (255) not null,
	gender varchar (1) not null
)

insert into tblBiodata
(first_name,last_name,dob,pob,address,gender) values
('soraya','rahayu','1990-12-22','Bali','Jl. raya kuta, Bali','P'),
('hanum','danuary','1990-01-02','Bandung','Jl. berkah ramadhan, Bandung','P'),
('melati','marcelia','1991-03-03','Jakarta','Jl. mawar 3, Brebes','P'),
('farhan','djokowidodo','1989-10-11','Jakarta','Jl. bahari raya, Solo','L')


--TABLE EMPLOYEE
create table tblEmployee (
id bigint primary key identity (1,1),
	biodata_id bigint not null,
	nip varchar (5) not null,
	status varchar (10) not null,
	join_date datetime not null,
	salary decimal (18,2) not null
)

insert into tblEmployee
(biodata_id,nip,status,join_date,salary) values
('1','XA001','Permanen','2015-11-01 00:00:00:000','12000000'),
('2','XA002','Kontrak','2017-01-02 00:00:00:000','10000000'),
('3','XA003','Kontrak','2018-08-19 00:00:00:000','10000000')

--TABEL CONTACT PERSON
create table tblContact_Person (
id bigint primary key identity (1,1),
	biodata_id bigint not null,
	type varchar (5) not null,
	contact varchar (100) not null
)

insert into tblContact_Person
(biodata_id,type,contact) values
('1','Mail','soraya.rahayu@gmail.com'),
('1','Phone','085612345678'),
('2','Mail','hanum.danuary@gmail.com'),
('2','Phone','081312345678'),
('2','Phone','087812345678'),
('3','Mail','melati.marcelia@gmail.com')

--TABLE LEAVE
create table tblLeave (
id bigint primary key identity (1,1),
	type varchar (10) not null,
	name varchar (100) not null
)

insert into tblLeave
(type,name) values
('Regular','Cuti Tahunan'),
('Khusus','Cuti Menikah'),
('Khusus','Cuti Haji & Umroh'),
('Khusus','Melahirkan')

--TABLE EMPLOYEE LEAVE
create table tblEmployee_Leave (
id bigint primary key identity (1,1),
	employee_id int not null,
	period varchar (4) not null,
	regular_quota int not null
)

insert into tblEmployee_Leave
(employee_id,period,regular_quota) values
('1','2021','16'),
('2','2021','12'),
('3','2021','12')

--TABLE LEAVE REQUEST
create table tblLeave_Request (
id bigint primary key identity (1,1),
	employee_id bigint not null,
	leave_id bigint not null,
	start_date date,
	end_date date,
	reason varchar (2550) not null
)

insert into tblLeave_Request
(employee_id,leave_id,start_date,end_date,reason) values
('1','1','2021-10-10','2021-10-12','Liburan'),
('1','1','2021-11-12','2021-11-15','Acara Kelaurga'),
('2','2','2021-05-05','2021-05-07','Menikah'),
('2','1','2021-09-09','2021-09-13','Touring'),
('2','1','2021-12-20','2021-12-23','Acara Keluarga')


select * from tblBiodata
select * from tblEmployee
select * from tblContact_Person
select * from tblLeave
select * from tblEmployee_Leave
select * from tblLeave_Request

--soal
--1. Menampilkan karyawan yang pertama kali masuk.
--jawaban:
select * from tblBiodata
select * from tblEmployee
select * from tblContact_Person
select * from tblLeave
select * from tblEmployee_Leave
select * from tblLeave_Request

select top 1 join_date, employ.nip,concat (biodata.first_name,' ',biodata.last_name) as nama_lengkap,
dob,pob,address,gender,status,salary
from tblBiodata as biodata
join tblEmployee as employ on biodata.id = employ.biodata_id
order by join_date asc


--2. Menampilkan daftar karyawan yang saat ini sedang cuti. Daftar berisi nomor_induk, nama, tanggal_mulai, lama_cuti dan keterangan.
--jawaban: --Date Now 22 Desember 2021 -- INI YANG DI CARI
--Daftar berisi nomor_induk, nama, tanggal_mulai, lama_cuti dan keterangan.s

select * from tblEmployee --1
select * from tblBiodata --2
select * from tblLeave_Request --3

select employ.nip, concat (biodata.first_name,' ',biodata.last_name) as nama_lengkap,
employ.join_date, start_date,end_date,
(datediff(day,leave_req.start_date, leave_req.end_date)+1) as lama_cuti_yg_diambil,
leave_req.reason
from tblEmployee as employ
join tblBiodata as biodata on employ.biodata_id = biodata.id
join tblLeave_Request as leave_req on employ.biodata_id = leave_req.leave_id
where '2021/12/22' between leave_req.start_date and leave_req.end_date -- dicari berdasarkan data dari (select * from tblLeave_Request) yang diamana interval pengajuan cutinya
--Date Now 22 Desember 2021 -- INI YANG DI CARI

--where getdate()/(tahun yang ditentukan soal) => beetwen start_date and end_date untuk mencari data yang sudah ada semua


--3. Menampilkan daftar karyawan yang sudah mengajukan cuti lebih dari 2 kali. Tampilkan data berisi no_induk, nama, jumlah pengajuan .
--jawaban:Tampilkan data berisi no_induk, nama, jumlah pengajuan .
select * from tblBiodata
select * from tblEmployee
select * from tblLeave
select * from tblLeave_Request

select employ.nip, concat (biodata.first_name,' ',biodata.last_name) as nama_lengkap,
count (leave_req.employee_id) as jumlah_pengajuan
from tblBiodata as biodata
join tblEmployee as employ on biodata.id = employ.biodata_id
join tblLeave as leave on employ.biodata_id = leave.id
join tblLeave_Request as leave_req on employ.biodata_id = leave_req.employee_id
group by employ.nip, biodata.first_name,biodata.last_name,leave_req.employee_id
having count (leave_req.employee_id) > 2

--4. Menampilkan sisa cuti tiap karyawan tahun ini, jika di ketahui jatah cuti setiap karyawan tahun ini adalah sesuaidengan quota cuti. Daftar berisi no_induk, nama, quota, cuti ygsudah di ambil dan sisa_cuti
--jawaban:  Daftar berisi no_induk, nama, quota, cuti ygsudah di ambil dan sisa_cuti --PENTING
select * from tblBiodata --1
select * from tblEmployee -- 2
select * from tblLeave_Request --3
select * from tblEmployee_Leave --4

select employ.nip, concat (biodata.first_name,' ',biodata.last_name) as nama_lengkap,
employ_leave.regular_quota,
count (leave_req.employee_id) as daftar_req_berdasarkan_karyawan,
case
	when leave_req.employee_id is not null then sum (datediff(day,start_date, end_date)+1)
	else 0
end cuti_yang_diambil,

case
	when leave_req.employee_id is not null then employ_leave.regular_quota - sum (datediff(day,start_date, end_date)+1)
	else employ_leave.regular_quota
end sisa_cuti

from tblBiodata as biodata
join tblEmployee_Leave as employ_leave on biodata.id = employ_leave.employee_id

join tblEmployee as employ on biodata.id = employ.biodata_id

left join tblLeave_Request as leave_req on employ_leave.employee_id = leave_req.employee_id

group by employ.nip, biodata.first_name,biodata.last_name , regular_quota,leave_req.employee_id


--5. Perusahaan akan meberikan bonus bagi karyawan yang sudah bekerja lebih dari 5 tahun sebanyak 1.5 kali gaji. Tampilan No induk, Fullname, Berapa lama bekerja, Bonus, Total Gaji(gaji + bonus)
--jawaban: Tampilan No induk, Fullname, Berapa lama bekerja, Bonus = (gaji)*1.5 + gaji, Total Gaji(gaji + bonus)
select * from tblEmployee --1
select * from tblBiodata --2
select * from tblEmployee_Leave --3

select employ.nip, concat (biodata.first_name,' ',biodata.last_name) as nama_lengkap,
datediff(year,employ.join_date,'2021/12/22') as lama_bekerja, employ.salary,
(employ.salary)*1.5 as bonus,
case
	when datediff(year,employ.join_date,'2021/12/22') > 5 then ((employ.salary) + (employ.salary)*1.5)
end as total_gaji
from tblEmployee as employ
join tblBiodata as biodata on employ.biodata_id = biodata.id
join tblEmployee_Leave as employ_leave on employ.biodata_id = employ_leave.employee_id
where datediff(year,employ.join_date,'2021/12/22') > 5


--6. Tampilkan nip, nama_lengkap, (jika karyawan ada yg berulang tahun di hari ini akan diberikan hadiah bonus sebanyak 5% dari gaji) (jika tidak ulang tahun maka bonus 0 dan total gaji) . Tampilkan No Induk, nama, Tgl lahir , Usia, Bonus, Total Gaji
--jawaban: 
--Tampilkan No Induk, nama, Tgl lahir , Usia, Bonus, Total Gaji (salary)
--(jika karyawan ada yg berulang tahun di hari ini akan diberikan hadiah bonus sebanyak 5% dari gaji) as bonus
--(jika tidak ulang tahun maka bonus 0 dan total gaji) as bonus 0
select * from tblEmployee --1
select * from tblBiodata --2
select * from tblEmployee_Leave --3
select * from tblContact_Person
select * from tblLeave

select employ.nip, CONCAT (biodata.first_name,' ',biodata.last_name) as nama_lengkap,
datepart (day,dob) as tgl_lahir, datepart(month, dob) as bulan_lahir,
datediff(year,dob,'2021/12/22') as usia, dob,
case
	when day (biodata.dob) = day ('2021/12/22') and month (biodata.dob) = month ('2021/12/22') then 0.05*(employ.salary) 
	-- ini cara memecah belah hari dan bulan,YANG DIMANA AKAN DI DETECT BERDASARKAN SOAL
	else 0
end as bonus,
case
	when day (biodata.dob) = day ('2021/12/22') and month (biodata.dob) = month ('2021/12/22') then (employ.salary) + 0.05*(employ.salary)
	-- ini cara memecah belah hari dan bulan,YANG DIMANA AKAN DI DETECT BERDASARKAN SOAL
	else 0
end as total_gaji
from tblEmployee as employ
join tblBiodata as biodata on employ.biodata_id = biodata.id
join tblEmployee_Leave as employ_leave on employ.biodata_id = employ_leave.employee_id
where day (biodata.dob) = day ('2021/12/22') and month (biodata.dob) = month ('2021/12/22')

--7. Tampilkan No Induk, nama, Tgl lahir , Usia. Urutkan biodata dari yg paling muda sampai yg tua
--jawaban:
select * from tblEmployee --1
select * from tblBiodata --2
select * from tblEmployee_Leave --3

select employ.nip, CONCAT (biodata.first_name,' ',biodata.last_name) as nama_lengkap,
datepart(day,dob) as tgl_lahir,
datediff (year,dob,'2021/12/22') as umur
from tblEmployee as employ
join tblBiodata as biodata on employ.biodata_id = biodata.id
join tblEmployee_Leave as employ_leave on employ.biodata_id = employ_leave.employee_id
order by datediff (year,dob,'2021/12/22') asc

--8. Tampikan Karyawan yg belum pernah Cuti
--jawaban:
select * from tblBiodata --1
select * from tblEmployee --2
select * from tblLeave_Request --3
select * from tblContact_Person --4

select employ.nip, CONCAT (biodata.first_name,' ',biodata.last_name) as nama_lengkap,
dob,pob,address,gender,status,join_date,salary,type,contact
from tblBiodata as biodata
join tblEmployee as employ on biodata.id = employ.biodata_id
left join tblLeave_Request as leave_req on employ.biodata_id = leave_req.employee_id
join tblContact_Person as contact on employ.biodata_id = contact.biodata_id
where leave_req.employee_id is null


--9. Tampikan Nama Lengkap, Jenis Cuti, Durasi Cuti, dan no telp yang sedang cuti
--jawaban:
select * from tblBiodata --1
select * from tblEmployee --2
select * from tblContact_Person --3
select * from tblEmployee_Leave --4
select * from tblLeave_Request --5
select * from tblLeave --6

select concat(biodata.first_name,' ',biodata.last_name) Nama_Lengkap,
leave.name, 
DATEDIFF(day,leave_req.start_date,leave_req.end_date) as Durasi_Cuti,
contact.contact
from tblBiodata as biodata
join tblEmployee as employe on biodata.id = employe.biodata_id
join tblContact_Person as contact on employe.biodata_id = contact.biodata_id
join tblEmployee_Leave as employ_leave on employe.biodata_id = employ_leave.employee_id
join tblLeave_Request as leave_req on employ_leave.employee_id = leave_req.employee_id
join tblLeave as leave on leave.id = leave_req.leave_id
where '2021/12/22' between start_date and end_date and contact.type = 'PHONE'
--group by biodata.first_name,biodata.last_name , leave.name,DATEDIFF(day,leave_req.start_date,leave_req.end_date),contact.contact


--10. Tampilkan nama-nama pelamar yang tidak diterima sebagai karyawan
--jawaban:
select * from tblBiodata --1
select * from tblEmployee --2

select CONCAT (biodata.first_name,' ',biodata.last_name) as nama_lengkap,
dob,pob,address,gender
from tblBiodata as biodata
left join tblEmployee as employ on biodata.id = employ.biodata_id
where employ.biodata_id is null

--11. buatlah sebuah view yg menampilkan data nama lengkap, tgl lahir dan tmpat lahir , status, dan salary
--jawaban:
select * from tblBiodata --1
select * from tblEmployee --2

create view data_karyawan as
select CONCAT (biodata.first_name,' ',biodata.last_name) as nama_lengkap,
dob as tgl_lahir,
pob as tempat_lahir, employ.status, employ.salary
from tblBiodata as biodata
join tblEmployee as employ on biodata.id = employ.biodata_id

select * from data_karyawan
--drop view data_karyawan