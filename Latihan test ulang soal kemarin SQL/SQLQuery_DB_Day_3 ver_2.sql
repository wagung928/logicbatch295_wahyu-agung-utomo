create table tblMahasiswa (
id int primary key identity (1,1),
	nim varchar (20),
	nama varchar(50) not null,
	jenis_kelamin varchar (50) not null,
	alamat varchar (50) not null
)
select * from tblMahasiswa
drop table tblMahasiswa
insert into tblMahasiswa
(nim, nama, jenis_kelamin, alamat) values
('101','Arif','L','Jl.Kenangan'),
('102','Budi','L','Jl.Jombang'),
('103','Wati','P','Jl.Surabaya'),
('104','Ika','P','Jl.Jombang'),
('105','Tono','L','Jl.Jakarta'),
('106','Iwan','L','Jl.Bandung'),
('107','Sari','P','Jl.Malang')
select * from tblMahasiswa
--drop table tblMahasiswa



create table tblMatakuliah (
id int primary key identity (1,1),
	kode_mk varchar (50),
	nama_mk varchar(50) not null,
	sks int  not null,
	semester varchar (50) not null,
)
select * from tblMatakuliah
--drop table tblMatakuliah
insert into tblMatakuliah
(kode_mk, nama_mk, sks, semester) values
('PTI447','Praktikum Basis Data','1','3'),
('TIK342','Praktikum Basis Data','1','3'),
('PTI333','Basis Data Terdistribusi','3','5'),
('TIK123','Jaringan Komputer','2','5'),
('TIK333','Sistem Operasi','3','5'),
('PTI123','Grafika Multimedia','3','5'),
('PTI777','Sistem Informasi','2','3')
select * from tblMatakuliah
--drop table tblMatakuliah



create table tblAmbil_mk (
id int primary key identity (1,1),
	nim varchar (20) not null,
	kode_mk varchar(50) not null
)
select * from tblAmbil_mk
--drop table tblAmbil_mk

insert into tblAmbil_mk
(nim, kode_mk) values
('101','PTI447'),
('103','TIK333'),
('104','PTI333'),
('104','PTI777'),
('111','PTI123'),
('123','PTI999')

select * from tblAmbil_mk
--drop table tblAmbil_mk


--SOAL
--1. Tampilkan nama mahasiswa dan matakuliah yang diambil
--jawaban:
select * from tblMahasiswa
select * from tblMatakuliah
select * from tblAmbil_mk

select nama, nama_mk from tblMatakuliah as Mk
join tblAmbil_mk as Am on Mk.kode_mk = Am.kode_mk
join tblMahasiswa as Mhs  on Mhs.nim = Am.nim

--2. Tampilkan data mahasiswa yang tidak mengambil matakuliah
--jawaban:
select * from tblMahasiswa
select * from tblMatakuliah
select * from tblAmbil_mk

select Mhs.nim, nama, jenis_kelamin, alamat from tblMatakuliah as Mk
right join tblAmbil_mk as Am on Mk.kode_mk = Am.kode_mk
right join tblMahasiswa as Mhs  on Mhs.nim = Am.nim
where nama_mk is null


--3. Kelompokan data mahasiswa yang tidak mengambil matakuliah berdasarkan jenis kelaminnya, kemudian hitung banyaknya.
--jawaban:
select * from tblMahasiswa
select * from tblMatakuliah
select * from tblAmbil_mk

select  count (jenis_kelamin) as jml,
jenis_kelamin from tblMatakuliah as Mk
right join tblAmbil_mk as Am on Mk.kode_mk = Am.kode_mk
right join tblMahasiswa as Mhs  on Mhs.nim = Am.nim
where nama_mk is null
group by jenis_kelamin


--4. Dapatkan nim dan nama mahasiswa yang mengambil matakuliah beserta kode_mk dan nama_mk yang diambilnya
--jawaban:
select * from tblMahasiswa
select * from tblMatakuliah
select * from tblAmbil_mk

select  Mhs.nim,Mhs.nama,Mk.kode_mk, Mk.nama_mk from tblMatakuliah as Mk
right join tblAmbil_mk as Am on Mk.kode_mk = Am.kode_mk
right join tblMahasiswa as Mhs  on Mhs.nim = Am.nim
where Mk.kode_mk is not null

--5. Dapatkan nim, nama, dan total sks yang diambil oleh mahasiswa, Dimana total sksnya lebih dari 4 dan kurang dari 10.
--jawaban:
select * from tblMahasiswa
select * from tblMatakuliah
select * from tblAmbil_mk

select mhs.nim,Mhs.nama, sum (mk.sks) as jml_sks
from tblMahasiswa as Mhs
join tblAmbil_mk as Am on Am.nim = Mhs.nim
join tblMatakuliah as Mk on Mk.kode_mk = Am.kode_mk
group by Mhs.nim, Mhs.nama
having sum (mk.sks) > 4 and sum (mk.sks) < 10


--6. Dapatkan data matakuliah yang tidak diambil oleh mahasiswa terdaftar (mahasiswa yang terdaftar adalah mahasiswa yang tercatat di tabel mahasiswa).
--jawaban:
select * from tblMahasiswa
select * from tblMatakuliah
select * from tblAmbil_mk

select Mk.nama_mk , mk.kode_mk
from tblMatakuliah as Mk 
left join tblAmbil_mk as Am on Mk.kode_mk = Am.kode_mk
left join tblMahasiswa as Mhs on Mhs.nim = am.nim
where Mhs.nama is null -- karena yang dicari itu nama matkulnya yang di daftarkan oleh nama mhs, maka disini yang di masukkan adalah nama mhs, karena agar matkul yang tidak di daftarkan mhs itu terdetec

