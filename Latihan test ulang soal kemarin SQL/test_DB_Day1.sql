--TEST DB DAY 1 ULANG
select * from tblPengarang
select * from tblGaji

--soal
--1. Hitung dan tampilkan jumlah pengarang dari table tblPengarang.
--jawaban:
select * from tblPengarang
select count (Kd_Pengarang) as jumlah from tblPengarang

--2. Hitunglah berapa jumlah Pengarang Wanita dan Pria
--jawaban:
select * from tblPengarang
select kelamin ,count (kelamin) as jumlah from tblPengarang
group by kelamin

--3. Tampilkan record kota dan jumlah kotanya dari table tblPengarang.
--jawaban:
select * from tblPengarang
select kota ,COUNT (kota) as jumlah from tblPengarang
group by kota
--4. Tampilkan record kota diatas 1 kota dari table tblPengarang.
--jawaban:
select * from tblPengarang
select kota ,count (kota) as jumlah from tblPengarang
group by kota
having count (kota) > 1 -- ini wajib di bawa kondisinya berdasarkan select

--5. Tampilkan Kd_Pengarang yang terbesar dan terkecil dari table tblPengarang.
--jawaban:
select * from tblPengarang
select max (Kd_Pengarang) as kd_pengarang_besar, min (Kd_Pengarang) as kd_pengarang_kecil from tblPengarang

--6. Tampilkan gaji tertinggi dan terendah.
--jawaban:
select * from tblGaji
select max(gaji) as gaji_tertinggi, min (gaji) as gaji_terendah from tblGaji

--7. Tampilkan gaji diatas 600.000.
--jawaban:
select * from tblGaji
select gaji from tblGaji
where gaji > 600000

--8. Tampilkan jumlah gaji.
--jawaban:
select * from tblGaji
select SUM (gaji) as jumlah_gaji from tblGaji

--9. Tampilkan jumlah gaji berdasarkan Kota
--jawaban:
select * from tblGaji
select * from tblPengarang
select peng.kota, sum (gaji) as gaji_perKota from tblGaji as gaji
join tblPengarang as peng on gaji.Kd_Pengarang = peng.Kd_Pengarang
group by kota

--10. Tampilkan seluruh record pengarang antara P0001-P0006 dari tabel pengarang.
--jawaban:
select * from tblPengarang
select * from tblPengarang
where Kd_Pengarang between 'p0001' and 'p0006'