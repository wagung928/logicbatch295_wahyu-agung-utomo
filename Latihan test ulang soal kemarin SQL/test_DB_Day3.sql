--TEST DB DAY 3 ULANG

select * from tblKaryawan
select * from tblDivisi
select * from tblJabatan
select * from tblPekerjaan

--soal
--1. Tampilkan nama lengkap, nama jabatan, tunjangan jabatan + gaji , yang gaji + tunjangan pekerjaannya dibawah 5juta
--jawaban: -- kd_nip --kd_jabatan
select * from tblKaryawan
select * from tblPekerjaan
select * from tblJabatan

select concat (nama_depan,' ',nama_belakang) as nama_lengkap,
jabatan.nama_jabatan, 
(jabatan.tunjangan_jabatan + jabatan.gaji_pokok) as gaji_tunjangan
from tblKaryawan as karyawan
join tblPekerjaan as pekerjaan on karyawan.nip = pekerjaan.nip
join tblJabatan as jabatan on pekerjaan.kode_jabatan = jabatan.kd_jabatan
where (jabatan.tunjangan_jabatan + jabatan.gaji_pokok) < 5000000


--2. Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi
--jawaban: --kd_nip (karyawan+pekerjaan) -- kd_jabatan (jabatan + pekerjaan) --kd_divisi (divisi + pekerjaan)
--total gaji = Gaji_pokok+ Tunjangan_jabatan+ Tunjangan_kinerja
--Pajak = 5% dari total gaji
--gaji bersih = total gaji - pajak
select * from tblKaryawan
select * from tblPekerjaan
select * from tblJabatan
select * from tblDivisi

select concat (karyawan.nama_depan,' ',karyawan.nama_belakang) as nama_lengkap,
jabatan.nama_jabatan, divisi.nama_divisi, pekerjaan.kota_penempatan,
(jabatan.gaji_pokok + jabatan.tunjangan_jabatan + pekerjaan.tunjangan_kinerja) as total_gaji,
(jabatan.gaji_pokok + jabatan.tunjangan_jabatan + pekerjaan.tunjangan_kinerja) * 0.05 as pajak,
(jabatan.gaji_pokok + jabatan.tunjangan_jabatan + pekerjaan.tunjangan_kinerja) - 
((jabatan.gaji_pokok + jabatan.tunjangan_jabatan + pekerjaan.tunjangan_kinerja) * 0.05) as gaji_bersih
from tblKaryawan as karyawan
join tblPekerjaan as pekerjaan on karyawan.nip = pekerjaan.nip
join tblJabatan as jabatan on pekerjaan.kode_jabatan = jabatan.kd_jabatan
join tblDivisi as divisi on pekerjaan.kode_divisi = divisi.kd_divisi
where karyawan.jenis_kelamin = 'PRIA' and pekerjaan.kota_penempatan != 'sukabumi'


--3. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja * 7)
--jawaban: --kd_nip (kar + peker) --kd_jabatan (peker + jabat) --kd_divisi (peker + divisi)
select * from tblKaryawan
select * from tblPekerjaan
select * from tblJabatan
select * from tblDivisi

select kar.nip, concat (kar.nama_depan,' ',kar.nama_belakang) as nama_lengkap,
jab.nama_jabatan, div.nama_divisi,
0.25*(jab.gaji_pokok + jab.tunjangan_jabatan + per.tunjangan_kinerja)*7 as bonus
from tblKaryawan as kar
join tblPekerjaan as per on kar.nip = per.nip
join tblJabatan as jab on per.kode_jabatan = jab.kd_jabatan
join tblDivisi as div on per.kode_divisi = div.kd_divisi


--4. Tampilkan nip, nama lengkap, nama jabatan, nama divisi, total gaji, infak(5%*total gaji) yang mempunyai jabatan MGR
--jawaban: --kd_nip(kar + per) --kd_jabatan (per + jab) --kd_divisi (per + div)
--total gaji = Gaji_pokok+ Tunjangan_jabatan+ Tunjangan_kinerja
--infak = (5%*total gaji)
select * from tblKaryawan
select * from tblPekerjaan
select * from tblJabatan
select * from tblDivisi

select kar.nip,concat (kar.nama_depan,' ',kar.nama_belakang) as nama_lengkap,
jab.nama_jabatan, div.nama_divisi,
(jab.gaji_pokok + jab.tunjangan_jabatan + per.tunjangan_kinerja) as total_gaji,
(jab.gaji_pokok + jab.tunjangan_jabatan + per.tunjangan_kinerja)*0.05 as infak
from tblKaryawan as kar
join tblPekerjaan as per on kar.nip = per.nip
join tblJabatan as jab on per.kode_jabatan = jab.kd_jabatan
join tblDivisi as div on per.kode_divisi = div.kd_divisi
where jab.kd_jabatan = 'mgr'

--5. Tampilkan nip, nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1
--jawaban: --kd_nip (kar + per) --kd_jabatan (per + jab) --kd_div (per + div)
select * from tblKaryawan
select * from tblPekerjaan
select * from tblJabatan
select * from tblDivisi

select kar.nip, concat (kar.nama_depan,' ',kar.nama_belakang) as nama_lengkap,
nama_jabatan, kar.pendidikan_terakhir, 
2000000 as tunjangan_pendidikan,
(jab.gaji_pokok + jab.tunjangan_jabatan + 2000000) as total_gaji_pokok
from tblKaryawan as kar
join tblPekerjaan as per on kar.nip = per.nip
join tblJabatan as jab on per.kode_jabatan = jab.kd_jabatan
join tblDivisi as div on per.kode_divisi = div.kd_divisi
where kar.pendidikan_terakhir like 'S1%'

--6. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus
--jawaban: --kd_nip (kar + per) --kd_jabatan (per + jab) --kd_div (per + div)
--MGR = (bonus=25% dari total gaji*7)
--ST = (bonus=25% dari total gaji*5)
--other = (bonus=25% dari total gaji*2)
select * from tblKaryawan
select * from tblPekerjaan
select * from tblJabatan
select * from tblDivisi

select kar.nip, concat (kar.nama_depan,' ',kar.nama_belakang) as nama_lengkap,
jab.nama_jabatan, div.nama_divisi,
case
when per.kode_jabatan = 'mgr' then 0.25*(jab.gaji_pokok + jab.tunjangan_jabatan + per.tunjangan_kinerja)*7
when per.kode_jabatan = 'st' then 0.25*(jab.gaji_pokok + jab.tunjangan_jabatan + per.tunjangan_kinerja)*5
else 0.25*(jab.gaji_pokok + jab.tunjangan_jabatan + per.tunjangan_kinerja)*2
end as bonus
from tblKaryawan as kar
join tblPekerjaan as per on kar.nip = per.nip
join tblJabatan as jab on per.kode_jabatan = jab.kd_jabatan
join tblDivisi as div on per.kode_divisi = div.kd_divisi

--7. Buatlah kolom nip pada table karyawan sebagai kolom unique
--jawaban:
select * from tblKaryawan

alter table tblKaryawan
add CONSTRAINT UC_nip unique (nip)

--8. buatlah kolom nip pada table karyawan sebagai index (KHUSUS INDEX TIDAK ADA ALTER/ WAJIB CREATE)
--jawaban:
select * from tblKaryawan

create index index_nip
on tblKaryawan (nip)

--9. Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf W
--jawaban:
select * from tblKaryawan

select concat (nama_depan,' ',upper(nama_belakang)) as nama_lengkap -- ini namanya sisipan nama besar di dalam concat
from tblKaryawan
where nama_belakang like 'w%' order by id asc

--10.  Perusahaan akan memberikan bonus sebanyak 10% dari total gaji bagi karyawan yg sudah join di peruashaan diatas 8/ sudah 8 thn tahun
-- Tampilkan nip, nama lengkap, jabatan, nama divisi, total gaji , bonus, lama bekerja
-- (total gaji = Gaji_pokok+Tunjangan_jabatan+Tunjangan_kinerja )
--jawaban: --kd_nip (kar + per) --kd_jabatan (per + jab) --kd_divisi (per + div)
select * from tblKaryawan
select * from tblPekerjaan
select * from tblJabatan
select * from tblDivisi

select kar.nip, concat (kar.nama_depan,' ',kar.nama_belakang) as nama_lengkap, --1
tgl_masuk, --2
jab.nama_jabatan, div.nama_divisi,
(jab.gaji_pokok + jab.tunjangan_jabatan + per.tunjangan_kinerja) as total_gaji, --3
case
when datediff(year,tgl_masuk,getdate()) >= 8 then (jab.gaji_pokok + jab.tunjangan_jabatan + per.tunjangan_kinerja)*0.1
end as bonus, --4
getdate () as tahun_sekarang, --5
datediff(year,tgl_masuk,getdate()) as Lama_bekerja --6
from tblKaryawan as kar
join tblPekerjaan as per on kar.nip = per.nip
join tblJabatan as jab on per.kode_jabatan = jab.kd_jabatan
join tblDivisi as div on per.kode_divisi = div.kd_divisi
where datediff(year,tgl_masuk,getdate()) >=8 --7 
--artinya ini ambil tahun = (tahun sekarang (getdate) - tgl masuk) wajib gunakan datediff jika perbedaan dua tanggal (pengurangan)