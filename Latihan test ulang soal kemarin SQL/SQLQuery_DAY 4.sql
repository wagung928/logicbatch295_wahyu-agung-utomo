--DB day 4

create table tblSalesPerson (
id int primary key identity (1,1) not null,
	name varchar (50) not null,
	bod date,
	salary decimal (18,2) not null
)

select * from tblSalesPerson

insert into tblSalesPerson
(name, bod, salary) values
('Abe','9/11/1988','14000'),
('Bob','9/11/1978','44000'),
('Chris','9/11/1983','40000'),
('Dan','9/11/1980','52000'),
('Ken','9/11/1977','115000'),
('Joe','9/11/1990','38000')

select * from tblSalesPerson
--drop table tblSalesPerson

create table tblOrders (
id int primary key identity (1,1) not null,
	order_Date date,
	cust_id int not null,
	salesPerson_id int not null,
	amount decimal (18,2) not null
)

select * from tblOrders

insert into tblOrders 
(order_date,cust_id,salesPerson_id,amount) values
('8/2/2020','4','2','540'),
('1/22/2021','4','5','1800'),
('7/14/2019','9','1','460'),
('1/29/2018','7','2','2400'),
('2/3/2021','6','4','600'),
('3/2/2020','6','4','720'),
('5/6/2021','9','4','150')

select * from tblOrders
--drop table tblOrders



--soal

--Kita memiliki 2 tabel data, yaitu SalesPerson dan Orders. Dari 2 tabel tersebut kita membutuhkan informasi yaitu :
select * from tblSalesPerson
select * from tblOrders

--a. Informasi nama sales yang memiliki order lebih dari 1.
--jawaban:
select * from tblSalesPerson
select * from tblOrders

select sales.name, count (orderan.salesPerson_id) as order_lebih_dari_1
from tblSalesPerson as sales
join tblOrders as orderan 
on sales.id = orderan.salesPerson_id
group by sales.name
having count (orderan.salesPerson_id) > 1

--b. Informasi nama sales yang total amount ordernya di atas 1000.
--jawaban:
select * from tblSalesPerson
select * from tblOrders

select sales.name,sum (orderan.amount) as total_amount
from tblSalesPerson as sales
join tblOrders as orderan
on sales.id = salesPerson_id
group by sales.name
having sum(orderan.amount) >= 1000

--c. Informasi nama sales, umur, gaji dan total amount order yang tahun ordernya >= 2020 dan data ditampilan berurut sesuai dengan umur (ascending).
--jawaban:
select * from tblSalesPerson
select * from tblOrders

select sales.name, datediff (YYYY,bod,getdate()) as umur,
(sales.salary),
sum (orderan.amount) as sum_amount
from tblSalesPerson as sales
join tblOrders as orderan
on sales.id = orderan.salesPerson_id
where orderan.order_Date >= '2020' 
group by sales.name, datediff (YYYY,bod,getdate()), sales.salary
order by datediff (YYYY,bod,getdate()) asc-- order by bisa setelah gruop by di atasnya (POKONYA ORDER BY PALING BAWAH DIA SETELAH SEMUA SYNTAX)

--d. Carilah rata-rata total amount masing-masig sales urutkan dari hasil yg paling besar
--jawaban:
select * from tblSalesPerson
select * from tblOrders

select sales.name,avg (orderan.amount) as rata_rata
from tblSalesPerson as sales
join tblOrders as orderan
on sales.id = orderan.salesPerson_id
group by sales.name
order by avg (orderan.amount) desc

--e. perusahaan akan memberikan bonus bagi sales yang berhasil memiliki order lebih dari 2 dan total order lebih dari 1000 sebanyak 30% dari salary
--jawaban:
select * from tblSalesPerson
select * from tblOrders

select sales.name, orderan.cust_id,orderan.amount,sales.salary,
case
	when orderan.cust_id > 2 and orderan.amount > 1000 then 0.3*(salary)
	end as order_lbh_2_total_order_lbhdari_1000,
case
	when orderan.cust_id > 2 and orderan.amount > 1000 then 0.3*(salary) + salary
	end as total_gaji_plus_bonus_30persen
from tblSalesPerson as sales
join tblOrders as orderan
on sales.id = orderan.cust_id
where orderan.amount > 1000

--f. Tampilkan data sales yang belum memiliki orderan sama sekali
--jawaban:
select * from tblSalesPerson
select * from tblOrders

select * 
from tblSalesPerson as sales
left join tblOrders as orderan
on sales.id = salesPerson_id
where salesPerson_id is null

--g. jika sales tidak memiliki orderan maka gaji akan di potong sebanyak 2%
--jawaban:
select * from tblSalesPerson
select * from tblOrders

select * ,
case
	when orderan.salesPerson_id is null then (sales.salary*2)/100
	end as sales_gada_orderan_kena_potong_gaji,
case
	when orderan.salesPerson_id is null then sales.salary - ((sales.salary*2)/100)
	end as sales_gada_orderan_sudah_di_potong_gaji
from tblSalesPerson as sales
left join tblOrders as orderan
on sales.id = orderan.salesPerson_id
where orderan.salesPerson_id is null








--c. Informasi nama sales, umur, gaji dan total amount order yang tahun ordernya >= 2020 dan data ditampilan berurut sesuai dengan umur (ascending).
--jawaban:

select * from tblSalesPerson
select * from tblOrders

select sales.name, datediff (YYYY,bod,getdate()) as umur, sales.salary,
sum (orderan.amount) as total_amount
from tblSalesPerson as sales
join tblOrders as orderan
on sales.id = orderan.salesPerson_id
where orderan.order_Date >= '2020'
group by sales.name,datediff (YYYY,bod,getdate()), sales.salary
order by datediff (YYYY,bod,getdate()) asc



--d. Carilah rata-rata total amount masing-masig sales urutkan dari hasil yg paling besar
select * from tblSalesPerson
select * from tblOrders

select sales.name, avg (orderan.amount) as rata_rata
from tblSalesPerson as sales
left join tblOrders as orderan
on sales.id = orderan.salesPerson_id
group by sales.name
order by avg (orderan.amount) desc