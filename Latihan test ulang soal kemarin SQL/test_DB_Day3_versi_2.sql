--TEST DB DAY 3 VERSI 2 ULANG

select * from tblMahasiswa
select * from tblMatakuliah
select * from tblAmbil_mk

--soal
--1. Tampilkan nama mahasiswa dan matakuliah yang diambil
--jawaban: --kd_nim (mhs + mk) --kd_mk (mk + matkul)
select * from tblMahasiswa
select * from tblAmbil_mk
select * from tblMatakuliah

select mhs.nama, matkul.nama_mk
from tblMahasiswa as mhs
join tblAmbil_mk as mk on mhs.nim = mk.nim
join tblMatakuliah as matkul on mk.kode_mk = matkul.kode_mk

--2. Tampilkan data mahasiswa yang tidak mengambil matakuliah
--jawaban: --kd_nim (mhs + mk) --kd_mk (mk + matkul)
select * from tblMahasiswa
select * from tblAmbil_mk
select * from tblMatakuliah

select mhs.nim, mhs.nama, mhs.jenis_kelamin,mhs.alamat
from tblMahasiswa as mhs
left join tblAmbil_mk as mk on mhs.nim = mk.nim
left join tblMatakuliah as matkul on mk.kode_mk = matkul.kode_mk
where matkul.nama_mk is null

--3. Kelompokan data mahasiswa yang tidak mengambil matakuliah berdasarkan jenis kelaminnya, kemudian hitung banyaknya.
--jawaban: --kd_nim (mhs + mk) --kd_mk (mk + matkul)
select * from tblMahasiswa
select * from tblAmbil_mk
select * from tblMatakuliah

select mhs.jenis_kelamin,count (mhs.jenis_kelamin) as jumlah
from tblMahasiswa as mhs
left join tblAmbil_mk as mk on mhs.nim = mk.nim
left join tblMatakuliah as matkul on mk.kode_mk = matkul.kode_mk
where mk.kode_mk is null
group by mhs.jenis_kelamin

--4. Dapatkan nim dan nama mahasiswa yang mengambil matakuliah beserta kode_mk dan nama_mk yang diambilnya
--jawaban: --kd_nim (mhs + mk) --kd_mk (mk + matkul)
select * from tblMahasiswa
select * from tblAmbil_mk
select * from tblMatakuliah

select mhs.nim, mhs.nama, mk.kode_mk, matkul.nama_mk
from tblMahasiswa as mhs
left join tblAmbil_mk as mk on mhs.nim = mk.nim
left join tblMatakuliah as matkul on mk.kode_mk = matkul.kode_mk
where mk.kode_mk is not null


--5. Dapatkan nim, nama, dan total sks yang diambil oleh mahasiswa, Dimana total sksnya lebih dari 4 dan kurang dari 10.
--jawaban: --kd_nim (mhs + mk) --kd_mk (mk + matkul)
select * from tblMahasiswa
select * from tblAmbil_mk
select * from tblMatakuliah

select mhs.nim, mhs.nama, sum (matkul.sks) as total_sks
from tblMahasiswa as mhs
join tblAmbil_mk as mk on mhs.nim = mk.nim
join tblMatakuliah as matkul on mk.kode_mk = matkul.kode_mk
group by mhs.nim,mhs.nama
having sum (matkul.sks) > 4 and sum (matkul.sks) < 10


--6. Dapatkan data matakuliah yang tidak diambil oleh mahasiswa terdaftar (mahasiswa yang terdaftar adalah mahasiswa yang tercatat di tabel mahasiswa).
--jawaban: --kd_nim (mhs + mk) --kd_mk (mk + matkul)
select * from tblMahasiswa
select * from tblAmbil_mk
select * from tblMatakuliah

select matkul.nama_mk, mk.kode_mk, mhs.nama
from tblMahasiswa as mhs
right join tblAmbil_mk as mk on mhs.nim = mk.nim
right join tblMatakuliah as matkul on mk.kode_mk = matkul.kode_mk --karena kita butuh persetujuannya data matkul untuk di database detec makanya
--pilihannya right join
where mhs.nama is null --karena data matkul ke sinkron dengan nama mhs yang terdaftar, maka hanya yang di butuhkan untuk ceknya yakni (nama mhs nya)